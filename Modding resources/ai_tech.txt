#Ship_Company
ai_will_do = {
	factor = 0
	
	modifier = {
		has_navy_size = { size > 25 } #has a large navy
		factor = 1
	}
	modifier = {
		num_of_naval_factories > 3 #has the industry to take advantage of the company
		factor = 1
	}
	modifier = {
		is_major = yes #Majors project power
		factor = 1
	}
}
#Submarine_Company
ai_will_do = {
	factor = 0
	
	modifier = {
		has_navy_size = { size > 25 } #has a large navy
		factor = 1
	}
	modifier = {
		num_of_naval_factories > 3 #has the industry to take advantage of the company
		factor = 1
	}
	modifier = {
		is_major = yes #Majors project power
		factor = 1
	}
}
#Vehicle_Company
ai_will_do = {
	factor = 1 #All countries need a land army, vehicles are part of modern warfare
	
	modifier = {
		num_of_military_factories > 10 #has the industry to take advantage of the company
		factor = 1
	}
	modifier = {
		is_major = yes #Majors project power
		factor = 1
	}
}
#Infantry_Weapon_Company
ai_will_do = {
	factor = 1
	
	modifier = {
		num_of_military_factories > 5 #has the industry to take advantage of the company
		factor = 1
	}
	modifier = {
		is_major = yes #Majors project power
		factor = 1
	}
}
#Helicopter_Company
ai_will_do = {
	factor = 0
	
	modifier = {
		has_tech = attack_helicopter2 #has semi-modern tech, most countries dont have it
		factor = 1
	}
	modifier = {
		has_tech = transport_helicopter2 #has semi-modern tech, most countries dont have it
		factor = 1
	}
	modifier = {
		is_major = yes #Majors project power
		factor = 1
	}
}
#Aircraft_Company
ai_will_do = {
	factor = 0
	
	modifier = {
		or = {
			has_tech = AS_Fighter2 #has semi-modern tech
			has_tech = MR_Fighter2
			has_tech = Strike_fighter2
			has_tech = L_Strike_fighter2
			has_tech = Air_UAV1
		}
		factor = 1
	}
	modifier = {
		or = {
			has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
			has_tech = transport_plane2
			has_tech = naval_plane3
			has_tech = cas2
		}
		factor = 1
	}
	modifier = {
		is_major = yes #Majors project power
		factor = 1
	}
}