
Faction related defines:

IDEOLOGY_JOIN_FACTION_MIN_LEVEL = 0.3,			-- ideology limit required to join faction (of faction leader idelogy)
JOIN_FACTION_LIMIT_CHANGE_AT_WAR = 1.0,			-- if in defensive war this or your modifier is counted as limit to join factions (so if you have 80% join limit this now means you can join at 50% <- A filthy lie. It's the lowest of the two) --was -0.3

DIPLOMACY_CREATE_FACTION_FACTOR = 1.00,		-- Factor for AI desire to create a new faction. Val < 1.0 makes it less likely to create than to join.
DIPLOMACY_FACTION_WRONG_IDEOLOGY_PENALTY = 60, -- AI penalty for diplomatic faction acitons between nations of different ideologies
DIPLOMACY_FACTION_SAME_IDEOLOGY_MAJOR = 10, -- AI bonus acceptance when being asked about faction is a major of the same ideology
DIPLOMACY_FACTION_NEUTRALITY_PENALTY = 0,	-- Neutral nations have a separate penalty, not wanting to get involved at all, rather than caring much about the difference in ideology #30
DIPLOMACY_FACTION_GLOBAL_TENSION_FACTOR = 0.25,-- How much the AI takes global tension into account when considering faction actions
DIPLOMACY_LEAVE_FACTION_OPINION_FACTOR = 0.75,-- How much the opinion of countries being targeted by faction leader matters when deciding to leave a faction (democratic only)
DIPLOMACY_FACTION_WAR_RELUCTANCE = -60,		-- Penalty to desire to enter a faction with a country that we are not fighting wars together with.
DIPLOMACY_SCARED_MINOR_EXTRA_RELUCTANCE = -60, -- extra reluctance to join stuff as scared minor
DIPLOMACY_FACTION_PLAYER_JOIN = 0,			-- Bonus for human players asking to join a faction.
DIPLOMACY_FACTION_MAJOR_AT_WAR = 1000.0,	-- Factor that will be multiplied with the surrender level in the desire to offer to the other ai to join a faction
DIPLOMACY_FACTION_SURRENDER_LEVEL = 20, 	-- How much the recipient nation losing matters for joining a faction
DIPLO_PREFER_OTHER_FACTION = -200,			-- The country has yet to ask some other faction it would prefer to be a part of.
JOIN_FACTION_BOTH_LOSING = -300,			-- Desire to be in a faction when both we and htey are in losing wars
DIFFERENT_FACTION_THREAT = 30,				-- Threat caused by not being in the same faction
DEMOCRATIC_AI_FACTION_KICKING_PLAYER_THREAT_DIFFERENCE = 0.0, -- World threat generation difference needed to kick a player from a democratic faction #Was 6