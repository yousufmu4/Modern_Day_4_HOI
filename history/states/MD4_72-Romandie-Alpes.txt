
state={
	id=72
	name="STATE_72"
	resources={
		aluminium=1.000
	}

	history={
		owner = SWI
		victory_points = {
			6666 5 
		}
		buildings = {
			infrastructure = 7
			industrial_complex = 4
			arms_factory = 2
			air_base = 4

		}
		add_core_of = SWI

	}

	provinces={
		3612 6666 6683 9622 11590 
	}
	manpower=2197526
	buildings_max_level_factor=1.000
	state_category=state_03
}
