
state={
	id=224
	name="STATE_224"
	
	resources={
		oil=1
	}

	history={
		owner = DAR
		victory_points = {
			1960 5 
		}
		victory_points = {
			10739 1 
		}
		victory_points = {
			8052 1 
		}
		victory_points = {
			5060 1 
		}
		victory_points = {
			11072 5
		}
		victory_points = {
			11073 5
		}
		buildings = {
			infrastructure = 1
			industrial_complex = 1
			arms_factory = 1

		}
		add_core_of = DAR
		add_claim_by = SUD
		add_claim_by = SRF
		
		SUD = {
			set_province_controller = 1960
			set_province_controller = 5060
			set_province_controller = 8052
			set_province_controller = 8103
			set_province_controller = 10739
			set_province_controller = 2
			set_province_controller = 11044
			set_province_controller = 11052
			set_province_controller = 11067
			set_province_controller = 11069
			set_province_controller = 11075
			set_province_controller = 11076
		}

	}

	provinces={
		1960 5060 8052 8103 10739 2 11044 11052 11067 11069 11072 11073 11075 11076 
	}
	manpower=9291770
	buildings_max_level_factor=1.000
	state_category=state_08
}
