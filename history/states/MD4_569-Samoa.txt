state= {
	id=569
	name="STATE_569"
	manpower = 306000
	state_category = state_01

	history={
		owner = PLY
		victory_points = { 7290 5 } #Apia
		
		buildings = {
			infrastructure = 3
			air_base = 1
			
			7290 = {
				naval_base = 1
			}

		}
		add_core_of = PLY
	}

	provinces={
		 7290 
		
		}
}
