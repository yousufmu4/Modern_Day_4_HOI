state= {
	id=550
	name="STATE_550"
	manpower = 1828885
	state_category = state_03

	history={
		owner = SWE
		victory_points = { 11215 1 } #Karlskrona
		victory_points = { 9279 5 } #Malmö
		victory_points = { 11160 1 } #Kristianstad
		victory_points = { 6345 1 } #Helsingborg
		victory_points = { 6412 1 } #Halmstad
		
		buildings = {
			infrastructure = 6
			industrial_complex = 2
			air_base = 3
			dockyard = 1
			
			11215 = {
				naval_base = 5
			}
			
			9279 = {
				naval_base = 5
			}
			
		}
		add_core_of = SWE
	}

	provinces={
		11215 11327 11160 6120 9300 11376 9279 6345 9362 6412 3307
		
		}
}
