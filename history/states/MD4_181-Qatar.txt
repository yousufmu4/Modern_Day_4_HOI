
state={
	id=181
	name="STATE_181"
	resources={
		oil=65.000
		aluminium=6.000
	}

	history={
		owner = QAT
		victory_points = {
			13388 5 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 3
			arms_factory = 4
			air_base = 10
			8051 = {
				naval_base = 10

			}

		}
		add_core_of = QAT

	}

	provinces={
		8051 13387 13388 
	}
	manpower=2376714
	buildings_max_level_factor=1.000
	state_category=state_04
}
