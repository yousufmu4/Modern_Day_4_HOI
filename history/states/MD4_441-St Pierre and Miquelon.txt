state= {
	id=441
	name="STATE_441"
	manpower = 6080
	state_category = state_00

	history={
		owner = FRA
		victory_points = { 11023 1 } #Saint-Pierre
		
		buildings = {
			infrastructure = 5
			373 = {
				naval_base = 1
			}
		}
		add_core_of = FRA
	}
	
	provinces={ 373 }
}
