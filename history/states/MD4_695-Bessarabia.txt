
state={
	id=695
	name="STATE_695"
	
	resources={
		oil=1
		steel=3
		aluminium=3
	}

	history={
		owner = UKR
		victory_points = {
			11683 5 
		}
		victory_points = {
			11670 5 
		}
		victory_points = {
			6597 3 
		}
		victory_points = {
			3704 3 
		}
		victory_points = {
			9714 1 
		}
		victory_points = {
			6727 1 
		}
		victory_points = {
			11546 1 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			dockyard = 2
			11670 = {
				naval_base = 5

			}

		}
		add_core_of = UKR

	}

	provinces={
		574 741 754 3575 3701 3704 3738 3755 3757 6597 6727 9573 9683 9698 9714 11546 11670 11683 11703 
	}
	manpower=3464484
	buildings_max_level_factor=1.000
	state_category=state_04
}
