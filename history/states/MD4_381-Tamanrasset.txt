state= {
	id=381
	name="STATE_381" 
	manpower = 769805 
	state_category = state_01
	
	resources={
		oil=8
	}

	history={
		owner = ALG
		victory_points = { 5095 1 } #Tamanrasset
		victory_points = { 10749 1 } #Illizi
		
		buildings = {
			infrastructure = 1
			air_base = 2
			
		}
		add_core_of = ALG 
		add_core_of = TUA
	}

	provinces={
		8061 1043 10749 5095 12849 1901
}