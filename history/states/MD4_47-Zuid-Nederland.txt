
state={
	id=47
	name="STATE_47" #
	manpower=4031241

	
	state_category = state_05
	
	resources={
		steel=4
	}

	history={
		owner = HOL
		victory_points = { 6496 5 } #Eindhoven
		victory_points = { 11562 2 } #Maastricht
		victory_points = { 3262 3 } #Breda
		
		buildings = {
			infrastructure = 7
			industrial_complex = 4
		}

	}

	provinces={
		3262 6496 6500 11562 9431 480
	}
}
