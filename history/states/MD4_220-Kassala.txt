state= {
	id=220
	name="STATE_220"
	manpower = 5606004
	state_category = state_06

	history={
		owner = SUD
		victory_points = { 2088 5 } #Kassala
		victory_points = { 12862 5 } #Gedaref
		
		buildings = {
			infrastructure = 1
			industrial_complex = 1
			
		}
		add_core_of = SUD
		add_core_of = SRF
	}

	provinces={
		2088 12862 10784 10743
}