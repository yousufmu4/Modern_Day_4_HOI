
state={
	id=729
	name="STATE_729"
	
	resources={
		oil=8
	}

	history={
		owner = TRK
		victory_points = {
			12333 5 
		}
		victory_points = {
			10485 3 
		}
		victory_points = {
			7491 1 
		}
		victory_points = {
			1593 3 
		}
		victory_points = {
			7477 3 
		}
		victory_points = {
			10359 1 
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 2
			arms_factory = 2
			air_base = 4

		}
		add_core_of = TRK

	}

	provinces={
		1493 1593 4563 4599 7477 7563 7618 7635 8099 10359 10406 10485 10510 12333 12359 12491 3662 3725 3744 3798 
	}
	manpower=3173409
	buildings_max_level_factor=1.000
	state_category=state_04
}
