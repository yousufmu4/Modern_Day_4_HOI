
state={
	id=188
	name="STATE_188"
	
	resources={
		oil=1
	}

	history={
		owner = SYR
		victory_points = {
			6302 5 
		}
		victory_points = {
			6297 3 
		}
		victory_points = {
			11124 2 
		}
		victory_points = {
			13344 1 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1

		}
		add_core_of = ISI
		add_core_of = NUS
		add_core_of = SYR
		add_core_of = FSA
		NUS = {
			set_province_controller = 11123
		}
		
	}

	provinces={
		1056 13344 6302 11118 11123 11124 
	}
	manpower=1120000
	buildings_max_level_factor=1.000
	state_category=state_02
}
