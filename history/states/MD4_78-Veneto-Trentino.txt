
state={
	id=78
	name="STATE_78"
	resources={
		steel=2.000
	}

	history={
		owner = ITA
		victory_points = {
			11584 5 
		}
		victory_points = {
			3604 5 
		}
		victory_points = {
			9598 1 
		}
		victory_points = {
			628 1 
		}
		victory_points = {
			6626 3 
		}
		victory_points = {
			11598 1 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 4
			dockyard = 1
			air_base = 3
			11584 = {
				naval_base = 6

			}

		}
		add_core_of = ITA

	}

	provinces={
		628 656 3604 3657 6626 6631 6656 6675 9598 9630 11584 11598 13272 13273 
	}
	manpower=7174777
	buildings_max_level_factor=1.000
	state_category=state_07
}
