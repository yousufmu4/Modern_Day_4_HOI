state= {
	id=318
	name="STATE_318"
	manpower = 947979
	state_category = state_02

	history={
		owner = EGU
		victory_points = { 8244 5 } #Bata
		
		buildings = {
			infrastructure = 2
			air_base = 1
			industrial_complex = 1
			
			8244 = {
				naval_base = 2
			}
			
		}
		add_core_of = EGU 
	}

	provinces={
		8244
}