
state={
	id=182
	name="STATE_182"
	resources={
		steel=5
		aluminium=2
	}

	history={
		owner = OMA
		victory_points = {
			10760 8 
		}
		victory_points = {
			4993 5 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 2
			arms_factory = 2
			dockyard = 1
			air_base = 1
			10760 = {
				naval_base = 6

			}

		}
		add_core_of = OMA

	}

	provinces={
		1947 4993 8020 10760 13377 13378 13379 13380 
	}
	manpower=3344189
	buildings_max_level_factor=1.000
	state_category=state_04
}
