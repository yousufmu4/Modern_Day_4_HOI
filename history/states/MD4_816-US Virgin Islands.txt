state= {
	id=816
	name="STATE_816"
	manpower = 106217
	state_category = state_00

	history={
		owner = USA
		victory_points = { 4155 1 } #Charlotte Amalie
		
		buildings = {
			infrastructure = 5
			
			4155 = {
				naval_base = 1
			}

		}
		add_core_of = USA
	}

	provinces={
		  4155
		
		}
}
