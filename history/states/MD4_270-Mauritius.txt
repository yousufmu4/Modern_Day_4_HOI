state= {
	id=270
	name="STATE_270"
	manpower = 1285000
	state_category = state_02

	history={
		owner = MRT
		victory_points = { 13018 5 } #Port Louis
		
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			air_base = 3
			
			13018 = {
				naval_base = 2
			}

		}
		add_core_of = MRT
	}

	provinces={
		 13018
		
		}
}
