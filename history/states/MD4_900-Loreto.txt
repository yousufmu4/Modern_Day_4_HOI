
state={
	id=900
	name="STATE_900"
	
	resources={
		oil=5
		tungsten=3
	}

	history={
		owner = PRU
		victory_points = {
			9926 5 
		}
		victory_points = {
			5162 3 
		}
		victory_points = {
			9589 1 
		}
		buildings = {
			infrastructure = 1
            industrial_complex = 1
			air_base = 1

		}
		add_core_of = PRU

	}

	provinces={
		5162 10942 10970 9528 9589 9639 9733 9895 9926 10143 
	}
	manpower=2406456
	buildings_max_level_factor=1.000
	state_category=state_03
}
