
state={
	id=310
	name="STATE_310"
	
	resources={
		tungsten=6
	}

	history={
		owner = DRC
		victory_points = {
			10858 2 
		}
		victory_points = {
			3748 1 
		}
		buildings = {
			infrastructure = 0

		}
		add_core_of = DRC

	}

	provinces={
		7161 10766 10858 3748
	}
	manpower=2960400
	buildings_max_level_factor=1.000
	state_category=state_04
}
