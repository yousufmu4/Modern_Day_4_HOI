state= {
	id=540
	name="STATE_540" 
	manpower = 25660000
	state_category = state_12

	history={
		owner = CHI
		victory_points = { 7014 15 } #Shanghai
		
		buildings = {
			infrastructure = 7
			industrial_complex = 4
			dockyard = 4
			air_base = 10
			
			12076 = {
				naval_base = 10
			}

		}
		add_core_of = CHI
	}

	provinces={
		12076 7014
		
		}
}
