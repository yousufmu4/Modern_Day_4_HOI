
state={
	id=166
	name="STATE_166"
	resources={
		oil=5.000
	}

	history={
		owner = ISI
		victory_points = {
			10106 8 
		}
		victory_points = {
			1938 1 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 2
			arms_factory = 2
			air_base = 2

		}
		add_core_of = IRQ
		add_core_of = ISI
		
		IRQ = {
			set_province_controller = 11369
			set_province_controller = 11498
		}

	}

	provinces={
		1938 10106 12372 11369 11498 
	}
	manpower=2906278
	buildings_max_level_factor=1.000
	state_category=state_04
}
