
state={
	id=498
	name="STATE_498"
	
	resources={
		tungsten=2
	}

	history={
		owner = BRM
		victory_points = {
			7974 5 
		}
		victory_points = {
			1170 5 
		}
		victory_points = {
			7603 1 
		}
		victory_points = {
			1566 1 
		}
		victory_points = {
			2061 1 
		}
		buildings = {
			infrastructure = 0
			air_base = 1
			industrial_complex = 1

		}

	}

	provinces={
		1170 1566 1607 2061 7562 7603 130 135 
	}
	manpower=3321387
	buildings_max_level_factor=1.000
	state_category=state_04
}
