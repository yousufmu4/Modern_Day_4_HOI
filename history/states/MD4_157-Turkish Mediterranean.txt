
state={
	id=157
	name="STATE_157"
	manpower = 7134825
	
	state_category = state_07
	
	resources={
		steel=12
	}

	history={
		owner = TUR
		victory_points = { 11746 10 } #Adana
		victory_points = { 1119 5 } #Antalya
		victory_points = { 10056 5 } #Mersin
		victory_points = { 4127 1 } #Alanya
		victory_points = { 806 1 } #Osmaniye
		victory_points = { 1122 1 } #Manavgat
		victory_points = { 12096 1 } #Fethiye
		
		buildings = {
			infrastructure = 4
			industrial_complex = 2
			arms_factory = 1
			dockyard = 1
			1119 = {
				naval_base = 6
			}
		}
		add_core_of = TUR
	}

	provinces={
		1005 1119 9941 10122 11917 12013 12016 12096 
		3876 784 3788 6894
		1122 4008 4124 4127 7026 7143 10056
		806 886 3921 6956 11746 11810
		
	}
}