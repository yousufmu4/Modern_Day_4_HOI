state= {
	id=701
	name="STATE_701"
	manpower = 3456023
	state_category = state_04

	history={
		owner = UKR

		victory_points = { 577 5 } #Chernivtsi
		victory_points = { 11411 5 } #Ivano-Frankivsk
		victory_points = { 11691 3 } #Uzhhorod
		victory_points = { 3548 3 } #Mukachevo
		victory_points = { 11550 3 } #Kolomyia
		victory_points = { 6571 1 } #Khust

		buildings = {
			infrastructure = 3
			industrial_complex = 1

		}
		add_core_of = UKR
	}

	provinces={
		  3407 577 11550 3743 11411 9425 6571 9563 6460 3548 11691 11536 9548
		
		}
}


