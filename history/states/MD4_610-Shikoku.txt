
state={
	id=610
	name="STATE_610"
	
	resources={
		aluminium=3
	}

	history={
		owner = JAP
		victory_points = {
			4179 5 
		}
		victory_points = {
			4094 3 
		}
		victory_points = {
			7197 3 
		}
		victory_points = {
			9965 2 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 3
			arms_factory = 2

		}
		add_core_of = JAP

	}

	provinces={
		4094 4179 7046 7197 9965 11997 12028 6161 
	}
	manpower=3547120
	buildings_max_level_factor=1.000
	state_category=state_04
}
