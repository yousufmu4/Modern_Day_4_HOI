state= {
	id=431
	name="STATE_431"
	manpower = 26215960
	state_category = state_12
	
	resources={
}

	history={
		owner = RAJ
		victory_points = { 2086 10 } #Delhi
		
		buildings = {
			infrastructure = 4
			industrial_complex = 3
			arms_factory = 1
			air_base = 8

		}
		add_core_of = RAJ
	}

	provinces={
		2086
		
		}
}