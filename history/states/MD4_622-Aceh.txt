
state={
	id=622
	name="STATE_622"
	
	resources={
		tungsten=6
	}

	history={
		owner = IND
		victory_points = {
			7658 5 
		}
		victory_points = {
			10509 2 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			arms_factory = 1
			7658 = {
				naval_base = 2

			}

		}
		add_core_of = IND

	}

	provinces={
		7658 10509 12488 6112 6113 
	}
	manpower=4981529
	buildings_max_level_factor=1.000
	state_category=state_05
}
