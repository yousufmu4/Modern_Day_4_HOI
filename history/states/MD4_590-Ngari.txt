state= {
	id=590
	name="STATE_590"
	manpower = 420846
	state_category = state_01

	history={
		owner = CHI
		
		victory_points = { 10918 1 } #Nagqu
		victory_points = { 1961 1 } #Gar
		victory_points = { 1991 1 } #Zhongba

		
		buildings = {
			infrastructure = 1
			
		}
		add_core_of = CHI
		add_core_of = TIB
	}

	provinces={
	    5080 10918 10755 5011 8068 12801 10802 1961 8029 1991 8121 4994 7926 2093 2010 5094 12784 
		
		}
}
