
state={
	id=433
	name="STATE_433"
	
	resources={
		steel=4
		tungsten=1
	}

	history={
		owner = RAJ
		victory_points = {
			12876 5 
		}
		victory_points = {
			4984 5 
		}
		victory_points = {
			12041 1 
		}
		victory_points = {
			7998 1 
		}
		buildings = {
			infrastructure = 2
			industrial_complex = 2

		}
		add_core_of = RAJ

	}

	provinces={
		4984 7998 10847 10866 12041 12718 12844 12876 12886 11170 11852 12094 13257 13415 6 15 45 
	}
	manpower=22869735
	buildings_max_level_factor=1.000
	state_category=state_12
}
