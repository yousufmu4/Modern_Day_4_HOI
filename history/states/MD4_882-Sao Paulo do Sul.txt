state= {
	id=882
	name="STATE_882"
	manpower = 31296364
	state_category = state_13
	
	resources={
		aluminium=2
	}

	history={
		owner = BRA
		victory_points = { 10513 20 } #São Paulo
		victory_points = { 5112 5 } #Santos
		victory_points = { 10432 5 } #São José dos Campos
		victory_points = { 10947 1 } #Itapetininga
		victory_points = { 11008 1 } #Itapeva
		victory_points = { 5167 1 } #Peruíbe
		victory_points = { 7553 1 } #Registro
		
		buildings = {
			infrastructure = 6
			industrial_complex = 6
			arms_factory = 5
			air_base = 8
			
			5112 = {
				naval_base = 2
			}

		}
		add_core_of = BRA
	}

	provinces={
		  7553 11008 10947 5167 5112 10513 10432
		
		}
}
