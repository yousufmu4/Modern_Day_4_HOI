state= {
	id=618
	name="STATE_618"
	manpower = 1416587
	state_category = state_02
	
	resources={
		oil=3
	}

	history={
		owner = JAP
		victory_points = { 11932 5 } #Naha
		
		
		
		buildings = {
			infrastructure = 6
			industrial_complex = 2
			air_base = 4
			
			11932 = {
				naval_base = 4
			}

		}
		add_core_of = JAP
	}

	provinces={
		1123 10124 11932
		
		}
}
