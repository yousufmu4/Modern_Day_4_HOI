﻿capital = 652

oob = "SOV_2017"



add_ideas = {
	pop_050
	russian_legacy
	orthodox_christian
	unrestrained_corruption
	rentier_state
    export_economy
	sco_member
	gdp_6
	p5_member
	debt_0
	recession
	bud_neg_04
	defence_05
	edu_03
	health_03
	social_04
	bureau_03
	police_04
	partial_draft_army
	volunteer_women
	Fossil_fuel_industry
	oligarchs
	intelligence_community
	intervention_regional_interventionism
	large_far_right_movement
	civil_law
}
set_country_flag = bud_neg_04
set_country_flag = debt_0
set_country_flag = gdp_6

add_opinion_modifier = { target = SAU modifier = Arabian_Peninsula_Russia_Economical_Relations }
add_opinion_modifier = { target = UAE modifier = Arabian_Peninsula_Russia_Economical_Relations }
add_opinion_modifier = { target = QAT modifier = Arabian_Peninsula_Russia_Economical_Relations }

create_faction = EURASIAN_UNION

add_to_faction = SOV
add_to_faction = BLR
add_to_faction = ARM
add_to_faction = KAZ
add_to_faction = KYR

if = {
	limit = { has_dlc = "Together for Victory" }
	SOV = { add_to_tech_sharing_group = Eurasion_Union_Tech_Share }
	BLR = { add_to_tech_sharing_group = Eurasion_Union_Tech_Share }
	ARM = { add_to_tech_sharing_group = Eurasion_Union_Tech_Share }
	KAZ = { add_to_tech_sharing_group = Eurasion_Union_Tech_Share }
	KYR = { add_to_tech_sharing_group = Eurasion_Union_Tech_Share }
	
}
diplomatic_relation = {
	country = SOO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ABK
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CHE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = NOV
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = PMR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = TAJ
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SYR
	relation = military_access
	active = yes
}

set_technology = { 
 legacy_doctrines = 1 
 armoured_mass_assault = 1 
 deep_echelon_advance = 1 
 army_group_operational_freedom = 1 
 massed_artillery = 1

    infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1 #2005
	
	
	
	
	
	 #2005
	
	combat_eng_equipment = 1
	
	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1 #1985
	
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1 #2005
	
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	land_Drone_equipment2 = 1 #2005
	
	Early_APC = 1 #Vehicle Design
	
	APC_1 = 1
	APC_2 = 1
	APC_3 = 1
	APC_4 = 1
	APC_5 = 1 #2005
	#APC_6 = 1 #2015
	
	 #1965
	 #1975
	 #1985
	 #1995
	 #2005
	
	Air_APC_2 = 1 #1975
	#Air_APC_6 = 1 #2015
	
	IFV_1 = 1
	IFV_2 = 1
	IFV_3 = 1
	IFV_4 = 1
	IFV_5 = 1 #2005
	#IFV_6 = 1 #2015
	
	Air_IFV_1 = 1
	Air_IFV_3 = 1
	Air_IFV_4 = 1
	Air_IFV_5 = 1
	
	MBT_1 = 1
	MBT_2 = 1
	MBT_3 = 1
	MBT_4 = 1 
	MBT_5 = 1 #2015
	
	ENG_MBT_1 = 1
	ENG_MBT_2 = 1
	ENG_MBT_5 = 1 #2015
	
	Rec_tank_0 = 1 #1965
	Rec_tank_1 = 1 #1965
	Rec_tank_2 = 1 #2005
	Rec_tank_3 = 1 #2015
	
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	util_vehicle_equipment_3 = 1
	util_vehicle_equipment_4 = 1 #2005
	
	gw_artillery = 1
	Arty_upgrade_1 = 1
	SP_arty_equipment_0 = 1
	SP_R_arty_equipment_0 = 1
	artillery_1 = 1
	SP_arty_equipment_1 = 1
	SP_R_arty_equipment_1 = 1
	SP_arty_equipment_2 = 1
	SP_R_arty_equipment_2 = 1
	Arty_upgrade_3 = 1
	
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	AT_upgrade_2 = 1
	Anti_tank_1 = 1
	Heavy_Anti_tank_1 = 1
	Heavy_Anti_tank_2 = 1
	Anti_tank_2 = 1
	
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	Anti_Air_1 = 1
	SP_Anti_Air_1 = 1
	AA_upgrade_2 = 1
	Anti_Air_2 = 1
	SP_Anti_Air_2 = 1
	
	early_fighter = 1
	Strike_fighter1 = 1
	Strike_fighter2 = 1
	Strike_upgrade_1 = 1
	Strike_fighter3 = 1 #4.5gen
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1 #4.5gen
	AS_Fighter1 = 1
	AS_Fighter2 = 1
	AS_upgrade_1 = 1
	AS_Fighter3 = 1 #4.5gen
	Int_Fighter1 = 1 #4th gen
	CV_MR_Fighter1 = 1
	CV_MR_Fighter2 = 1
	CV_MR_Fighter3 = 1 #4.5gen
	
	L_Strike_fighter1 = 1
    L_Strike_fighter2 = 1
	L_Strike_fighter3 = 1

	N_B_Cruiser_2 = 1
	modern_carrier_0 = 1
	modern_carrier_1 = 1
	modern_carrier_2 = 1
	Nuclear_carrier_1 = 1
	Nuclear_carrier_0 = 1
	cruiser_1 = 1
	cruiser_2 = 1
	missile_cruiser_1 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	corvette_1 = 1
	corvette_2 = 1
	missile_corvette_1 = 1
	missile_corvette_2 = 1
	missile_corvette_3 = 1
	missile_corvette_4 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_3 = 1
	missile_submarine_5 = 1
	missile_submarine_3 = 1
	missile_submarine_2 = 1
	missile_submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	attack_submarine_4 = 1
	attack_submarine_5 = 1
	diesel_attack_submarine_1 = 1
	diesel_attack_submarine_2 = 1
	diesel_attack_submarine_3 = 1
	diesel_attack_submarine_4 = 1
	
	early_helicopter = 1
	attack_helicopter1 = 1
	attack_helicopter2 = 1
	attack_helicopter3 = 1
	
	transport_helicopter1 = 1
	transport_helicopter2 = 1
	transport_helicopter3 = 1
	transport_helicopter4 = 1
	
	landing_craft = 1
	
	early_bomber = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	
	transport_plane1 = 1
	transport_plane2 = 1
	transport_plane3 = 1
	
	naval_plane1 = 1
	naval_plane2 = 1
	
	cas1 = 1
	cas2 = 1
	
}

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot
	complete_national_focus = Generic_30_IC_slot
	complete_national_focus = Generic_50_IC_slot
	complete_national_focus = Generic_85_IC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 5
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 73
		}
		
		neutrality = {
			popularity = 10
		}
		
		Nationalist = {
			popularity = 12
		}
	}
	
	ruling_party = communism
	last_election = "2012.3.4"
	election_frequency = 72
	elections_allowed = yes
}


set_convoys = 200


create_country_leader = {
	name = "Said Kharakansky"
	picture = "Sal_Said_Kharakansky.dds"
	expire = "2050.3.1"
	ideology = Caliphate
	traits = {
		
	}
}

create_country_leader = {
	name = "Garry Kasparov"
	picture = "Western_Garry_Kasparov.dds"
	expire = "2050.3.1"
	ideology = liberalism
	traits = {
		western_liberalism
	}
}

create_country_leader = {
	name = "Alexei Navalny"
	picture = "Western_Alexei_Navalny.dds"
	expire = "2050.3.1"
	ideology = liberalism
	traits = {
		western_liberalism
	}
}

create_country_leader = {
	name = "Emilia Slabunova"
	picture = "Neutral_Emilia_Slabunova.dds"
	expire = "2050.3.1"
	ideology = neutral_Social
	traits = {
		neutrality_neutral_Social
	}
}

create_country_leader = {
	name = "Pavel Grudinin"
	picture = "COM_Pavel_Grudinin.dds"
	expire = "2050.3.1"
	ideology = Neutral_Communism
	traits = {
		neutrality_Neutral_Communism
	}
}

create_country_leader = {
	name = "Vladimir Zhirinovsky"
	picture = "Nationalist_Vladimir_Zhirinovsky.dds"
	expire = "2050.3.1"
	ideology = Nat_Populism
	traits = {
		nationalist_Nat_Populism
	}
}

#lol, who knows
create_country_leader = {
	name = "Natalia Poklonskaya"
	picture = "Natalia_Poklonskaya.dds"
	expire = "2060.3.1"
	ideology = Conservative
	traits = {
		emerging_Conservative
	}
}

create_country_leader = {
	name = "Alexey Dyumin"
	picture = "Alexey_Dyumin.dds"
	expire = "2050.3.1"
	ideology = Conservative
	traits = {
		emerging_Conservative
	}
}

create_country_leader = {
	name = "Dimitry Medvedev"
	picture = "Dimitry_Medvedev.dds"
	expire = "2040.3.1"
	ideology = Conservative
	traits = {
		emerging_Conservative
	}
}

create_country_leader = {
	name = "Vyacheslav Volodin"
	picture = "Vyacheslav_Volodin.dds"
	expire = "2050.3.1"
	ideology = Conservative
	traits = {
		emerging_Conservative
	}
}

create_country_leader = {
	name = "Vladimir Putin"
	picture = "Vladimir_Putin.dds"
	expire = "2028.3.1"
	ideology = Conservative
	traits = {
		spy
		emerging_Conservative
		geopolitical_thinker
		political_dancer
		likeable
		sly
		rational
	}
}

add_namespace = {
	name = "unit_leader_sov"
	type = unit_leader
}

create_field_marshal = {
	name = "Pavel Popov"
		picture = "Gen_Popov.dds"
	traits = { defensive_doctrine }
	skill = 1

}

create_field_marshal = {
	name = "Dmitry Bulgakov"
		picture = "Gen_Dmitry_Bulgakov.dds"
	traits = { defensive_doctrine }
	skill = 1

}

create_corps_commander = {
	name = "Aleksandr Galkin"
		picture = "Gen_Aleksandr_Galkin.dds"
	traits = { winter_specialist }
	skill = 1

}

create_field_marshal = {
	name = "Andrey Kartapolov"
		picture = "Gen_Andrey_Kartapolov.dds"
	traits = { offensive_doctrine logistics_wizard }
	skill = 2

}

create_corps_commander = {
	name = "Igor Korobov"
		picture = "Gen_Igor_Korobov.dds"
	traits = { winter_specialist }
	skill = 1

}

create_corps_commander = {
	name = "Sergey Surovikin"
		picture = "Gen_Sergey_Surovikin.dds"
	traits = { winter_specialist }
	skill = 1

}

create_corps_commander = {
	name = "Vladimir Zarudnitsky"
		picture = "Gen_Vladimir_Zarudnitsky.dds"
	traits = { winter_specialist }
	skill = 1

}

create_corps_commander = {
	name = "Oleg Salyukov"
		picture = "Gen_Oleg_Salyukov.dds"
	traits = { winter_specialist }
	skill = 1

}

create_field_marshal = {
	name = "Valery Gerasimov"
		picture = "Gen_Valery_Gerasimov.dds"
	traits = { defensive_doctrine logistics_wizard }
	skill = 2

}

create_field_marshal = {
	name = "Nikolay Pankov"
		picture = "Gen_Nikolay_Pankov.dds"
	traits = { defensive_doctrine }
	skill = 1

}

create_corps_commander = {
	name = "Yuriy Sadovenko"
		picture = "Gen_Yuriy_Sadovenko.dds"
	traits = { winter_specialist }
	skill = 1

}

create_corps_commander = {
	name = "Igor Borisovich Tymofeyev"
		picture = "Gen_Igor_Borisovich_Tymofeyev.dds"
	traits = { trait_mountaineer commando }
	skill = 3

}
create_corps_commander = {
	name = "Vadym Ivanovych Pankov"
		picture = "Gen_Vadym_Ivanovych_Pankov.dds"
	traits = { commando desert_fox }
	skill = 4

}
create_corps_commander = {
	name = "Vladimir Anatolievich Kochetkov"
		picture = "Gen_Kochetkov_Vladimir_Anatolievich.dds"
	traits = { commando ranger }
	skill = 2

}
create_corps_commander = {
	name = "Aleksandr Ivanovich Lentsov"
		picture = "Gen_Aleksandr_Ivanovich_Lentsov.dds"
	traits = { commando urban_assault_specialist }
	skill = 3

}

create_corps_commander = {
	name = "Aleksej Vladimirovich Zavizion"
		picture = "Gen_Aleksej_Vladimirovich_Zavizion.dds"
	traits = { panzer_leader ranger }
	skill = 2

}

create_corps_commander = {
	name = "Andrey Leonidovich Krasov"
		picture = "Gen_Andrey_Leonidovich_Krasov.dds"
	traits = { commando winter_specialist }
	skill = 2

}

create_corps_commander = {
	name = "Gennady Anashkin"
		picture = "Gen_Gennady_Anashkin.dds"
	traits = { commando trickster }
	skill = 2

}

create_corps_commander = {
	name = "Vladimir Shamanov"
		picture = "Gen_Vladimir_Shamanov.dds"
	traits = { commando panzer_leader }
	skill = 3

}

create_corps_commander = {
	name = "Viktor Goremykin"
		picture = "Gen_Viktor_Goremykin.dds"
	traits = { trickster }
	skill = 2

}

create_corps_commander = {
	name = "Mikhail Stepanovich Zusko"
		picture = "Gen_Mikhail_Stepanovich_Zusko.dds"
	traits = { trait_mountaineer hill_fighter }
	skill = 2

}

create_corps_commander = {
	name = "Alexey Naumets"
		picture = "Gen_Alexey_Naumets.dds"
	traits = { commando panzer_leader }
	skill = 2

}

create_corps_commander = {
	name = "Andrei Viktorovich Guruliov"
		picture = "Gen_Andrei_Viktorovich_Guruliov.dds"
	traits = { urban_assault_specialist panzer_leader }
	skill = 3

}

create_corps_commander = {
	name = "Oleg Mussovich Tsekov"
		picture = "Gen_Oleg_Mussovich_Tsekov.dds"
	traits = { panzer_leader winter_specialist }
	skill = 3

}

create_corps_commander = {
	name = "Sergej Nikolayevich Volyk"
		picture = "Gen_Sergej_Nikolayevich_Volyk.dds"
	traits = { urban_assault_specialist trickster }
	skill = 2

}

create_navy_leader = {
	name = "Vladimir Korolev"
		picture = "Adm_Vladimir_Korolev.dds"
	traits = { }
	skill = 2
}

create_navy_leader = {
	name = "Aleksandr Viktorovich Vitko"
		picture = "Adm_Aleksandr_Viktorovich_Vitko.dds"
	traits = { }
	skill = 3
}

create_navy_leader = {
	name = "Nikolay Yevmenov"
		picture = "Adm_Nikolay_Yevmenov.dds"
	traits = { superior_tactician }
	skill = 3
}

create_navy_leader = {
	name = "Sergei Avakyants"
		picture = "Adm_Sergei_Avakyants.dds"
	traits = { }
	skill = 1
}

create_navy_leader = {
	name = "Viktor Nikolayevich Mardusin"
		picture = "Adm_Viktor_Nikolayevich_Mardusin.dds"
	traits = { }
	skill = 1
}

create_navy_leader = {
	name = "Alexander Nosatov"
		picture = "Adm_Alexander_Nosatov.dds"
	traits = { }
	skill = 2
}

create_navy_leader = {
	name = "Viktor Petrovich Kravchuk"
		picture = "Adm_Viktor_Petrovich_Kravchuk.dds"
	traits = { }
	skill = 2
}

#### Naval Variants ####

create_equipment_variant = {
    name = "Grisha-Class"
    type = corvette_2
    upgrades = {
        corvette_asw_upgrade = 1
		corvette_ground_support = 1
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Parchim-Class"
    type = corvette_2
    upgrades = {
        corvette_asw_upgrade = 1
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Koni-Class"
    type = corvette_2
    upgrades = {
        corvette_vls_upgrade = 0
		corvette_asw_upgrade = 2
		corvette_cm_upgrade = 0
		corvette_radar_upgrade = 0
		corvette_stealth_upgrade = 0
		corvette_ground_support = 0
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Pauk-Class"
    type = missile_corvette_1
    upgrades = {
        corvette_vls_upgrade = 0
		corvette_asw_upgrade = 2
		corvette_cm_upgrade = 0
		corvette_radar_upgrade = 0
		corvette_stealth_upgrade = 0
		corvette_ground_support = 0
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Bora-Class"
    type = missile_corvette_2
    upgrades = {
        corvette_ground_support = 1
		corvette_vls_upgrade = 1
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Buyan-M-Class"
    type = missile_corvette_3
    upgrades = {
        corvette_cm_upgrade = 1
		corvette_vls_upgrade = 1
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Steregushchiy-Class"
    type = missile_corvette_3
    upgrades = {
        corvette_cm_upgrade = 2
		corvette_vls_upgrade = 3
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Gremyashchiy-Class"
    type = missile_corvette_3
    upgrades = {
        corvette_cm_upgrade = 2
		corvette_vls_upgrade = 4
		corvette_asw_upgrade = 2
		corvette_cm_upgrade = 1
		corvette_radar_upgrade = 1
    }
	obsolete = yes
}

create_equipment_variant = {
    name = "Karakurt-Class"
    type = missile_corvette_3
    upgrades = {
        corvette_cm_upgrade = 1
		corvette_vls_upgrade = 4
		corvette_stealth_upgrade = 2
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Sierra-II-Class"
    type = attack_submarine_1
    upgrades = {
        att_sub_sonar = 1
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Victor-III-Class"
    type = attack_submarine_2
    upgrades = {
        att_sub_stealth = 2
		att_sub_displacement = 1
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Akula II-Class"
    type = attack_submarine_3
    upgrades = {
       att_sub_displacement = 0
		att_sub_sonar = 2
		att_sub_stealth = 2
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Improved-Kilo-Class"
    type = diesel_attack_submarine_3
    upgrades = {
        D_att_sub_stealth = 2
		D_att_sub_displacement = 1
        }
		
	obsolete = yes
    }

create_equipment_variant = {
    name = "Oscar II-Class"
    type = missile_submarine_3
    upgrades = {
        SSGN_displacement = 1
		SSGN_stealth = 1
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Sovremennyy-Class"
    type = destroyer_2
    upgrades = {
        destroyer_vls_upgrade = 3
		destroyer_cm_upgrade = 1
		destroyer_radar_upgrade = 2
    }
	obsolete = yes
}
	
create_equipment_variant = {
    name = "Sovremennyy EM-Class"
    type = destroyer_2
    upgrades = {
        destroyer_vls_upgrade = 5
		destroyer_cm_upgrade = 2
		destroyer_radar_upgrade = 3
    }
}
	
create_equipment_variant = {
    name = "Udaloy-Class"
    type = destroyer_2
    upgrades = {
		destroyer_asw_upgrade = 3
		destroyer_cm_upgrade = 1
		destroyer_radar_upgrade = 1
    }
	obsolete = yes
}

create_equipment_variant = {
    name = "Udaloy II-Class"
    type = missile_destroyer_1
    upgrades = {
		destroyer_asw_upgrade = 3
		destroyer_cm_upgrade = 1
		destroyer_radar_upgrade = 1
    }
	obsolete = yes
}

create_equipment_variant = {
    name = "Kirov mod-Class"
    type = N_B_Cruiser_2
    upgrades = {
		cruiser_vls_upgrade = 3
		cruiser_radar_upgrade = 2
    }
	obsolete = yes
}

#### Land Equipment Variants ####

create_equipment_variant = {
	name = "T-62"
	type = MBT_Equipment_1
	upgrades = {
		tank_gun_upgrade = 2
		tank_armor_upgrade = 1
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "T-64"
	type = MBT_Equipment_1
	upgrades = {
		tank_reliability_upgrade = 1
		tank_engine_upgrade = 1
		tank_armor_upgrade = 2
		tank_gun_upgrade = 3
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "T-72B"
	type = MBT_Equipment_2
	upgrades = {
			tank_engine_upgrade = 2
			tank_armor_upgrade = 3
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "T-72B3"
	type = MBT_Equipment_2
	upgrades = {
			tank_engine_upgrade = 2
			tank_armor_upgrade = 4
			tank_gun_upgrade = 2
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "T-72 SIM-1"
	type = MBT_Equipment_2
	upgrades = {
			tank_engine_upgrade = 0
			tank_armor_upgrade = 2
			tank_gun_upgrade = 2
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "T-90A Vladimir"
	type = MBT_Equipment_4
	upgrades = {
		tank_engine_upgrade = 1
		tank_armor_upgrade = 0
		tank_gun_upgrade = 1
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "T-90AM"
	type = MBT_Equipment_4
	upgrades = {
		tank_engine_upgrade = 2
		tank_armor_upgrade = 1
		tank_gun_upgrade = 2
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "T-90M"
	type = MBT_Equipment_4
	upgrades = {
		tank_engine_upgrade = 2
		tank_armor_upgrade = 2
		tank_gun_upgrade = 3
	}
}

create_equipment_variant = {
	name = "BTR-82"
	type = APC_Equipment_4
	upgrades = {
			tank_engine_upgrade = 2
			tank_armor_upgrade = 2
		}
}

create_equipment_variant = {
	name = "AT-7 Metis"
	type = L_AT_Equipment_1
	upgrades = {
			L_AT_Fire_Control = 1
			L_AT_Weight = 3
		}
		obsolete = yes
}

create_equipment_variant = {
	name = "AT-6 Spiral"
	type = H_AT_Equipment_1
	upgrades = {
			H_AT_Fire_Control = 1
			H_AT_Warhead = 1
		}
		obsolete = yes
}

create_equipment_variant = {
	name = "SA-25 Verba"
	type = AA_Equipment_2
	upgrades = {
			AA_Fire_Control = 3
			AA_Warhead = 1
			AA_Weight = 1
		}
}

create_equipment_variant = {
	name = "ZSU-23-4 Shilka"
	type = SP_AA_Equipment_0
	upgrades = {
			SP_AA_Fire_Control = 1
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "SA-6 Gainful"
	type = SP_AA_Equipment_0
	upgrades = {
			SP_AA_Fire_Control = 1
			SP_AA_Warhead = 1
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "SA-9 Strela-1"
	type = SP_AA_Equipment_0
	upgrades = {
			SP_AA_Fire_Control = 1
			SP_AA_Warhead = 2
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "S-125 Pechora"
	type = SP_AA_Equipment_0
	upgrades = {
			SP_AA_Fire_Control = 1
			SP_AA_Warhead = 2
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "SA-13 Strela-10"
	type = SP_AA_Equipment_1
	upgrades = {
			SP_AA_Fire_Control = 1
			SP_AA_Warhead = 3
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "SA-15 Tor-M"
	type = SP_AA_Equipment_1
	upgrades = {
			SP_AA_Fire_Control = 3
			SP_AA_Warhead = 3
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "SA-22 Pantsir"
	type = SP_AA_Equipment_2
	upgrades = {
			SP_AA_Fire_Control = 3
			SP_AA_Warhead = 1
		}
}

create_equipment_variant = {
	name = "9P138" 
	type = SP_R_arty_equipment_1
	upgrades = {
			SP_Arty_Warhead  = 0
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "TOS-1" #Termobaric
	type = SP_R_arty_equipment_1
	upgrades = {
			SP_Arty_Warhead  = 4
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "BM-30 Smerch"
	type = SP_R_arty_equipment_1
	upgrades = {
	        SP_Arty_Fire_Control = 2
			SP_Arty_Warhead  = 2
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "2S5 Giatsint-S"
	type = SP_arty_equipment_0
	upgrades = {
			SP_Arty_Fire_Control = 3
			SP_Arty_Warhead = 2
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "2S1 Gvozdika"
	type = SP_arty_equipment_0
	upgrades = {
			SP_Arty_Fire_Control = 1
			SP_Arty_Warhead = 1
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "2S7 Pion"
	type = SP_arty_equipment_0
	upgrades = {
			SP_Arty_Fire_Control = 1
			SP_Arty_Warhead = 3
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "2S9 Nona"
	type = SP_arty_equipment_0
	upgrades = {
			SP_Arty_Fire_Control = 4
		}
	obsolete = yes
}

create_equipment_variant = {
	name = "2S35 Koalitsiya-SV"
	type = SP_arty_equipment_2
	upgrades = {
			SP_Arty_Fire_Control = 3
			SP_Arty_Warhead = 3
		}
}

create_equipment_variant = {
	name = "2S31 Vena"
	type = SP_arty_equipment_2
	upgrades = {
			SP_Arty_Fire_Control = 5
		}
}

create_equipment_variant = {
	name = "2S31 Vena"
	type = artillery_equipment_0
	upgrades = {
		L_Arty_Fire_Control = 1
		L_Arty_Warhead  = 1
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "BRDM-2"
	type = APC_Equipment_1
	upgrades = {
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "BTR-50"
	type = APC_Equipment_1
	upgrades = {
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "BRDM-2"
	type = Rec_tank_Equipment_0
	upgrades = {
	}
	obsolete = yes
}

#### Air Variants ####
create_equipment_variant = {
	name = "MiG-25 Foxbat"
	type = AS_Fighter_equipment_1
	upgrades = {
			plane_engine_upgrade = 3
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "Su-30 Flanker-C"
	type = AS_Fighter_equipment_2
	upgrades = {
			plane_range_upgrade = 2
			plane_engine_upgrade = 1
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "MiG-27 Flogger"
	type = Strike_fighter_equipment_2
	upgrades = {
		strike_gun_upgrade = 1
		plane_range_upgrade = 1
		plane_engine_upgrade = 1
		plane_reliability_upgrade = 0
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "Kamov Ka-52 Alligator"
	type = attack_helicopter_equipment_3
	upgrades = {
	}
}

create_equipment_variant = {
	name = "Mil Mi-14"
	type = transport_helicopter_equipment_1
	upgrades = {
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "Mil Mi-17"
	type = transport_helicopter_equipment_1
	upgrades = {
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "Kazan Ansat"
	type = transport_helicopter_equipment_4
	upgrades = {
	}
	obsolete = yes
}
create_equipment_variant = {
	name = "Mil Mi-6"
	type = transport_helicopter_equipment_1 #"Mil Mi-6
	upgrades = {
			
	}
	obsolete = yes
}
create_equipment_variant = {
	name = "Su-30MK2"
	type = AS_Fighter_equipment_2 #SSu-30MK2
	upgrades = {
			
	}
	obsolete = yes
}
create_equipment_variant = {
	name = "Su-22 Fitter"
	type = Strike_fighter_equipment_1 #Su-22 Fitter
	upgrades = {
			
	}
	obsolete = yes
}
create_equipment_variant = {
	name = "GAZ Vodnik"
	type = util_vehicle_equipment_3 #GAZ Vodnik
	upgrades = {
			
	}
	obsolete = yes
}

