﻿capital = 693

oob = "NOV_2017"


# Starting tech
set_technology = { 
	legacy_doctrines = 1 
	modern_blitzkrieg = 1 
	forward_defense = 1 
	encourage_nco_iniative = 1 
	air_land_battle = 1
	night_vision_1 = 1
	
	#For templates
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	land_Drone_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	SP_R_arty_equipment_0 = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
	MBT_1 = 1
	ENG_MBT_1 = 1
}

set_convoys = 20


add_ideas = {
	pop_050
	unrestrained_corruption
	orthodox_christian
	gdp_3
	debt_0
	stable_growth
	bud_neg_04
	defence_06
	edu_02
	health_02
	social_02
	bureau_03
	police_03
	volunteer_army
	volunteer_women
	the_military
	intelligence_community
	oligarchs
	civil_law
}
set_country_flag = bud_neg_04
set_country_flag = debt_0
set_country_flag = gdp_3

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 2
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 91
			#banned = no #default is no
		}
		neutrality = { 
			popularity = 7
		}
	}
	
	ruling_party = communism
	last_election = "2016.12.11"
	election_frequency = 60
	elections_allowed = no
}

create_country_leader = {
	name = "Alexander Zakharchenko"
	desc = ""
	picture = "Alexander_Zakharchenko.dds"
	expire = "2050.1.1"
	ideology = Autocracy
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Alexander Zakharchenko"
	picture = "generals/Portrait_Alexander_Zakharchenko.dds"
	traits = { organisational_leader }
	skill = 2
}

create_field_marshal = {
	name = "Sergey Kozlov"
	picture = "generals/Portrait_Sergey_Kozlov.dds"
	traits = {  }
	skill = 2
}

create_field_marshal = {
	name = "Igor Plotnitsky"
	picture = "generals/Portrait_Igor_Plotnitsky.dds"
	traits = {  }
	skill = 2
}

create_field_marshal = {
	name = "Igor Strelkov"
	picture = "generals/Portrait_Igor_Strelkov.dds"
	traits = {  }
	skill = 1
}

create_field_marshal = {
	name = "Oleg Bugrov"
	picture = "generals/Portrait_Oleg_Bugrov.dds"
	traits = {  }
	skill = 1
}

create_field_marshal = {
	name = "Vladimir Kononov"
	picture = "generals/Portrait_Vladimir_Kononov.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Akhra Avidzba"
	picture = "generals/Portrait_Akhra_Avidzba.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Aleksey Mozgovoy"
	picture = "generals/Portrait_Aleksey_Mozgovoy.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Bednov"
	picture = "generals/Portrait_Alexander_Bednov.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Khodakovsky"
	picture = "generals/Portrait_Alexander_Khodakovsky.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Botsman"
	picture = "generals/Portrait_Botsman.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Eduard Basurin"
	picture = "generals/Portrait_Eduard_Basurin.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Givi"
	picture = "generals/Portrait_Givi.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Igor Bezler"
	picture = "generals/Portrait_Igor_Bezler.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Konstantin Kuzmin"
	picture = "generals/Portrait_Konstantin_Kuzmin.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Motorola"
	picture = "generals/Portrait_Motorola.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Nikolai Kozytsin"
	picture = "generals/Portrait_Nikolai_Kozytsin.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Pavel Dryomov"
	picture = "generals/Portrait_Pavel_Dryomov.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Pavel Gubarev"
	picture = "generals/Portrait_Pavel_Gubarev.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Romashka"
	picture = "generals/Portrait_Romashka.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Sergei Petrovskiy"
	picture = "generals/Portrait_Sergei_Petrovskiy.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Valery Bolotov"
	picture = "generals/Portrait_Valery_Bolotov.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Yuri Shevchenko"
	picture = "generals/Portrait_Yuri_Shevchenko.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Matyushin"
	picture = "generals/Portrait_Alexander_Matyushin.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Nemogay"
	picture = "generals/Portrait_Alexander_Nemogay.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Alexey Karyakin"
	picture = "generals/Portrait_Alexey_Karyakin.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Igor Iwanow"
	picture = "generals/Portrait_Igor_Iwanow.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Igor Kornet"
	picture = "generals/Portrait_Igor_Kornet.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Ivan Kondrashov"
	picture = "generals/Portrait_Ivan_Kondrashov.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Mikhail Verin"
	picture = "generals/Portrait_Mihail_Verin.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Mikhail Shatohin"
	picture = "generals/Portrait_Mikhail_Shatohin.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Sergey Fomchenkov"
	picture = "generals/Portrait_Sergey_Fomchenkov.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Sergey Zavdoveev"
	picture = "generals/Portrait_Sergey_Zavdoveev.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Yuri Safonenko"
	picture = "generals/Portrait_Yuri_Safonenko.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Alexander Zakharchenko"
	picture = "admirals/Portrait_Alexander_Zakharchenko_navy.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Igor Plotnitsky"
	picture = "admirals/Portrait_Igor_Plotnitsky_navy.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Sergey Kozlov"
	picture = "admirals/Portrait_Sergey_Kozlov_navy.dds"
	traits = {  }
	skill = 1
}