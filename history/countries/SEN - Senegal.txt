﻿capital = 364

oob = "SEN_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	
	#For Templates
	SP_R_arty_equipment_0 = 1
	gw_artillery = 1
	combat_eng_equipment = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	Rec_tank_0 = 1
	util_vehicle_equipment_0 = 1
	infantry_weapons = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Anti_Air_0 = 1
	land_Drone_equipment = 1
	early_helicopter = 1
	transport_helicopter1 = 1
	attack_helicopter1 = 1
	
}

add_ideas = {
	pop_050
	systematic_corruption
	gdp_2
	sufi_islam
	debt_40
	fast_growth
	bud_neg_04
	defence_02
	edu_03
	health_02
	social_01
	bureau_02
	police_03
	draft_army
	volunteer_women
	Enduring_Freedom
	restoring_hope
	international_bankers
	small_medium_business_owners
	Fossil_fuel_industry
	hybrid
}
set_country_flag = bud_neg_04
set_country_flag = debt_40
set_country_flag = gdp_2
set_country_flag = positive_international_bankers
set_country_flag = positive_small_medium_business_owners

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4_IC_slot

set_convoys = 20


set_politics = {

	parties = {
		democratic = { 
			popularity = 50
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 15
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 35
		}
	}
	
	ruling_party = democratic
	last_election = "2012.2.26"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Macky Sall"
	desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" 
	picture = "Macky_Sall.dds"
	expire = "2050.1.1"
	ideology = liberalism
	traits = {
		#
	}
}