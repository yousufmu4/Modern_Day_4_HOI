﻿capital = 589

oob = "TIB_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
 
 
	#No army, but let's give them some basic tech in case they change their mind
	infantry_weapons = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Anti_Air_0 = 1
	util_vehicle_equipment_0 = 1
	
	night_vision_1 = 1
}

add_ideas = {
	pop_050
	unrestrained_corruption
	gdp_3
	buddism
	reserves_180
	stable_growth
	bud_neg_08
	defence_01
	edu_03
	health_06
	social_01
	bureau_01
	police_01
	volunteer_army
	no_women_in_military
}
set_country_flag = bud_neg_08
set_country_flag = reserves_180
set_country_flag = gdp_3

set_politics = {

	parties = {
		democratic = { 
			popularity = 0
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 0
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 100
		}
	}
	
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

