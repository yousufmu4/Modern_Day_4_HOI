﻿capital = 128

oob = "BOS_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	
	#For templates
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	SP_R_arty_equipment_0 = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
	
}

add_ideas = {
	pop_050
	#small_medium_business_owners
	#Labour_Unions
	#Mass_Media
	unrestrained_corruption
	gdp_3
	sunni
	debt_40
	stable_growth
	bud_bal
	defence_01
	edu_03
	health_05
	social_03
	bureau_03
	police_03
	volunteer_army
	volunteer_women
	small_medium_business_owners
	international_bankers
	The_Ulema
	civil_law
}
set_country_flag = bud_bal
set_country_flag = debt_40
set_country_flag = gdp_3
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_international_bankers

set_country_flag = NATO_Aspirant

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 74
		}

		fascism = {
			popularity = 1
		}
		
		communism = {
			popularity = 13
		}
		
		neutrality = {
			popularity = 12
		}
		
		Nationalist = {
			popularity = 0
		}
	}
	
	ruling_party = democratic
	last_election = "2014.10.12"
	election_frequency = 48
	elections_allowed = yes
}


set_convoys = 5


create_country_leader = {
	name = "Nermin Niksic"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "nermin_niksic.dds"
	expire = "2065.1.1"
	ideology = socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Milorad Dodik"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "milorad_dodik.dds"
	expire = "2065.1.1"
	ideology = Conservative
	traits = {
		#
	}
}

create_country_leader = {
	name = "Mladen Bosic"
	desc = "POLITICS_EARL_BROWDER_DESC"
	picture = "mladen_bosic.dds"
	expire = "2065.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Fahrudin Radoncic"
	desc = "POLITICS_EARL_BROWDER_DESC"
	picture = "fahrudin_radoncic.dds"
	expire = "2065.1.1"
	ideology = Nat_Populism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Bakir Izetbegovic"
	desc = "POLITICS_EARL_BROWDER_DESC"
	picture = "bakir_izetbegovic.dds"
	expire = "2065.1.1"
	ideology = conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Denis Zvizdić"
	desc = ""
	picture = "BOS_Denis_Zvizdic.dds"
	expire = "2065.1.1"
	ideology = liberalism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Anto Jeleč"
	picture = "Portrait_Anto_Jelec.dds"
	traits = { old_guard thorough_planner }
	skill = 3
}

create_corps_commander = {
	name = "Mirko Tepšić"
	picture = "Portrait_Mirko_Tepsic.dds"
	traits = { fortress_buster }
	skill = 2
}

create_corps_commander = {
	name = "Senad Mašović"
	picture = "Portrait_Senad_Masovic.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Dragan Vuković"
	picture = "Portrait_Dragan_Vukovic.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Husein Tursunović"
	picture = "Portrait_Husein_Tursunovic.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Mirsad Ahmić"
	picture = "Portrait_Mirsad_Ahmic.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Ivica Jerkić"
	picture = "Portrait_Ivica_Jerkic.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Marko Stojčić"
	picture = "Portrait_Marko_Stojcic.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Gojko Knežević"
	picture = "Portrait_Gojko_Knesevic.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Kenan Dautović"
	picture = "Portrait_Kenan_Dautovic.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Tomo Kolenda"
	picture = "Portrait_Tomo_Kolenda.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Radovan Ilić"
	picture = "Portrait_Radovan_Ilic.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Enes Husejnović"
	picture = "Portrait_Enes_Huseinovic.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Amir Čorbo"
	picture = "Portrait_Amir_Corbo.dds"
	traits = { trait_engineer }
	skill = 1
}

