﻿capital = 773
oob = "USA_2017"

add_ideas = {
	pop_065
	modest_corruption
	gdp_7
	christian
	debt_80
	stagnation
	bud_neg_04
	defence_05
	edu_03
	health_03
	social_04
	bureau_03
	police_02
	volunteer_army
	volunteer_women
	western_country
	large_far_right_movement
}

set_country_flag = bud_neg_04
set_country_flag = debt_80
set_country_flag = gdp_9

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1

	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1 #2005

	#2005
	combat_eng_equipment = 1
	
	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1 #1985
	
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	command_control_equipment4 = 1 #2015
	
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	land_Drone_equipment2 = 1
	land_Drone_equipment3 = 1 #2015
	
	Early_APC = 1 #Vehicle Design
	
	APC_1 = 1
	APC_2 = 1
	APC_3 = 1
	APC_4 = 1
	APC_5 = 1 #2005
	
	 #1985
	
	IFV_1 = 1
	IFV_2 = 1
	IFV_3 = 1
	IFV_4 = 1 #1995
	
	MBT_1 = 1
	MBT_2 = 1
	MBT_3 = 1
	MBT_4 = 1 #1995
	
	ENG_MBT_1 = 1
	ENG_MBT_2 = 1
	ENG_MBT_3 = 1
	ENG_MBT_4 = 1 #1995
	
	Rec_tank_0 = 1 #1965
	Rec_tank_1 = 1
	Rec_tank_2 = 1 #2005
	
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	util_vehicle_equipment_3 = 1
	util_vehicle_equipment_4 = 1
	util_vehicle_equipment_5 = 1 #2015
	
	gw_artillery = 1
	Arty_upgrade_1 = 1
	SP_arty_equipment_0 = 1
	SP_R_arty_equipment_0 = 1
	artillery_1 = 1
	artillery_2 = 1
	Arty_upgrade_2 = 1
	Arty_upgrade_3 = 1
	SP_arty_equipment_1 = 1
	SP_R_arty_equipment_1 = 1
	SP_arty_equipment_2 = 1
	SP_R_arty_equipment_2 = 1
	
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	AT_upgrade_2 = 1
	Anti_tank_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	Anti_Air_1 = 1
	SP_Anti_Air_1 = 1
	AA_upgrade_2 = 1
	Anti_Air_2 = 1
	AA_upgrade_3 = 1
	
	early_fighter = 1
	Strike_fighter1 = 1
	Strike_fighter2 = 1
	Strike_upgrade_1 = 1
	Strike_fighter3 = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	MR_Fighter4 = 1
	AS_Fighter1 = 1
	AS_Fighter2 = 1
	AS_upgrade_1 = 1
	AS_Fighter3 = 1
	AS_Fighter4 = 1
	CV_MR_Fighter1 = 1
	CV_MR_Fighter2 = 1
	CV_MR_Fighter3 = 1
	CV_MR_Fighter4 = 1
	
	Air_UAV1 = 1
	Air_UAV2 = 1
	
	L_Strike_fighter1 = 1
	L_Strike_fighter2 = 1
	
	Int_Fighter1 = 1 #4th gen
	
	early_bomber = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	strategic_bomber4 = 1
	
	transport_plane1 = 1
	transport_plane2 = 1
	transport_plane3 = 1
	transport_plane4 = 1
	
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	naval_plane4 = 1
	
	cas1 = 1
	cas2 = 1
	
	early_helicopter = 1
	attack_helicopter1 = 1
	attack_helicopter2 = 1
	attack_helicopter3 = 1
	attack_helicopter4 = 1
	
	transport_helicopter1 = 1
	transport_helicopter2 = 1
	transport_helicopter3 = 1

	
	corvette_1 = 1
	corvette_2 = 1
	missile_corvette_1 = 1
	missile_corvette_2 = 1
	missile_corvette_3 = 1
	
	
	
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_3 = 1
	missile_frigate_4 = 1 #Freedom Class
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	missile_destroyer_2 = 1 #Arleigh Burke-class destroyer flight II
	
	cruiser_1 = 1
	cruiser_2 = 1
	missile_cruiser_1 = 1
	missile_cruiser_4 = 1
	
	submarine_1 = 1
	missile_submarine_1 = 1
	missile_submarine_2 = 1
	missile_submarine_3 = 1
	
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	attack_submarine_4 = 1
	
	diesel_attack_submarine_1 = 1
	
	modern_carrier_0 = 1
	Nuclear_carrier_0 = 1
	Nuclear_carrier_1 = 1
	Nuclear_carrier_2 = 1 #Ford class
	
	LPD_0 = 1 #Austin-class
	LPD_1 = 1 #San Antonio-class
	
	LHA_0 = 1 #Tarawa-class
	LHA_1 = 1 #Wasp class
	LHA_2 = 1 #America Class
	
	landing_craft = 1

}

set_convoys = 100
set_stability = 0.80
set_politics = {
	parties = {
		democratic = { 
			popularity = 33
		}
		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 0
			#banned = no #default is no
		}
		
		neutrality = {
			popularity = 4
		}
		
		Nationalist = {
			popularity = 63
		}
	}
	
	ruling_party = Nationalist
	last_election = "2016.11.8"
	election_frequency = 48
	elections_allowed = no
}
create_country_leader = {
	name = "David Duke"
	desc = "POLITICS_EARL_BROWDER_DESC"
	picture = "David_Duke.dds"
	expire = "2065.1.1"
	ideology = Nat_Fascism
	traits = {
		nationalist_Nat_Fascism
	}
}