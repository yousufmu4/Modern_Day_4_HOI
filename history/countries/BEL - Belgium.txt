﻿capital = 51

oob = "BEL_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1
	
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1

	#FN SCAR
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	
	#Wielingen-Class
	frigate_1 = 1
	frigate_2 = 1
	
	#SIBMAS
	IFV_1 = 1
	IFV_2 = 1
	
	#For templates
	
	combat_eng_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	util_vehicle_equipment_0 = 1
	
	landing_craft = 1
	
}

add_ideas = {
	pop_050
	#small_medium_business_owners
	#Labour_Unions
	#The_Clergy
	modest_corruption
	christian
	gdp_9
	EU_member
	debt_60
    stable_growth
	bud_neg_04
	defence_02
	edu_05
	health_05
	social_06
	bureau_03
	police_04
	volunteer_army
	volunteer_women
	intervention_limited_interventionism
	NATO_member
	western_country
	medium_far_right_movement
	small_medium_business_owners
	landowners
	industrial_conglomerates
	civil_law
	}
set_country_flag = bud_neg_04
set_country_flag = debt_60
set_country_flag = gdp_9
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_landowners
set_country_flag = positive_industrial_conglomerates

#NATO military access
diplomatic_relation = {
	country = ALB
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BUL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CAN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CRO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CZH
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = DEN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = EST
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = FRA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GER
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GRE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HUN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ICE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ITA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LAT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LIT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LUX
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HOL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = NOR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ROM
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLV
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SPR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = TUR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ENG
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = USA
	relation = military_access
	active = yes
}

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 87
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 1
		}
		
		neutrality = {
			popularity = 10
		}
		
		Nationalist = {
			popularity = 2
		}
	}
	
	ruling_party = democratic
	last_election = "2014.5.25"
	election_frequency = 60
	elections_allowed = yes
}


set_convoys = 40


create_country_leader = {
	name = "Wouter Beke"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "wouter_beke.dds"
	expire = "2065.1.1"
	ideology = conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Elio Di Rupo"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "elio_di_rupo.dds"
	expire = "2050.1.1"
	ideology = socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "John Crombez"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "john_crombez.dds"
	expire = "2060.1.1"
	ideology = socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Peter Mertens"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "peter_mertens.dds"
	expire = "2065.1.1"
	ideology = Communist-State
	traits = {
		#
	}
}

create_country_leader = {
	name = "Bart De Wever"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "BEL_Bart_De_Wever.dds"
	expire = "2065.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Olivier Maingain"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "olivier_maingain.dds"
	expire = "2065.1.1"
	ideology = Nat_Populism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Charles Michel"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "BEL_Charles_Michel.dds"
	expire = "2065.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_field_marshal = {
	name = "Marc Compernol"
	picture = "Portrait_Marc_Compernol.dds"
	traits = { old_guard thorough_planner }
	skill = 3
}

create_corps_commander = {
	name = "Pierre Neirinckx"
	picture = "Portrait_Pierre_Neirinckx.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Frederick Vansina"
	picture = "Portrait_Frederick_Vansina.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Jean-Paul Deconinck"
	picture = "Portrait_Jean-Paul_Deconicnk.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Hubert de Vos"
	picture = "Portrait_Hubert_de_Vos.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Eddy Testelmans"
	picture = "Portrait_Eddy_Testelmans.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Marc Thys"
	picture = "Portrait_Marc_Thys.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Bart Moerman"
	picture = "Portrait_Bart_Moerman.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Bruno Denis"
	picture = "Portrait_Bruno_Denis.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Bruno van Loo"
	picture = "Portrait_Bruno_van_Loo.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Michel Pihard"
	picture = "Portrait_Michel_Pihard.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Wim Denolf"
	picture = "Portrait_Wim_Denolf.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Wim Robberecht"
	picture = "Portrait_Wim_Robberecht.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 2
}

create_navy_leader = {
	name = "Georges Heerlen"
	picture = "Portrait_Georges_Heerlen.dds"
	traits = { blockade_runner }
	skill = 1
}