﻿capital = 102

oob = "FIN_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1

    #RK-95 latest infantry weapon
    infantry_weapons = 1
    infantry_weapons1 = 1
    infantry_weapons2 = 1
    infantry_weapons3 = 1 
    
    combat_eng_equipment = 1
    
    night_vision_1 = 1
    night_vision_2 = 1
    night_vision_3 = 1
    
    command_control_equipment = 1
    command_control_equipment1 = 1
    command_control_equipment2 = 1
    command_control_equipment3 = 1
    
    #95S 58-61 Musti Recoilles Rifle
    Anti_tank_0 = 1
    
    #155 K 98 Heavy Artillery
    gw_artillery = 1
    Arty_upgrade_1 = 1   
    artillery_1 = 1
    Arty_upgrade_2 = 1
    Arty_upgrade_3 = 1
	artillery_2 = 1
    
    #Patria AMOS Self Propelled Mortar System
    SP_arty_equipment_0 = 1
    SP_arty_equipment_1 = 1
    SP_arty_equipment_2 = 1
    
    #ITSPV Leopard 2 with Marksman Turret
    Anti_Air_0 = 1
    AA_upgrade_1 = 1
    Anti_Air_1 = 1
    AA_upgrade_3 = 1
    Anti_Air_2 = 1
    SP_Anti_Air_0 = 1
    SP_Anti_Air_1 = 1
    SP_Anti_Air_2 = 1
	
	#Hämeenmaa-Class
	frigate_1 = 1
    
    #Patria XA-series (and Patria AMV)
    Early_APC = 1
    APC_1 = 1
    APC_2 = 1
    APC_3 = 1
    APC_4 = 1
    APC_5 = 1
    APC_6 = 1
	
	IFV_1 = 1
	IFV_2 = 1
	IFV_3 = 1
	IFV_4 = 1
	IFV_5 = 1
	IFV_6 = 1

    #Patria XA-series is also amphibious
    
    
    
    

    #SISU SA-240 Truck
    util_vehicle_equipment_0 = 1
    util_vehicle_equipment_1 = 1
    util_vehicle_equipment_2 = 1
	
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	land_Drone_equipment2 = 1
    
    #To make templates work
    MBT_1 = 1
    ENG_MBT_1 = 1
    Heavy_Anti_tank_0 = 1
    early_helicopter = 1
    transport_helicopter1 = 1
	
	landing_craft = 1
	
}

add_ideas = {
	pop_050

	negligible_corruption

	gdp_9
	christian
	EU_member
	reserves_40
	stagnation
	bud_neg_04
	defence_01
	edu_05
	health_05
	social_06
	bureau_03
	police_01
	draft_army
	volunteer_women
	small_medium_business_owners
	labour_unions
	industrial_conglomerates
	intervention_limited_interventionism
	western_country
	large_far_right_movement
	civil_law
}
set_country_flag = bud_neg_04
set_country_flag = reserves_40
set_country_flag = gdp_9
set_country_flag = Major_Importer_US_Arms
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_industrial_conglomerates

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot

#Nordic good relations
add_opinion_modifier = { target = SWE modifier = nordic }
add_opinion_modifier = { target = ICE modifier = nordic }
add_opinion_modifier = { target = NOR modifier = nordic }
add_opinion_modifier = { target = DEN modifier = nordic }

set_politics = {

	parties = {
		democratic = { 
			popularity = 67
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 4
		}
		
		neutrality = {
			popularity = 8
		}
		
		Nationalist = {
			popularity = 21
		}
	}
	
	ruling_party = democratic
	last_election = "2015.4.19"
	election_frequency = 48
	elections_allowed = yes
}


set_convoys = 16


create_country_leader = {
	name = "Petteri Orpo"
	desc = "POLITICS_WILLIAM_DUDLEY_PELLEY_DESC"
	picture = "petteri_orpo.dds"
	expire = "2060.1.1"
	ideology = conservatism
	traits = {
		
	}
}

create_country_leader = {
	name = "Antti Rinne"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "antti_rinne.dds"
	expire = "2060.1.1"
	ideology = socialism
	traits = {
	
	}
}

create_country_leader = {
	name = "Li Andersson"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "li_andersson.dds"
	expire = "2060.1.1"
	ideology = Communist-State
	traits = {
	
	}
}

create_country_leader = {
	name = "Ville Niinistö"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "ville_niinisto.dds"
	expire = "2065.1.1"
	ideology = Neutral_social
	traits = {
	
	}
}

create_country_leader = {
	name = "Timo Soini"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "timo_soini.dds"
	expire = "2065.1.1"
	ideology = Nat_Populism
	traits = {
	
	}
}

create_country_leader = {
	name = "Juha Sipilä"
	desc = "POLITICS_Juha_Sipila_DESC"
	picture = "Juha_Sipila.dds"
	expire = "2055.1.1"
	ideology = liberalism
	traits = {
		businessman
		western_liberalism
		polished
		greedy
	}
}
create_field_marshal = {
	name = "Seppo Toivonen" #Kenraaliluutnantti #General Lietnuant
	picture = "gen_Seppo_Toivonen.dds"
	traits = {  }
	skill = 3
}
create_corps_commander = {
	name = "Petri Hulkko" #Maavoimien komentaja #Ground Forces Commander
		picture = "gen_Petri_Hulkko.dds"
	traits = {  }
	skill = 2
}
create_field_marshal = {
	name = "Jarmo Lindberg"
	picture = "Portrait_Jarmo_Lindberg.dds"
	traits = { old_guard defensive_doctrine }
	skill = 4
}

create_corps_commander = {
	name = "Pekka Järvi"
	picture = "Portrait_Pekka_Jaervi.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Ahti Kurvinen"
	picture = "Portrait_Ahti_ Kurvinen.dds"
	traits = { ranger winter_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Antti Lehtisalo"
	picture = "Portrait_Antti_Lehtisalo.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Vesa Virtanen"
	picture = "Portrait_Vesa_Virtanen.dds"
	traits = { trickster }
	skill = 2
}

create_corps_commander = {
	name = "Jari Kallio"
	picture = "Portrait_Jari_Kallio.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Arto-Pekka Nurminen"
	picture = "Portrait_Artto-Pekka_Nurminen.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Ali Mättölä"
	picture = "Portrait_ Ali_ Maettoelae.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Ilkka Laitinen"
	picture = "Portrait_Ilkka_Laitinen.dds"
	traits = { ranger }
	skill = 2
}

create_corps_commander = {
	name = "Kim Jäämeri"
	picture = "Portrait_Kim_Jaeaemeri.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Kyösti Halonen"
	picture = "Portrait_Kyoesti_Halonen.dds"
	traits = { trait_engineer }
	skill = 3
}

create_corps_commander = {
	name = "Eero Pyötsiä"
	picture = "Portrait_Eero_Pyoetsia.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Timo Kivinen"
	picture = "Portrait_Timo_Kivinen.dds"
	traits = { trait_engineer }
	skill = 3
}
	
create_corps_commander = {
	name = "Markku Myllykangas"
	picture = "Portrait_Markku_Myllykangas.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Pasi Velimäki"
	picture = "Portrait_Pasi_Velimaeki.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Timo Kakkola"
	picture = "Portrait_Timo_Kakkola.dds"
	traits = { winter_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Sampo Eskelinen"
	picture = "Portrait_Sampo_Eskelinen.dds"
	traits = { trait_engineer }
	skill = 1
}
	
create_corps_commander = {
	name = "Harri Ohra-Aho"
	picture = "Portrait_Harri_Ohra-Aho.dds"
	traits = { trickster }
	skill = 2
}

create_corps_commander = {
	name = "Kjell Törner"
	picture = "Portrait_Kjell_Toerner.dds"
	traits = { commando naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Esa Pulkkinen"
	picture = "Portrait_Esa_Pukkinen.dds"
	traits = { hill_fighter }
	skill = 3
}

create_navy_leader = {
	name = "Juha Rannikko"
	picture = "Portrait_Juha_Ranniko.dds"
	traits = { air_controller }
	skill = 3
}

create_navy_leader = {
	name = "Kari Takanen"
	picture = "Portrait_Kari_Takanen.dds"
	traits = { superior_tactician }
	skill = 3
}

create_navy_leader = {
	name = "Veijo Teipalus"
	picture = "Portrait_Veijo_Taipalus.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 3
}

create_navy_leader = {
	name = "Erkki Mikkola"
	picture = "Portrait_Erkki_Mikkola.dds"
	traits = { seawolf }
	skill = 1
}

create_navy_leader = {
	name = "Juha Vauhkonen"
	picture = "Portrait_Juha_Vauhkonen.dds"
	traits = { spotter }
	skill = 2
}

create_navy_leader = {
	name = "Timo Hirvonen"
	picture = "Portrait_Timo_Hirvonen.dds"
	traits = { blockade_runner }
	skill = 1
}