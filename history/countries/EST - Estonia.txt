﻿capital = 105

oob = "EST_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	night_vision_2 = 1
	
	#For templates
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	land_Drone_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	util_vehicle_equipment_0 = 1
	MBT_1 = 1
	ENG_MBT_1 = 1
	
}

add_ideas = {
	pop_050
	#small_medium_business_owners
	#Labour_Unions
	#Mass_Media
	pluralist
	modest_corruption
	gdp_7
	
	EU_member
	debt_0
	stable_growth
	bud_bal
	defence_02
	edu_04
	health_04
	social_04
	bureau_03
	police_03
	draft_army
	volunteer_women
	NATO_member
	small_medium_business_owners
	landowners
	international_bankers
	civil_law
}
set_country_flag = bud_bal
set_country_flag = debt_0
set_country_flag = gdp_7
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_landowners

#NATO military access
diplomatic_relation = {
	country = ALB
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BEL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BUL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CAN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CRO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CZH
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = DEN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = FRA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GER
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GRE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HUN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ICE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ITA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LAT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LIT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LUX
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HOL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = NOR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ROM
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLV
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SPR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = TUR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ENG
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = USA
	relation = military_access
	active = yes
}

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 66
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 0
		}
		
		neutrality = {
			popularity = 27
		}
		
		Nationalist = {
			popularity = 7
		}
	}
	
	ruling_party = democratic
	last_election = "2015.3.1"
	election_frequency = 48
	elections_allowed = yes
}


set_convoys = 15



create_country_leader = {
	name = "Jevgeni Ossinovski"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "jevgeni_ossinovski.dds"
	expire = "2065.1.1"
	ideology = socialism
	traits = {
	
	}
}

create_country_leader = {
	name = "Sergei Jurgens"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "sergei_jurgens.dds"
	expire = "2065.1.1"
	ideology = Communist-State
	traits = {
	
	}
}

create_country_leader = {
	name = "Edgar Savisaar"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "edgar_savisaar.dds"
	expire = "2065.1.1"
	ideology = Neutral_Libertarian
	traits = {
	
	}
}

create_country_leader = {
	name = "Mart Helme"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "mart_helme.dds"
	expire = "2065.1.1"
	ideology = Nat_Populism
	traits = {
	
	}
}

create_country_leader = {
	name = "Taavi Rõivas"
	desc = "POLITICS_Taavi_Roivas.dds_DESC"
	picture = "Taavi_Roivas.dds"
	expire = "2055.1.1"
	ideology = liberalism
	traits = {
		#
	}
}
set_research_slots = 2

create_field_marshal = {
	name = "Riho Terras"
	picture = "Portrait_Riho_Terras.dds"
	traits = { old_guard thorough_planner }
	skill = 4
}

create_field_marshal = {
	name = "Indrek Sirel"
	picture = "Portrait_Indrek_Sirel.dds"
	traits = { offensive_doctrine }
	skill = 1
}

create_field_marshal = {
	name = "Neeme Väli"
	picture = "Portrait_Neeme_Vaeli.dds"
	traits = { inspirational_leader }
	skill = 2
}

create_corps_commander = {
	name = "Martin Herem"
	picture = "Portrait_Martin_Herem.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Veiko-Vello Palm"
	picture = "Portrait_Veiko-Vello_Palm.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Janno Märk"
	picture = "Portrait_Janno_Maerk.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Arno Kruusmann"
	picture = "Portrait_Arno_Kruusmann.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Arbo Probal"
	picture = "Portrait_Arbo_Probal.dds"
	traits = { bearer_of_artillery }
	skill = 1
}

create_corps_commander = {
	name = "Priit Pikner"
	picture = "Portrait_Priit_Pikner.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Sten Allik"
	picture = "Portrait_Sten_Allik.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "André Lilleleht"
	picture = "Portrait_Andre_Lilleleht.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Toomas Pindis"
	picture = "Portrait_Toomas_Pindis.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Eero Rebo"
	picture = "Portrait_Eero_Rebo.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Roman Lukas"
	picture = "Portrait_Roman_Lukas.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Riho Ühtegi"
	picture = "Portrait_Riho_Uehtegi.dds"
	traits = { commando naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Jaak Tarien"
	picture = "Portrait_Jaak_Tarien.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Meelis Kiili"
	picture = "Portrait_Meelis_Kiili.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Peeter Hoppe"
	picture = "Portrait_Peeter_Hoppe.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Artur Tiganik"
	picture = "Portrait_Artur_Tiganik.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Aivar Kokka"
	picture = "Portrait_Aivar_Kokka.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Peeter Tali"
	picture = "Portrait_Peeter_Tali.dds"
	traits = { trait_engineer }
	skill = 1
}

create_navy_leader = {
	name = "Jüri Saska"
	picture = "Portrait_Jueri_Saska.dds"
	traits = { superior_tactician }
	skill = 2
}

create_navy_leader = {
	name = "Ain Pärna"
	picture = "Portrait_Ain_Paerna.dds"
	traits = { blockade_runner }
	skill = 1
}

create_navy_leader = {
	name = "Sten Sepper"
	picture = "Portrait_Sten_Sepper.dds"
	traits = { seawolf }
	skill = 1
}

create_navy_leader = {
	name = "Igor Schvede"
	picture = "Portrait_Igor_Schvede.dds"
	traits = { spotter }
	skill = 2
}