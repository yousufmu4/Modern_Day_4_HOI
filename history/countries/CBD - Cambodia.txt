﻿capital = 514

oob = "CBD_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	
	#For templates
	infantry_weapons = 1
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
	
	landing_craft = 1


}

add_ideas = {
	pop_050
	crippling_corruption
	buddism
	gdp_2
	debt_20
	fast_growth
	bud_bal
	defence_02
	edu_01
	health_01
	social_01
	bureau_01
	police_04
	volunteer_army
	volunteer_women
	farmers
	oligarchs
	international_bankers
	civil_law
}
set_country_flag = bud_bal
set_country_flag = debt_20
set_country_flag = gdp_2
set_country_flag = negative_farmers

#Nat focus
	complete_national_focus = bonus_tech_slots

add_opinion_modifier = { target = CHI modifier = bamboo_network }

set_convoys = 10


set_politics = {

	parties = {
		democratic = { 
			popularity = 35
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 47
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 18
		}
	}
	
	ruling_party = communism
	last_election = "2013.07.28"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Hun Sen"
	desc = ""
	picture = "CAB_Hun_Sen.dds"
	ideology = Autocracy
	traits = {
		guerrilla_leader
		emerging_Autocracy
		pro_china
		sly
		ruthless
	}
}
create_field_marshal = {
	name = "Tea Banh"
	picture = "Portrait_Tea_Banh.dds"
	traits = { old_guard defensive_doctrine }
	skill = 4
}

create_field_marshal = {
	name = "Soeung Samnang"
	picture = "Portrait_Soeung_Samnang.dds"
	traits = { logistics_wizard }
	skill = 4
}

create_corps_commander = {
	name = "Meas Sophea"
	picture = "Portrait_Meas_Sophea.dds"
	traits = { panzer_leader ranger }
	skill = 4
}

create_corps_commander = {
	name = "Sao Sokha"
	picture = "Portrait_Sao_Sokha.dds"
	traits = { urban_assault_specialist }
	skill = 4
}

create_corps_commander = {
	name = "So Phanni"
	picture = "Portrait_So_Phanni.dds"
	traits = {  }
	skill = 3
}

create_corps_commander = {
	name = "Eng Hie"
	picture = "Portrait_Eng_Hie.dds"
	traits = {  }
	skill = 3
}

create_navy_leader = {
	name = "Tea Vinh"
	picture = "Portrait_Tea_Vinh.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 3
}