﻿capital = 324

oob = "CAR_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Anti_Air_0 = 1
}

set_convoys = 20


declare_war_on = {
	target = LOG
	type = annex_everything
}

add_ideas = {
	pop_050
	rampant_corruption
	pluralist
	gdp_1
	debt_20
	stable_growth
	bud_neg_04
	defence_07
	edu_01
	health_01
	social_01
	bureau_01
	police_05
	draft_army
	no_women_in_military
	industrial_conglomerates
	international_bankers
	civil_law
}
set_country_flag = bud_neg_04
set_country_flag = debt_20
set_country_flag = gdp_1
set_country_flag = enthusiastic_industrial_conglomerates
set_country_flag = positive_international_bankers
set_country_flag = negative_farmers

#Nat focus
	complete_national_focus = bonus_tech_slots

set_politics = {

	parties = {
		democratic = { 
			popularity = 5
		}

		fascism = {
			popularity = 5
		}
		
		communism = {
			popularity = 0
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 90
		}
	}
	
	ruling_party = neutrality
	last_election = "2016.2.14"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Catherine Samba-Panza" #needs to be updated
	desc = "POLITICS_Mohamed Cheikh Biadillah_DESC"
	picture = "CAR_catherine_samba-panza.dds"
	expire = "2047.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}
#Sekela leader?
create_country_leader = {
	name = "Michel Djotodia"
	desc = "POLITICS_Mohamed Cheikh Biadillah_DESC"
	picture = "CAR_sekela-michel-djotodia.dds"
	expire = "2047.1.1"
	ideology = Neutral_Libertarian #please fixed, temp changed to Neutral_Libertarian
	traits = {
		#
	}
}
create_country_leader = {
	name = "Faustin-Archange Touadéra"
	desc = "POLITICS_Mohamed Cheikh Biadillah_DESC"
	picture = "CAR_faustin_archange_touadera.dds"
	expire = "2047.1.1"
	ideology = neutral_Social 
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Xavier Sylvestre Yangongo"
	picture = "Portrait_Xavier_Sylvestre_Yangongo.dds"
	traits = { organisational_leader }
	skill = 3
}