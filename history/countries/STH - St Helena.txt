﻿capital = 704

oob = "STH_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#No army, but let's give them some basic tech in case they change their mind
	infantry_weapons = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Anti_Air_0 = 1
	util_vehicle_equipment_0 = 1
	
	night_vision_1 = 1
}

add_ideas = {
	pop_050
	unrestrained_corruption
	gdp_3
	christian
	reserves_180
	stable_growth
	bud_neg_08
	defence_01
	edu_03
	health_06
	social_01
	bureau_01
	police_05
	no_military
	volunteer_women
	small_medium_business_owners
	farmers
	maritime_industry
	common_law
}


set_convoys = 20


set_politics = {

	parties = {
		democratic = { 
			popularity = 71
		}

		fascism = {
			popularity = 16
		}
		
		communism = {
			popularity = 13
			#banned = no #default is no
		}
	}
	
	ruling_party = democratic
	last_election = "1932.11.8"
	election_frequency = 48
	elections_allowed = yes
}
