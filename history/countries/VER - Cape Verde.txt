﻿capital = 398

oob = "VER_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	night_vision_2 = 1
	
	#for templates
	infantry_weapons = 1
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Anti_Air_0 = 1
	Rec_tank_0 = 1
}

add_ideas = {
	pop_050
	widespread_corruption
	gdp_3
	christian
	debt_80
	stable_growth
	bud_neg_04
	defence_01
	edu_03
	health_03
	bureau_03
	partial_draft_army 
	drafted_women
	police_03
	civil_law
}
set_country_flag = bud_neg_04
set_country_flag = debt_80
set_country_flag = gdp_3

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot

set_convoys = 20


set_politics = {

	parties = {
		democratic = { 
			popularity = 30
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 0
			#banned = no #default is no
		}
		
		neutrality = {
			popularity = 70
			#banned = no #default is no
		}
	}
	
	ruling_party = neutrality
	last_election = "2016.11.20"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Jorge Carlos Fonseca"
	desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" 
	picture = "CAV_Jorge_Carlos_Fonseca.dds"
	expire = "2050.1.1"
	ideology = Neutral_social
	traits = {
		#
	}
}