﻿capital = 725

oob = "UZB_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	
	###ALL FOR TEMPLATES###
	
	infantry_weapons = 1
	command_control_equipment = 1
	land_Drone_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	Anti_Air_0 = 1
	combat_eng_equipment = 1
	Early_APC = 1
	MBT_1 = 1
	
	IFV_1 = 1
	
	APC_1 = 1
	
	Air_APC_1 = 1
	
	util_vehicle_equipment_0 = 1
	
	SP_arty_equipment_0 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	
	SP_R_arty_equipment_0 = 1
	
	early_helicopter = 1
	
	ENG_MBT_1 = 1
	
	early_fighter = 1
	MR_Fighter1 = 1
	AS_Fighter1 = 1
	
	early_bomber = 1
	cas1 = 1
	
	night_vision_1 = 1
	
}


set_convoys = 20


add_ideas = {
	pop_050
	unrestrained_corruption
	gdp_2
	sunni
	sco_member
	debt_0
	fast_growth
	bud_bal
	defence_04
	edu_01
	health_02
	social_04
	bureau_02
	police_02
	draft_army
	volunteer_women
	oligarchs
	Fossil_fuel_industry
	landowners
	civil_law
}
set_country_flag = bud_bal
set_country_flag = debt_0
set_country_flag = gdp_2
set_country_flag = positive_Fossil_fuel_industry
set_country_flag = negative_landowners

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 4
		}

		fascism = {
			popularity = 13
		}
		
		communism = {
			popularity = 22
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 61
		}
	}
	
	ruling_party = neutrality
	last_election = "1932.11.8"
	election_frequency = 48
	elections_allowed = no
}

#Dead, need update.
create_country_leader = {
	name = "Islam Karimov"
	desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" 
	picture = "Islam_Karimov.dds"
	expire = "2050.1.1"
	ideology = Neutral_Autocracy
	traits = {
		#
	}
}