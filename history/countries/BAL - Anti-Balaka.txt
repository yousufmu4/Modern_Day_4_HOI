﻿capital = 922

oob = "BAL_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Anti_Air_0 = 1
}

declare_war_on = {
	target = CAR
	type = civil_war
}

declare_war_on = {
	target = LOG
	type = annex_everything
}

set_convoys = 20


add_ideas = {
	pop_050
	rampant_corruption
	christian
	gdp_1
	defence_07
	edu_01
	health_01
	social_01
	bureau_01
	police_03
	volunteer_army
	volunteer_women
	the_military
	The_Clergy
	farmers
	tribalism
}
set_country_flag = gdp_1
set_country_flag = negative_The_Clergy

#Nat focus
	complete_national_focus = bonus_tech_slots

set_politics = {

	parties = {
		democratic = { 
			popularity = 5
		}

		fascism = {
			popularity = 5
		}
		
		communism = {
			popularity = 0
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 90
		}
	}
	
	ruling_party = neutrality
	last_election = "2016.2.14"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Levy Yakete"
	desc = "POLITICS_Mohamed Cheikh Biadillah_DESC"
	picture = "BAL_levy_yakete.dds"
	expire = "2047.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}