﻿capital = 369

oob = "MAU_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
	ENG_MBT_1 = 1
}


set_convoys = 20


add_ideas = {
	pop_050
	unrestrained_corruption
	gdp_2
	sufi_islam
	LoAS_member
	debt_80
	stagnation
	bud_bal
	defence_03
	edu_02
	health_01
	social_02
	bureau_04
	police_03
	volunteer_army
	no_women_in_military
	Enduring_Freedom
	international_bankers
	industrial_conglomerates
	maritime_industry
	hybrid
}
set_country_flag = bud_bal
set_country_flag = debt_80
set_country_flag = gdp_2
set_country_flag = positive_international_bankers
set_country_flag = positive_industrial_conglomerates
set_country_flag = positive_maritime_industry

#Nat focus
	complete_national_focus = bonus_tech_slots

set_politics = {

	parties = {
		democratic = { 
			popularity = 10
		}

		fascism = {
			popularity = 9
		}
		
		communism = {
			popularity = 5
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 51
		}
		
		nationalist = { 
			popularity = 25
		}
	}
	
	ruling_party = neutrality
	last_election = "2014.6.21"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Mohamed Ould Abdel Aziz"
	desc = "POLITICS_Mohamed_Ould_Abdel_Aziz_DESC"
	picture = "Mohamed_Ould_Abdel_Aziz.dds"
	expire = "2050.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}