﻿capital = 423

oob = "BHR_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	night_vision_2 = 1
	
	#For templates
	infantry_weapons = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
}

add_ideas = {
	pop_050
	rampant_corruption
	gdp_9
	sunni
	saudi_aid
	stable_growth
	debt_80
	bud_neg_12
	defence_05
	edu_04
	health_03
	social_04
	bureau_03
	police_05
	al_jazeera_banned
	draft_army
	volunteer_women
	Major_Non_NATO_Ally
	restoring_hope
	rentier_state
    export_economy
	Fossil_fuel_industry
	wahabi_ulema
	saudi_royal_family
	hybrid
}
set_country_flag = bud_neg_12
set_country_flag = debt_80
set_country_flag = gdp_9
set_country_flag = saudi_royal_family
set_country_flag = enthusiastic_Fossil_fuel_industry
set_country_flag = wahabi_ulem

set_convoys = 30


add_opinion_modifier = { target = USA modifier = supports_us }

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot

add_opinion_modifier = { target = PER modifier = hostile_status }
add_opinion_modifier = { target = SYR modifier = hostile_status }
add_opinion_modifier = { target = IRQ modifier = hostile_status }

add_opinion_modifier = { target = SAU modifier = Gulf_Cooperation_Council_Relations }
add_opinion_modifier = { target = KUW modifier = Gulf_Cooperation_Council_Relations }
add_opinion_modifier = { target = UAE modifier = Gulf_Cooperation_Council_Relations }
add_opinion_modifier = { target = OMA modifier = Gulf_Cooperation_Council_Relations }

add_opinion_modifier = { target = USA modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = CAN modifier = Arabian_Peninsula_West_Economical_Relations }

add_opinion_modifier = { target = ENG modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = IRE modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = FRA modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = SPR modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = POR modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = AUS modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = ITA modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = BEL modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = HOL modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = LUX modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = GER modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = DEN modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = NOR modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = SWE modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = FIN modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = POL modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = EST modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = LAT modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = LIT modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = CZH modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = SLO modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = HUN modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = ROM modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = BUL modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = CRO modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = TUR modifier = Arabian_Peninsula_West_Economical_Relations }
                    
add_opinion_modifier = { target = SWI modifier = Arabian_Peninsula_West_Economical_Relations }
add_opinion_modifier = { target = ICE modifier = Arabian_Peninsula_West_Economical_Relations }

add_opinion_modifier = { target = CHI modifier = Arabian_Peninsula_Asia_Economical_Relations }
add_opinion_modifier = { target = KOR modifier = Arabian_Peninsula_Asia_Economical_Relations }
add_opinion_modifier = { target = RAJ modifier = Arabian_Peninsula_Asia_Economical_Relations }
add_opinion_modifier = { target = JAP modifier = Arabian_Peninsula_Asia_Economical_Relations }

add_opinion_modifier = { target = SOV modifier = Arabian_Peninsula_Russia_Economical_Relations }


set_politics = {

	parties = {
		democratic = { 
			popularity = 5
		}

		fascism = {
			popularity = 41
		}
		
		communism = {
			popularity = 49
			banned = yes
		}
		
		neutrality = { 
			popularity = 5
		}
	}
	
	ruling_party = fascism 
	last_election = "1999.3.6"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Ali Salman"
	picture = "BHR_Ali_Salman.dds"
	expire = "2070.1.1"
	ideology = Vilayat_e_Faqih
	traits = {
	}
}

create_country_leader = {
	name = "Isa Qassim"
	picture = "Isa_Qassim.dds"
	expire = "2025.1.1"
	ideology = Vilayat_e_Faqih
	traits = {
	}
}

create_country_leader = {
	name = "Hamad bin Isa Al Khalifa"
	desc = "POLITICS_Hamad_bin_Isa_Al_Khalifa_DESC" 
	picture = "BHR_Hamad_bin_Isa_Al_Khalifa.dds"
	expire = "2100.1.1"
	ideology = Kingdom
	traits = {
		king
		salafist_Kingdom
		pro_saudi
		ruthless
		deceitful
	}
}
create_field_marshal = {
	name = "Khalifa bin Ahmed Al Khalifa"
	picture = "generals/Portrait_Khalifa_bin_Ahmed_Al_Khalifa.dds"
	traits = { old_guard fast_planner }
	skill = 3
}

create_field_marshal = {
	name = "Dhiyab bin Saqr Al Nuaimi"
	picture = "generals/Portrait_Dhiyab_bin_Saqr_Al_Nuaimi.dds"
	traits = { defensive_doctrine }
	skill = 2
}

create_field_marshal = {
	name = "Hamad bin Abdullah Al Khalifa"
	picture = "generals/Portrait_Hamad_bin_Abdullah_Al_Khalifa.dds"
	traits = { logistics_wizard }
	skill = 2
}

create_corps_commander = {
	name = "Nasser bin Hamad Al Khalifa"
	picture = "generals/Portrait_Nasser_bin_Hamad_Al_Khalifa.dds"
	traits = { urban_assault_specialist }
	skill = 1
}