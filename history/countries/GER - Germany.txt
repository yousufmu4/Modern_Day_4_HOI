﻿capital = 45

oob = "GER_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1


	corvette_1 = 1
	corvette_2 = 1
	missile_corvette_1 = 1
	missile_corvette_2 = 1
	missile_corvette_3 = 1 #Braunschweig class

	frigate_1 = 1
	frigate_2 = 1 #Bremen class
	missile_frigate_1 = 1 
	missile_frigate_2 = 1 #Brandenburg class
	missile_frigate_3 = 1
	missile_frigate_4 = 1 #Baden-Württemberg class
	
	submarine_1 = 1
	diesel_attack_submarine_1 = 1
	diesel_attack_submarine_2 = 1
	diesel_attack_submarine_3 = 1
	diesel_attack_submarine_4 = 1 #Type 212
	diesel_attack_submarine_5 = 1 #Type 216
	
	#Lütjens-Class
	destroyer_1 = 1
	
	Rec_tank_1 = 1
	
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1

	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	command_control_equipment4 = 1
	
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	land_Drone_equipment2 = 3
	
	Anti_tank_0 = 1
	Anti_tank_1 = 1
	Anti_tank_2 = 1
	
	AT_upgrade_1 = 1
	AT_upgrade_2 = 1
	
	Heavy_Anti_tank_0 = 1
	Heavy_Anti_tank_1 = 1
	Heavy_Anti_tank_2 = 1
	
	Anti_Air_0 = 1
	Anti_Air_1 = 1
	AA_upgrade_1 = 1
	AA_upgrade_3 = 1
	
	SP_Anti_Air_0 = 1
	SP_Anti_Air_1 = 1
	combat_eng_equipment = 1
	
	#Panavia Tornado
	early_fighter = 1
	Strike_fighter1 = 1
	Strike_fighter2 = 1
	
	#EF-2000
	AS_Fighter1 = 1
	AS_Fighter2 = 1
	AS_upgrade_1 = 1
	AS_Fighter3 = 1 #4.5gen
	
	#Alpha Jet
	L_Strike_fighter1 = 1
    L_Strike_fighter2 = 1
	
	#Eurocopter Tiger
	early_helicopter = 1
	attack_helicopter1 = 1
	attack_helicopter2 = 1
	attack_helicopter3 = 1
	
	Early_APC = 1
	MBT_1 = 1
	MBT_2 = 1
	MBT_3 = 1
	MBT_4 = 1
	MBT_5 = 1
	
	ENG_MBT_1 = 1
	ENG_MBT_2 = 1
	ENG_MBT_3 = 1
	ENG_MBT_4 = 1
	ENG_MBT_5 = 1
	
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	util_vehicle_equipment_3 = 1
	util_vehicle_equipment_4 = 1
	
	Rec_tank_0 = 1
	Rec_tank_1 = 1
	Rec_tank_2 = 1
	
	IFV_1 = 1
	IFV_2 = 1
	IFV_3 = 1
	IFV_4 = 1
	IFV_5 = 1
	IFV_6 = 1
	
	APC_1 = 1
	APC_2 = 1
	APC_3 = 1
	APC_4 = 1
	APC_5 = 1
	APC_6 = 1
	
	
	
	gw_artillery = 1
	
	Arty_upgrade_1 = 1
	Arty_upgrade_3 = 1
	
	SP_arty_equipment_0 = 1
	SP_arty_equipment_1 = 1
	SP_arty_equipment_2 = 1
	
	SP_R_arty_equipment_0 = 1
	
	landing_craft = 1
	
	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1
	
}

add_ideas = {
	pop_050
	slight_corruption
	gdp_9
	christian
	volunteer_army
	volunteer_women
	defence_01
	german_legacy
	EU_member
	g7_member
	debt_40
	stable_growth
	bud_bal
	consumption_economy
	edu_04
	health_05
	social_06
	bureau_02
	police_02
	
	industrial_conglomerates
	Labour_Unions
	small_medium_business_owners
	intervention_limited_interventionism
	NATO_member
	western_country
	medium_far_right_movement
	large_green_movement
	civil_law
}
set_country_flag = bud_bal
set_country_flag = debt_40
set_country_flag = gdp_9
set_country_flag = enthusiastic_industrial_conglomerates
set_country_flag = positive_small_medium_business_owners

#NATO military access
diplomatic_relation = {
	country = ALB
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BEL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BUL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CAN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CRO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CZH
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = DEN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = EST
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = FRA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GRE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HUN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ICE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ITA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LAT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LIT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LUX
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HOL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = NOR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ROM
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLV
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SPR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = TUR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ENG
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = USA
	relation = military_access
	active = yes
}

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot
	complete_national_focus = Generic_30_IC_slot
	complete_national_focus = Generic_50_IC_slot
	complete_national_focus = Generic_85_IC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 80

		}

		fascism = {
			popularity = 0
		}
		
		communism = {

			popularity = 9
		}
		
		neutrality = {
			popularity = 6
		}
		
		Nationalist = {
			popularity = 5
		}
	}
	
	ruling_party = democratic
	last_election = "2013.9.22"
	election_frequency = 48
	elections_allowed = yes
}


set_convoys = 150


create_country_leader = {
	name = "Cem Özdemir"
	picture = "Cem_Ozdemir.dds"
	expire = "2060.1.1"
	ideology = Neutral_green
	traits = {
	neutrality_Neutral_green
	}
}

create_country_leader = {
	name = "Katrin Göring Eckardt"
	picture = "Katrin_Goring_Eckardt.dds"
	expire = "2060.1.1"
	ideology = Neutral_green
	traits = {
	neutrality_Neutral_green
	}
}

create_country_leader = {
	name = "Christian Lindner"
	picture = "lib_Christian_Lindner.dds"
	expire = "2060.1.1"
	ideology = liberalism
	traits = {
	
	}
}

create_country_leader = {
	name = "Sahra Wagenknecht"
	picture = "Sahra_Wagenknecht.dds"
	expire = "2060.1.1"
	ideology = anarchist_communism
	traits = {
	
	}
}

create_country_leader = {
	name = "Katja Kipping"
	picture = "Katja_Kipping.dds"
	expire = "2060.1.1"
	ideology = anarchist_communism
	traits = {
	
	}
}

create_country_leader = {
	name = "Frank Franz"
	picture = "Frank_Franz.dds"
	expire = "2065.1.1"
	ideology = Nat_Fascism
	traits = {
		nationalist_Nat_Fascism
		islamophobe
	}
}

create_country_leader = {
	name = "Alice Weidel"
	picture = "Alice_Weidel.dds"
	expire = "2065.1.1"
	ideology = Nat_populism
	traits = {
		nationalist_Nat_Populism
		islamophobe
	}
}

create_country_leader = {
	name = "Frauke Petry"
	picture = "frauke_petry.dds"
	expire = "2065.1.1"
	ideology = Nat_populism
	traits = {
		nationalist_Nat_Populism
	}
}

create_country_leader = {
	name = "Paul Ziemiak"
	picture = "paul_ziemiak_futcons.dds"
	expire = "2060.1.1"
	ideology = conservatism
	traits = {
		western_conservatism
	}
}

create_country_leader = {
	name = "Johanna Uekermann"
	picture = "Johanna_Uekermann.dds"
	expire = "2060.1.1"
	ideology = socialism
	traits = {
		western_socialism
	}
}

create_country_leader = {
	name = "Julia Klöckner"
	picture = "Julia_Klockner.dds"
	expire = "2050.1.1"
	ideology = conservatism
	traits = {
		writer
		western_conservatism
	}
}

create_country_leader = {
	name = "Andrea Nahles"
	picture = "Andrea_Nahles.dds"
	expire = "2050.1.1"
	ideology = socialism
	traits = {
		career_politician
		western_socialism
	}
}

create_country_leader = {
	name = "Annegret Kramp-Karrenbauer"
	picture = "Annegret_Kramp-Karrenbauer.dds"
	expire = "2050.1.1"
	ideology = conservatism
	traits = {
		career_politician
		western_conservatism
	}
}

create_country_leader = {
	name = "Martin Schulz"
	picture = "Martin_Schulz.dds"
	expire = "2025.1.1"
	ideology = socialism
	traits = {
		career_politician
		western_socialism
	}
}

create_country_leader = {
	name = "Angela Merkel"
	picture = "Angela_Merkel.dds"
	expire = "2050.1.1"
	ideology = conservatism
	traits = {
		scientist
		western_conservatism
		ruthless
		honest
		stubborn
		political_dancer
	}
}
create_field_marshal = {
	name = "Volker Wieker"
		picture = "gen_Volker_Wieker.dds"
	traits = {  }
	skill = 4
}
create_corps_commander = {
	name = "Andreas Marlow"
		picture = "gen_Andreas_Marlow.dds"
	traits = {  }
	skill = 3
}
create_corps_commander = {
	name = "Bernd Schutt"
		picture = "gen_Bernd_Schutt.dds"
	traits = {  }
	skill = 2
}
create_corps_commander = {
	name = "Egon Ramms"
		picture = "gen_Egon_Ramms.dds"
	traits = {  }
	skill = 1
}
create_corps_commander = {
	name = "Jorg Vollmer"
		picture = "gen_Jorg_Vollmer.dds"
	traits = {  }
	skill = 3
}
create_corps_commander = {
	name = "Klaus Reinhardt"
		picture = "gen_Klaus_Reinhardt.dds"
	traits = {  }
	skill = 1
}
create_corps_commander = {
	name = "Werner Freers"
		picture = "gen_Werner_Freers.dds"
	traits = {  }
	skill = 1
}
create_corps_commander = {
	name = "Markus Laubenthal"
		picture = "gen_Markus_Laubenthal.dds"
	traits = {  }
	skill = 2
}
create_navy_leader = {
	name = "Andreas Krause"
		picture = "adm_Andreas_Krause.dds"
	traits = {  }
	skill = 3
}
create_navy_leader = {
	name = "Manfred Nielson"
		picture = "adm_Manfred_Nielson.dds"
	traits = {  }
	skill = 3
}
create_navy_leader = {
	name = "Rainer Brinkmann"
		picture = "adm_Rainer_Brinkman.dds"
	traits = {  }
	skill = 3
}

create_field_marshal = {
	name = "Bruno Kasdorf"
	picture = "Portrait_Bruno_Kasdorf.dds"
	traits = { old_guard organisational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Jörg Vollmer"
	picture = "Portrait_Joerg_Vollmer.dds"
	traits = { old_guard thorough_planner }
	skill = 3
}

create_field_marshal = {
	name = "Werner Freers"
	picture = "Portrait_Werner_Freers.dds"
	traits = { old_guard fast_planner }
	skill = 4
}


create_field_marshal = {
	name = "Hans-Lothar Domröse"
	picture = "Portrait_Hans-Lothar_Domroese.dds"
	traits = { old_guard offensive_doctrine }
	skill = 4
}

create_field_marshal = {
	name = "Martin Schelleis"
	picture = "Portrait_Martin_Schelleis.dds"
	traits = { inspirational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Richard Roßmanith"
	picture = "Portrait_Richard_Rossmanith.dds"
	traits = { offensive_doctrine }
	skill = 3
}

create_field_marshal = {
	name = "Hans-Erich Antoni"
	picture = "Portrait_Hans-Ehrich_Antoni.dds"
	traits = { logistics_wizard }
	skill = 2
}

create_field_marshal = {
	name = "Joachim Wundrak"
	picture = "Portrait_Joachim_Wundrak.dds"
	traits = { fast_planner }
	skill = 3
}

create_field_marshal = {
	name = "Peter Bohrer"
	picture = "Portrait_Peter_Bohrer.dds"
	traits = { inspirational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Helmut Schütz"
	picture = "Portrait_Helmut_Schuetz.dds"
	traits = { logistics_wizard }
	skill = 3
}

create_field_marshal = {
	name = "Eberhard Zorn"
	picture = "Portrait_Eberhard_Zorn.dds"
	traits = { defensive_doctrine }
	skill = 3
}

create_field_marshal = {
	name = "Ansgar Rieks"
	picture = "Portrait_Ansgar_Rieks.dds"
	traits = { thorough_planner }
	skill = 2
}

create_field_marshal = {
	name = "Dieter Warnecke"
	picture = "Portrait_Dieter_Warnecke.dds"
	traits = { fast_planner }
	skill = 3
}

create_field_marshal = {
	name = "Josef Blotz"
	picture = "Portrait_Josef_Blotz.dds"
	traits = { thorough_planner }
	skill = 2
}

create_field_marshal = {
	name = "Dieter Naskrent"
	picture = "Portrait_Dieter_Naskrent.dds"
	traits = { inspirational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Karl Müllner"
	picture = "Portrait_Karl_Muellner.dds"
	traits = { organisational_leader }
	skill = 3
}

create_corps_commander = {
	name = "Ludwig Leinhos"
	picture = "Portrait_Ludwig_Leinhos.dds"
	traits = { trickster }
	skill = 3
}

create_corps_commander = {
	name = "Hans-Werner Wiermann"
	picture = "Portrait_Hans-Werner_Wiermann.dds"
	traits = { trickster }
	skill = 3
}

create_corps_commander = {
	name = "Franz Xaver Pfrengle"
	picture = "Portrait_Franz_Xaver_Pfrengle.dds"
	traits = { panzer_leader trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Stephan Thomas"
	picture = "Portrait_Stephan_Thomas.dds"
	traits = { panzer_leader ranger }
	skill = 2
}
	
create_corps_commander = {
	name = "Walter Huhn"
	picture = "Portrait_Walter_Huhn.dds"
	traits = { trickster }
	skill = 2
}

create_corps_commander = {
	name = "Gert Gawellek"
	picture = "Portrait_Gert_Gawellek.dds"
	traits = { commando trait_engineer ranger }
	skill = 1
}

create_corps_commander = {
	name = "Carsten Jahnel"
	picture = "Portrait_Carsten_Jahnel.dds"
	traits = { commando trickster urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Volker Thomas"
	picture = "Portrait_Volker_Thomas.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Christof Munzlinger"
	picture = "Portrait_Christof_Munzlinger.dds"
	traits = { panzer_leader bearer_of_artillery }
	skill = 1
}

create_corps_commander = {
	name = "Jürgen Weigt"
	picture = "Portrait_Juergen_Weigt.dds"
	traits = { panzer_leader }
	skill = 3
}

create_corps_commander = {
	name = "Dirk Backen"
	picture = "Portrait_Dirk_Backen.dds"
	traits = { desert_fox }
	skill = 2
}

create_corps_commander = {
	name = "Jürgen Setzer"
	picture = "Portrait_Juergen_Setzer.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Thorsten Poschwatta"
	picture = "Portrait_Thorsten_Poschwatta.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Henrik Riechert"
	picture = "Portrait_Henrik_Riechert.dds"
	traits = { naval_invader commando swamp_fox }
	skill = 1
}

create_corps_commander = {
	name = "Axel Meissel"
	picture = "Portrait_Axel_Meissel.dds"
	traits = { urban_assault_specialist bearer_of_artillery }
	skill = 1
}

create_corps_commander = {
	name = "Udo Schnittker"
	picture = "Portrait_Udo_Schnittker.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Sollfrank"
	picture = "Portrait_Alexander_Sollfrank.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Dirk Faust"
	picture = "Portrait_Dirk_Faust.dds"
	traits = { commando trickster }
	skill = 1
}

create_corps_commander = {
	name = "Ruprecht von Butler"
	picture = "Portrait_Ruprecht_von_Butler.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Heico Hübner"
	picture = "Portrait_Heico_Huebner.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Frank Leidenberger"
	picture = "Portrait_Frank_Leidenberger.dds"
	traits = { trait_engineer }
	skill = 3
}

create_corps_commander = {
	name = "Gert Hagemann"
	picture = "Portrait_Gert_Hagemann.dds"
	traits = { ranger }
	skill = 2
}

create_corps_commander = {
	name = "Walter Spindler"
	picture = "Portrait_Walter_Spindler.dds"
	traits = { fortress_buster }
	skill = 2
}

create_corps_commander = {
	name = "Manfred Hofmann"
	picture = "Portrait_Manfred_Hofmann.dds"
	traits = { trait_engineer fortress_buster }
	skill = 3
}

create_corps_commander = {
	name = "Dag Baehr"
	picture = "Portrait_Dag_Baehr.dds"
	traits = { commando urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Carsten Jacobson"
	picture = "Portrait_Carsten_Jacobson.dds"
	traits = { panzer_leader }
	skill = 3
}

create_corps_commander = {
	name = "Erich Pfeffer"
	picture = "Portrait_Erich_Pfeffer.dds"
	traits = { trait_mountaineer winter_specialist }
	skill = 3
}

create_corps_commander = {
	name = "Erhard Bühler"
	picture = "Portrait_Erhard_Buehler.dds"
	traits = { panzer_leader }
	skill = 3
}

create_corps_commander = {
	name = "Gunter Schneider"
	picture = "Portrait_Gunter_Schneider.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Jörg See"
	picture = "Portrait_Joerg_See.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Oliver Kohl"
	picture = "Portrait_Oliver_Kohl.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Kai Rohrschneider"
	picture = "Portrait_Kai_Ronald_Rohrschneider.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Wilhelm Grün"
	picture = "Portrait_Wilhelm_Gruen.dds"
	traits = { ranger }
	skill = 2
}

create_navy_leader = {
	name = "Axel Schimpf"
	picture = "Portrait_Axel_Schimpf.dds"
	traits = { seawolf }
	skill = 3
}

create_navy_leader = {
	name = "Joachim Rühle"
	picture = "Portrait_Joachim_Ruehle.dds"
	traits = { old_guard_navy }
	skill = 3
}

create_navy_leader = {
	name = "Carsten Stawitzki"
	picture = "Portrait_Carsten_Stawitzki.dds"
	traits = { air_controller }
	skill = 2
}

create_navy_leader = {
	name = "Thomas Jugel"
	picture = "Portrait_Thomas_Jugel.dds"
	traits = { fly_swatter }
	skill = 2
}

create_navy_leader = {
	name = "Jan Christian Kaack"
	picture = "Portrait_Jan_Christian_Kaack.dds"
	traits = { blockade_runner }
	skill = 1
}

create_navy_leader = {
	name = "Christoph Müller-Meinhard"
	picture = "Portrait_Christoph_Mueller-Meinhard.dds"
	traits = { ironside }
	skill = 1
}

create_navy_leader = {
	name = "Jean Martens"
	picture = "Portrait_Jean_Martens.dds"
	traits = { spotter }
	skill = 2
}

create_navy_leader = {
	name = "Axel Deertz"
	picture = "Portrait_Axel_Deertz.dds"
	traits = { spotter }
	skill = 1
}

create_navy_leader = {
	name = "Jens Nemeyer"
	picture = "Portrait_Jens_Nemeyer.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Kay-Achim Schönbach"
	picture = "Portrait_Kay-Achim_Schoenbach.dds"
	traits = { spotter }
	skill = 1
}

create_navy_leader = {
	name = "Rainer Endres"
	picture = "Portrait_Rainer_Endres.dds"
	traits = { blockade_runner }
	skill = 1
}

## Land variants
create_equipment_variant = {
	name = "Leopard 1A1"
	type = MBT_Equipment_2
	upgrades = {
		tank_reliability_upgrade = 1
		tank_engine_upgrade = 0
		tank_armor_upgrade = 1
		tank_gun_upgrade = 1
	}
	obsolete = yes
}
create_equipment_variant = {
	name = "Leopard 1A5"
	type = MBT_Equipment_2
	upgrades = {
		tank_reliability_upgrade = 1
		tank_engine_upgrade = 0
		tank_armor_upgrade = 1
		tank_gun_upgrade = 3
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "Leopard 2A5"
	type = MBT_Equipment_4
	upgrades = {
		tank_reliability_upgrade = 1
		tank_engine_upgrade = 0
		tank_armor_upgrade = 2
		tank_gun_upgrade = 1
	}
}

create_equipment_variant = {
	name = "Leopard 2A6"
	type = MBT_Equipment_4
	upgrades = {
		tank_reliability_upgrade = 1
		tank_engine_upgrade = 0
		tank_armor_upgrade = 3
		tank_gun_upgrade = 3
	}
}

create_equipment_variant = {
	name = "Henschel UR-416"
	type = APC_Equipment_1
	upgrades = {
		tank_reliability_upgrade = 0
		tank_engine_upgrade = 2
		tank_armor_upgrade = 0
		tank_gun_upgrade = 0
	}
	obsolete = yes
}

## Naval variants
create_equipment_variant = {
	name = "Type 218SG"
	type = diesel_attack_submarine_5
	upgrades = {
		
	}
}

create_equipment_variant = {
	name = "Type 209-Class"
	type = diesel_attack_submarine_2
	upgrades = {
		D_att_sub_displacement = 1
		D_att_sub_sonar = 1
		D_att_sub_stealth = 1
	}
	
	obsolete = yes
}
create_equipment_variant = {
	name = "Heroine-class"
	type = diesel_attack_submarine_2
	upgrades = {
		D_att_sub_displacement = 1
		D_att_sub_sonar = 1
		D_att_sub_stealth = 1
	}
	
	obsolete = yes
}

create_equipment_variant = {
	name = "TR-1700 Santa Cruz-Class"
	type = diesel_attack_submarine_3
	upgrades = {
		D_att_sub_displacement = 1
		D_att_sub_sonar = 0
		D_att_sub_stealth = 1
	}
	
	obsolete = yes
}

create_equipment_variant = {
	name = "Type 1500-Class"
	type = diesel_attack_submarine_3
	upgrades = {
		D_att_sub_displacement = 3
		D_att_sub_sonar = 0
		D_att_sub_stealth = 0
	}
	
	obsolete = yes
}

create_equipment_variant = {
	name = "Type 214-Class"
	type = diesel_attack_submarine_4
	upgrades = {
		D_att_sub_displacement = 1
		D_att_sub_sonar = 0
		D_att_sub_stealth = 0
	}
	
	obsolete = yes
}

create_equipment_variant = {
	name = "Kasturi-Class"
	type = missile_corvette_1
	upgrades = {
		corvette_vls_upgrade = 1
		corvette_asw_upgrade = 1
		corvette_cm_upgrade = 1
		corvette_radar_upgrade = 0
		corvette_stealth_upgrade = 1
		corvette_ground_support = 1
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "Almirante Padilla-Class"
	type = missile_corvette_1
	upgrades = {
		corvette_vls_upgrade = 2
		corvette_asw_upgrade = 1
		corvette_cm_upgrade = 1
		corvette_radar_upgrade = 1
		corvette_stealth_upgrade = 1
		corvette_ground_support = 1
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "MEKO 360-Class"
	type = missile_frigate_1
	upgrades = {
		frigate_vls_upgrade = 2
		frigate_asw_upgrade = 0
		frigate_cm_upgrade = 0
		frigate_radar_upgrade = 0
		frigate_stealth_upgrade = 0
	}
	
	obsolete = yes
}
create_equipment_variant = {
	name = "Barbaros-Class"
	type = missile_frigate_1
	upgrades = {
		frigate_vls_upgrade = 2
		frigate_asw_upgrade = 0
		frigate_cm_upgrade = 0
		frigate_radar_upgrade = 0
		frigate_stealth_upgrade = 0
	}
	
	obsolete = yes
}
create_equipment_variant = {
	name = "Yavuz class"
	type = missile_frigate_1
	upgrades = {
		frigate_vls_upgrade = 2
		frigate_asw_upgrade = 0
		frigate_cm_upgrade = 0
		frigate_radar_upgrade = 0
		frigate_stealth_upgrade = 0
	}
	
	obsolete = yes
}
create_equipment_variant = {
	name = "Preveze class"
	type = diesel_attack_submarine_2
	upgrades = {
		D_att_sub_displacement = 0
		D_att_sub_sonar = 0
		D_att_sub_stealth = 0
	}
}
create_equipment_variant = {
	name = "Gür-class"
	type = diesel_attack_submarine_2
	upgrades = {
		D_att_sub_displacement = 0
		D_att_sub_sonar = 0
		D_att_sub_stealth = 0
	}
}
create_equipment_variant = {
	name = "MEKO A-200AN-Class"
	type = missile_frigate_1
	upgrades = {
		frigate_vls_upgrade = 2
		frigate_asw_upgrade = 1
		frigate_cm_upgrade = 1
		frigate_radar_upgrade = 1
		frigate_stealth_upgrade = 1
	}
	obsolete = yes
}
create_equipment_variant = {
	name = "Valour-class"
	type = missile_frigate_1
	upgrades = {
		frigate_vls_upgrade = 2
		frigate_asw_upgrade = 1
		frigate_cm_upgrade = 1
		frigate_radar_upgrade = 1
		frigate_stealth_upgrade = 1
	}
	obsolete = yes
}
create_equipment_variant = {
	name = "Victory-Class"
	type = missile_corvette_1 #Victory-Class
	upgrades = {
		
	}
	obsolete = yes
}
create_equipment_variant = {
	name = "Valour-class"
	type = missile_frigate_2 #Valour-class
	upgrades = {
		
	}
	obsolete = yes
}
create_equipment_variant = {
	name = "Dolphin-class"
	type = diesel_attack_submarine_2
	upgrades = {
		D_att_sub_displacement = 1
		D_att_sub_sonar = 1
		D_att_sub_stealth = 1
	}
	
	obsolete = yes
}
create_equipment_variant = {
	name = "Dolphin II-class"
	type = diesel_attack_submarine_3
	upgrades = {
		D_att_sub_displacement = 1
		D_att_sub_sonar = 1
		D_att_sub_stealth = 1
	}
	
	obsolete = yes
}
