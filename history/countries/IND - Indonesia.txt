﻿capital = 631

oob = "IND_2017"

# Starting tech

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	night_vision_2 = 1
	
	command_control_equipment = 1
	command_control_equipment1 = 1
	
	#Pindad SS4
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	infantry_weapons5 = 1
	
	#Pindad Kaplan
	Rec_tank_0 = 1
	Rec_tank_1 = 1
	Rec_tank_2 = 1
	Rec_tank_3 = 1
	
	#Pindad Anoa
	Early_APC = 1
	APC_1 = 1
	APC_2 = 1
	APC_3 = 1
	APC_4 = 1
	APC_5 = 1
	
	#Pindad Komodo
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	util_vehicle_equipment_3 = 1
	util_vehicle_equipment_4 = 1
	util_vehicle_equipment_5 = 1
	
	#R-Han 122
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	SP_arty_equipment_1 = 1
	SP_arty_equipment_2 = 1
	SP_R_arty_equipment_0 = 1
	SP_R_arty_equipment_1 = 1
	SP_R_arty_equipment_2 = 1
	Arty_upgrade_1 = 1
	Arty_upgrade_3 = 1
	
	combat_eng_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	Anti_Air_0 = 1
	IFV_1 = 1
	MBT_1 = 1
	ENG_MBT_1 = 1
	
	landing_craft = 1
}

add_ideas = {
	pop_050
	unrestrained_corruption
	gdp_3
	sunni
	saudi_aid
	rentier_state
    export_economy
	debt_20
	fast_growth
	bud_neg_04
	defence_01
	edu_02
	health_01
	social_01
	bureau_02
	police_02
	export_economy
	volunteer_army
	volunteer_women
	small_medium_business_owners
	The_Ulema
	landowners
	hybrid
}
set_country_flag = bud_neg_04
set_country_flag = debt_20
set_country_flag = gdp_3
set_country_flag = Major_Importer_ENG_Arms
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_landowners


#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot
	complete_national_focus = Generic_30_IC_slot

add_opinion_modifier = { target = CHI modifier = bamboo_network }

set_convoys = 100

set_politics = {

	parties = {
		democratic = { 
			popularity = 0
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 23
		}
		
		neutrality = {
			popularity = 66
		}
		
		Nationalist = {
			popularity = 12
		}
	}
	
	ruling_party = neutrality
	last_election = "2014.4.9"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Sri Mulyani Indrawati"
	desc = ""
	picture = "IND_Sri_Mulyani_Indrawati.dds"
	expire = "2050.1.1"
	ideology = conservatism
	traits = {
	}
}

create_country_leader = {
	name = "Grace Natalie"
	desc = ""
	picture = "IND_Grace_Natalie.dds"
	expire = "2050.1.1"
	ideology = conservatism
	traits = {
	}
}

create_country_leader = {
	name = "Basuki Tjahaja Purnama"
	desc = ""
	picture = "IND_Basuki_Tjahaja_Purnama.dds"
	expire = "2050.1.1"
	ideology = Conservative
	traits = {
	}
}

create_country_leader = {
	name = "Ridwan Kamil"
	desc = ""
	picture = "IND_Ridwan_Kamil.dds"
	expire = "2050.1.1"
	ideology = Conservative
	traits = {
	}
}

create_country_leader = {
	name = "Hary Tanoesoedibjo"
	desc = ""
	picture = "IND_Hary_Tanoesoedibjo.dds"
	expire = "2050.1.1"
	ideology = Autocracy
	traits = {
	}
}

create_country_leader = {
	name = "Wiranto"
	desc = ""
	picture = "IND_Wiranto.dds"
	expire = "2050.1.1"
	ideology = Neutral_Autocracy
	traits = {
	}
}

create_country_leader = {
	name = "Setya Novanto"
	desc = ""
	picture = "IND_Setya_Novanto.dds"
	expire = "2050.1.1"
	ideology = Neutral_conservatism
	traits = {
	}
}

create_country_leader = {
	name = "Tri Rismaharini"
	desc = ""
	picture = "IND_Tri_Rismaharini.dds"
	expire = "2050.1.1"
	ideology = Neutral_conservatism
	traits = {
	}
}

create_country_leader = {
	name = "Anies Baswedan"
	desc = ""
	picture = "IND_Anies_Baswedan.dds"
	expire = "2050.1.1"
	ideology = neutral_Social
	traits = {
	}
}

create_country_leader = {
	name = "Agus Harimurti Yudhoyono"
	desc = ""
	picture = "IND_Agus_Harimurti_Yudhoyono.dds"
	expire = "2050.1.1"
	ideology = oligarchism
	traits = {
	}
}

create_country_leader = {
	name = "Joko Widodo"
	desc = "POLITICS_Joko_Widodo_DESC"
	picture = "Joko_Widodo.dds"
	expire = "2050.1.1"
	ideology = Neutral_conservatism
	traits = {
		businessman
		neutrality_Neutral_conservatism
		likeable
	}
}

create_country_leader = {
	name = "Gatot Nurmantyo"
	desc = ""
	picture = "IND_Gatot_Nurmantyo.dds"
	expire = "2050.1.1"
	ideology = Nat_Autocracy
	traits = {
	}
}

create_country_leader = {
	name = "Fadli Zon"
	desc = ""
	picture = "IND_Fadli_Zon.dds"
	expire = "2050.1.1"
	ideology = Nat_Populism
	traits = {
	}
}

create_country_leader = {
	name = "Prabowo Subianto"
	desc = ""
	picture = "IND_Prabowo_Subianto.dds"
	expire = "2050.1.1"
	ideology = Nat_Populism
	traits = {
	}
}

create_country_leader = {
	name = "Abu Bakar Ba'asyir"
	desc = ""
	picture = "IND_Abu_Bakar_Baasyir.dds"
	expire = "2050.1.1"
	ideology = Caliphate
	traits = {
	}
}

create_country_leader = {
	name = "Felix Siauw"
	desc = ""
	picture = "IND_Felix_Siauw.dds"
	expire = "2050.1.1"
	ideology = Caliphate
	traits = {
	}
}

create_country_leader = {
	name = "Habib Rizieq Shihab"
	desc = ""
	picture = "IND_Habib_Rizieq_Shihab.dds"
	expire = "2050.1.1"
	ideology = Caliphate
	traits = {
	}
}

create_corps_commander = {
	name = "Tatang Sulaiman"
	picture = "IND_gen_Tatang_Sulaiman.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Moch. Fachruddin"
	picture = "IND_gen_Moch_Fachruddin.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Lodewyk Pusung"
	picture = "IND_gen_Lodewyk_Pusung.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Cucu Sumantri"
	picture = "IND_gen_Cucu_Sumantri.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Sudirman"
	picture = "IND_gen_Sudirman.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Anto Mukti Putranto"
	picture = "IND_gen_Anto_Mukti_Putranto.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Teddy Lhaksmana"
	picture = "IND_gen_Teddy_Lhaksmana.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Jaswandi"
	picture = "IND_gen_Jaswandi.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Muhammad Herindra"
	picture = "IND_gen_Muhammad_Herindra.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Wuryanto"
	picture = "IND_gen_Wuryanto.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Kustanto Widiatmoko"
	picture = "IND_gen_Kustanto_Widiatmoko.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Arip Rahman"
	picture = "IND_gen_Arip_Rahman.dds"
	traits = {  }
	skill = 1
}
create_field_marshal = {
	name = "Gatot Nurmantyo"
	picture = "generals/Portrait_Gatot_Nurmantyo.dds"
	traits = { old_guard organisational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Ryamizard Ryacudu"
	picture = "generals/Portrait_Ryamizard_Ryacudu.dds"
	traits = { old_guard defensive_doctrine }
	skill = 4
}

create_field_marshal = {
	name = "Mulyono"
	picture = "generals/Portrait_Mulyono.dds"
	traits = { old_guard inspirational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Hinsa Siburian"
	picture = "generals/Portrait_Hinsa_Siburian.dds"
	traits = { thorough_planner }
	skill = 3
}

create_field_marshal = {
	name = "Edy Rahmayadi"
	picture = "generals/Portrait_Edy_Rahmayadi.dds"
	traits = { fast_planner }
	skill = 3
}

create_field_marshal = {
	name = "Agus Kriswanto"
	picture = "generals/Portrait_Agus_Kriswanto.dds"
	traits = { offensive_doctrine }
	skill = 3
}

create_corps_commander = {
	name = "Benny Susianto"
	picture = "generals/Portrait_Benny_Susianto.dds"
	traits = { fortress_buster }
	skill = 2
}

create_corps_commander = {
	name = "Madsuni"
	picture = "generals/Portrait_Madsuni.dds"
	traits = { commando urban_assault_specialist }
	skill = 2
}

create_corps_commander = {
	name = "Richard Tampubolon"
	picture = "generals/Portrait_Richard_Tampubolon.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Moch Fachrudin"
	picture = "generals/Portrait_Moch_Fachrudin.dds"
	traits = { bearer_of_artillery }
	skill = 2
}

create_corps_commander = {
	name = "Achmad Daniel Chardin"
	picture = "generals/Portrait_Achmad_Daniel_Chardin.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Cucu Somantri"
	picture = "generals/Portrait_Cucu_Somantri.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Tiopan Aritonang"
	picture = "generals/Portrait_Tiopan_Aritonang.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Marga Taufiq"
	picture = "generals/Portrait_Marga_Taufiq.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Eko Margiyono"
	picture = "generals/Portrait_Eko_Margiyono.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Yosua Pandit Sembiring"
	picture = "generals/Portrait_Yosua_Pandit_Sembiring.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "MS Fadhilah"
	picture = "generals/Portrait_Fadhilah.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Widodo Iryansyah"
	picture = "generals/Portrait_Widodo_Iryansyah.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Sonhadji"
	picture = "generals/Portrait_Sonhadji.dds"
	traits = { fortress_buster }
	skill = 2
}

create_corps_commander = {
	name = "Agus Surya Bakti"
	picture = "generals/Portrait_Agus_Surya_Bakti.dds"
	traits = { hill_fighter }
	skill = 2
}

create_corps_commander = {
	name = "Komaruddin Simanuntak"
	picture = "generals/Portrait_Komaruddin_Simanuntak.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Stephanus Tri Mulyono"
	picture = "generals/Portrait_Stephanus_Tri_Mulyono.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Andika Perkasa"
	picture = "generals/Portrait_Andika_Perkasa.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Sulaiman Agusto"
	picture = "generals/Portrait_Sulaiman_Agusto.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Ganip Warsito"
	picture = "generals/Portrait_Ganip_Warsito.dds"
	traits = { bearer_of_artillery }
	skill = 2
}

create_corps_commander = {
	name = "Santos Gunawan Matondang"
	picture = "generals/Portrait_Santos_Gunawan_Matondang.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Doni Monardo"
	picture = "generals/Portrait_Doni_Monardo.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Tri Soewandono"
	picture = "generals/Portrait_Tri_Soewandono.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "George Elnadus Supit"
	picture = "generals/Portrait_George_Elnadus_Supit.dds"
	traits = { trickster }
	skill = 2
}

create_corps_commander = {
	name = "Herman Asaribab"
	picture = "generals/Portrait_Herman_Asaribab.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Joppye Onesimus Wayangkau"
	picture = "generals/Portrait_Joppye_Onesimus_Wayangkau.dds"
	traits = { jungle_rat }
	skill = 2
}

create_corps_commander = {
	name = "Purnawan Widi Andaru"
	picture = "generals/Portrait_Purnawan_Widi_Andaru.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Dedy Iswanto"
	picture = "generals/Portrait_Dedy_Iswanto.dds"
	traits = { urban_assault_specialist }
	skill = 2
}

create_corps_commander = {
	name = "Suko Pranoto"
	picture = "generals/Portrait_Suko_Pranoto.dds"
	traits = { fortress_buster }
	skill = 2
}

create_corps_commander = {
	name = "Hadi Prasojo"
	picture = "generals/Portrait_Hadi_Prasojo.dds"
	traits = { bearer_of_artillery }
	skill = 2
}

create_corps_commander = {
	name = "Taat Agus Budianto"
	picture = "generals/Portrait_Taat_Agus_Budiyanto.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Alfret Denny D. Tuejeh"
	picture = "generals/Portrait_Alfret_Denny_Tuejeh.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Dody Usodo"
	picture = "generals/Portrait_Dody_Usodo.dds"
	traits = { trickster trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Dody Zulkarnaen"
	picture = "generals/Portrait_Dody_Zulkarnaen.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Frits Wilem Rizard Pelamonia"
	picture = "generals/Portrait_Frits_Willem.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Primadi Saiful Sulun"
	picture = "generals/Portrait_Primadi_Saiful_Sulun.dds"
	traits = { commando ranger }
	skill = 1
}

create_corps_commander = {
	name = "Febriel B.S."
	picture = "generals/Portrait_Febriel.dds"
	traits = { commando ranger }
	skill = 1
}

create_corps_commander = {
	name = "Hengki Yuda Setiawan"
	picture = "generals/Portrait_Hengki_Yuda_Setiawan.dds"
	traits = { commando ranger }
	skill = 1
}

create_corps_commander = {
	name = "Ainurrahman"
	picture = "generals/Portrait_Ainurrahman.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Agus Suhardi"
	picture = "generals/Portrait_Agus_Suhardi.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Bambang Suswantono"
	picture = "generals/Portrait_Bambang_Suswantono.dds"
	traits = { naval_invader }
	skill = 2
}

create_corps_commander = {
	name = "Hasanuddin"
	picture = "generals/Portrait_Hasanuddin.dds"
	traits = { naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Kasirun Situmorang"
	picture = "generals/Portrait_Kasirun_Situmorang.dds"
	traits = { naval_invader trickster }
	skill = 1
}

create_corps_commander = {
	name = "Rudy Andi Hamzah"
	picture = "generals/Portrait_Rudy_Andi_Hamzah.dds"
	traits = { naval_invader ranger }
	skill = 1
}

create_corps_commander = {
	name = "I Ketut Suardana"
	picture = "generals/Portrait_I_Ketut_Suardana.dds"
	traits = { naval_invader urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Supriyono"
	picture = "generals/Portrait_Supriyono.dds"
	traits = { naval_invader commando ranger }
	skill = 1
}

create_navy_leader = {
	name = "Ade Supandi"
	picture = "admirals/Portrait_Ade_Supandi.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 4
}

create_navy_leader = {
	name = "Achmad Taufiqoerrochman"
	picture = "admirals/Portrait_Achmad_Taufiq.dds"
	traits = { ironside }
	skill = 3
}

create_navy_leader = {
	name = "Darwanto"
	picture = "admirals/Portrait_Darwanto.dds"
	traits = { blockade_runner }
	skill = 2
}

create_navy_leader = {
	name = "Aan Kurnia"
	picture = "admirals/Portrait_Aan_Kurnia.dds"
	traits = { seawolf }
	skill = 2
}

create_navy_leader = {
	name = "Yudo Margono"
	picture = "admirals/Portrait_Yudo_Margono.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Agung Prasetiawan"
	picture = "admirals/Portrait_Agung_Prasetiawan.dds"
	traits = {  }
	skill = 2
}

create_navy_leader = {
	name = "Putu Wijamahadi"
	picture = "admirals/Portrait_Putu_Wijamahadi.dds"
	traits = { superior_tactician }
	skill = 2
}

create_navy_leader = {
	name = "Arusukmono Indra Sucahyo"
	picture = "admirals/Portrait_Arusukmono_Indra_Sucahyo.dds"
	traits = { spotter }
	skill = 2
}

create_navy_leader = {
	name = "Wuspo Lukito"
	picture = "admirals/Portrait_Wuspo_Lukito.dds"
	traits = { fly_swatter }
	skill = 2
}

create_navy_leader = {
	name = "Manahan Simorangkir"
	picture = "admirals/Portrait_Manahan_Simorangkir.dds"
	traits = { air_controller }
	skill = 1
}

#NOTES FROM ANNI
# LEADER

# Western
# 1. Grace Natalie (PSI)			: presenter turned politican, found Indonesian Solidarity Party, (allegedly) the only party in Indonesia with transparent recruitment process
# 2. Sri Mulyani Indrawati (PDIP)		: current economic minister, former Managing Director of World Bank Group, oft prided upon by folks and regarded as competent

# Emerging (note: all of this can be included in non aligned tbh, plus Basuki can be Western)
# 1. Ridwan Kamil	(PKS)			: major of Bandung, architect, regarded as a good governor, oft speaks against extreme political Islam, terms due in June 2018
# 2. Basuki Tjahaja Purnama (PDIP)	: (former) major of Jakarta, ethnic Chinese, lost to Anies (see below) last May, since May in prison for blashpermy, regarded as competent
# 3. Hary Tanoesoedibjo (Perindo)		: media mogul, become politican, made his own personal party (Perindo) ethnic Chinese, 

# Non aligned
# 1. Joko Widodo (PDIP)			: businessman, turned major of Solo, turned major of Jakarta, turned President. On first term, will certainly seek reelection
# 2. Agus Harimurti Yudhoyono (Demokrat)	: son of former president (SBY), tried (and failed) to be the major of Jakarta this year, regarded as SBY extension in politics
# 3. Anies Baswedan 			: governor of Jakarta, beats the incumbent this year in May through hotly contested election via less savoury campaign
# 4. Tri Rismaharani (PDIP)		: major of Surabaya, regarded as a good governor, famous for closing prostitution localization
# 5. Setya Novanto (Golkar)		: former speaker of the legislative body (DPR) until he was implicated in Identity Card corruption scandal
# 6. Wiranto (Hanura)			: former General, regarded as less shady than Prabowo during their time in East Timor

# Nationalist
# 1. Prabowo Subianto (Gerindra)		: contestant for last presidential election, former Lieutnant General, implicated in some shady shit in East Timor, use populist rhetoric in last election
# 2. Fadli Zon (Gerindra)			: current speaker of DPR, oft. regarded as corrupt and incompetent, Prabowo's ally [probably should be non aligned]
# 2. Gatot Nurmaynto			: former General in the armed force, made some... interesting remarks about commies, Chinese, and such
	
# Salafi
# 1. Habib Rizieq Shihab			: leader of Islamic Defender Front, the most vocal of Islamist organization leader, since July fled to Saudi Arabia to avoid charge regarding pornography
# 2. Felix Siaw				: Indonesian Hizb at Tahrir most vocal activist, generally speaks pro Caliphate ant secular stuff
# 3. Abu Bakar Ba'asyir			: in prison since 2002, have ties to Jemaah Islamiyah terrorist group, didn't do much today

# Probably I'll do traits later
# The next election is in April 2019


# PARTY

# Western
# 1. Partai Solidaritas Indonesia (PSI)

# Emerging
# 1. Partai Persatuan Indonesia (Perindo)
# 2. Partai Keadilan Sejahtera (PKS)

# Non aligned
# 1. Partai Demokrasi Indonesia Perjuangan (PDIP)
# 2. Partai Demokrat (Demokrat)
# 3. Golongan Karya (Golkar)
# 4. Partai Hati Nurani Rakyat

# Nationalist
# 1. Gerakan Indonesia Raya (Gerindra)

# Salafi
# 1. Front Pembela Islam (FPI)
# 2. Hizbut Tahrir

