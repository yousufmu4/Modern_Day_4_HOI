﻿capital = 522

oob = "VIE_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 infiltration_assault = 1 
 defence_in_depth = 1 
 early_tunnel_warfare = 1 
 massed_artillery = 1
	infantry_weapons = 1
	combat_eng_equipment = 1
	command_control_equipment = 1
	land_Drone_equipment = 1
	Rec_tank_0 = 1
	MBT_1 = 1
	ENG_MBT_1 = 1
	util_vehicle_equipment_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	

	gw_artillery = 1
	Arty_upgrade_1 = 1
	SP_arty_equipment_0 = 1
	SP_R_arty_equipment_0 = 1

	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1

	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1

	early_fighter = 1
	MR_Fighter1 = 1
	AS_Fighter1 = 1

	early_helicopter = 1
	transport_helicopter1 = 1
	attack_helicopter1 = 1

	submarine_1 = 1
	diesel_attack_submarine_1 = 1
	missile_corvette_1 = 1
	corvette_1 = 1
	
	landing_craft = 1
	
}

add_ideas = {
	pop_050
	unrestrained_corruption
	buddism
	gdp_2
	debt_20
	fast_growth
	bud_neg_04
	defence_02
	edu_03
	health_02
	social_02
	bureau_03
	police_05
	draft_army 
	volunteer_women 
	farmers
	communist_cadres
	industrial_conglomerates
	civil_law
}
set_country_flag = bud_neg_04
set_country_flag = debt_20
set_country_flag = gdp_2
set_country_flag = TPP_Signatory
set_country_flag = Major_Importer_RUS_Arms
set_country_flag = enthusiastic_communist_cadres
set_country_flag = positive_farmers
set_country_flag = positive_industrial_conglomerates

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot

add_opinion_modifier = { target = CHI modifier = bamboo_network }
add_opinion_modifier = { target = CHI modifier = hostile_status }

set_convoys = 25


set_politics = {

	parties = {
		democratic = { 
			popularity = 36
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 19
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 45
		}
	}
	
	ruling_party = neutrality
	last_election = "1932.11.8"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Nguyen Phu Trong"
	desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" 
	picture = "Nguyen_Phu_Trong.dds"
	expire = "2050.1.1"
	ideology = Neutral_Communism
	traits = {
		writer
		neutrality_Neutral_Communism
		pro_american
		polished
		rational
		cautious
	}
}
create_corps_commander = {
	name = "Tran Viet Khoa"
		picture = ""
	traits = { hill_fighter trait_engineer jungle_rat }
	skill = 3
}
create_corps_commander = {
	name = "Pham Van Hung"
		picture = ""
	traits = { trait_mountaineer jungle_rat }
	skill = 4
}
create_corps_commander = {
	name = "Nguyen Duc Hai"
		picture = ""
	traits = { hill_fighter trait_engineer jungle_rat }
	skill = 2
}
create_corps_commander = {
	name = "Nguyen Hoang"
		picture = ""
	traits = { hill_fighter trait_engineer }
	skill = 3
}
create_navy_leader = {
	name = "Pham Hoai Nam"
		picture = ""
	traits = { }
	skill = 3
}
