﻿capital = 344

oob = "BEN_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1

	landing_craft = 1
}
add_ideas = {
	pop_050
	unrestrained_corruption
	christian
	gdp_1
	debt_20
    fast_growth
	bud_neg_08
	defence_01
	edu_03
	health_01
	social_01
	bureau_03
	police_02
	draft_army
	volunteer_women
	international_bankers
	farmers
	small_medium_business_owners
	civil_law
	}
set_country_flag = bud_neg_08
set_country_flag = debt_20
set_country_flag = gdp_1
set_country_flag = positive_international_bankers
set_country_flag = negative_farmers
set_country_flag = negative_small_medium_business_owners

#Nat focus
	complete_national_focus = bonus_tech_slots

set_convoys = 10


set_politics = {
	parties = {
		democratic = { 
			popularity = 2
		}
		fascism = {
			popularity = 4
		}
		
		communism = {
			popularity = 28
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 66
	}
	}
	
	ruling_party = neutrality
	last_election = "2016.3.20"
	election_frequency = 60
	elections_allowed = yes
	
}
create_country_leader = {
	name = "Thomas Boni Yayi"
	desc = ""
	picture = "BEN_Thomas_Boni_Yayi.dds"
	expire = "2065.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Patrice Talon"
	desc = ""
	picture = "BEN_patrice_talon.dds"
	expire = "2065.1.1"
	ideology = neutral_social
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Laurent Amoussou"
	picture = "Portrait_Laurent_Amoussou.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "O. Laleyle"
	picture = "Portrait_O_Laleyle.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Albert Badou"
	picture = "Portrait_Albert_Badou.dds"
	traits = {  }
	skill = 1
}