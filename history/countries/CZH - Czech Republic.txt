﻿capital = 117

oob = "CZH_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	night_vision_2 = 1
	
	#T-72M4 CZ
	MBT_1 = 1
	MBT_2 = 1
	MBT_3 = 1
	
	early_APC = 1
	
	#BVP-2
	IFV_1 = 1
	IFV_2 = 1
	IFV_3 = 1
	
	#SpGH DANA
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	SP_arty_equipment_1 = 1
	
	#M1982 PRAM-L 120mm Mortar
	
	
	
	
	#Tatra T815
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	L_Strike_fighter1 = 1
	L_Strike_fighter2 = 1
	
	#CZ 805 BREN
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	infantry_weapons5 = 1
	
	#OT-64
	APC_1 = 1
	
	#OT-62 TOPAS
	
	
	#RM-51 & RM-70
	SP_R_arty_equipment_0 = 0
	SP_R_arty_equipment_1 = 0
	
	#For templates
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	Anti_Air_0 = 1
	
	
}

add_ideas = {
	pop_050
	small_medium_business_owners
	Labour_Unions
	#Mass_Media
	widespread_corruption
	gdp_7
	pluralist
	EU_member
	debt_20
	stable_growth
	bud_bal
	defence_01
	edu_04
	health_04
	social_04
	bureau_03
	police_04
	volunteer_army
	volunteer_women
	NATO_member
	western_country
	medium_far_right_movement
	landowners
	industrial_conglomerates
	small_medium_business_owners
	civil_law
}
set_country_flag = bud_bal
set_country_flag = debt_20
set_country_flag = gdp_7
set_country_flag = positive_landowners
set_country_flag = positive_industrial_conglomerates
set_country_flag = positive_small_medium_business_owners

#NATO military access
diplomatic_relation = {
	country = ALB
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BEL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BUL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CAN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CRO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = DEN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = EST
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = FRA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GER
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GRE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HUN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ICE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ITA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LAT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LIT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LUX
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HOL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = NOR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ROM
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLV
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SPR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = TUR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ENG
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = USA
	relation = military_access
	active = yes
}

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 71
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 16
		}
		
		neutrality = {
			popularity = 8
		}
		
		Nationalist = {
			popularity = 5
		}
	}
	
	ruling_party = democratic
	last_election = "2013.10.26"
	election_frequency = 48
	elections_allowed = yes
}


set_convoys = 5


create_country_leader = {
	name = "Andrej Babis"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "andrej_babis.dds"
	expire = "2065.1.1"
	ideology = liberalism
	traits = {
	
	}
}

create_country_leader = {
	name = "Vojtěch Filips"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "vojtech_filips.dds"
	expire = "2065.1.1"
	ideology = Communist-State
	traits = {
	
	}
}

create_country_leader = {
	name = "Miroslava Nemcova"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "miroslava_nemcova.dds"
	expire = "2060.1.1"
	ideology = Neutral_Libertarian
	traits = {
	
	}
}

create_country_leader = {
	name = "Tomio Okamura"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "tomio_okamura.dds"
	expire = "2065.1.1"
	ideology = Nat_Populism
	traits = {
	
	}
}

create_country_leader = {
	name = "Bohuslav Sobotka"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "CZE_Bohuslav_Sobotka.dds"
	expire = "2065.1.1"
	ideology = socialism
	traits = {
	
	}
}
create_field_marshal = {
	name = "Josef Bečvář"
	picture = "Portrait_Josef_Becvar.dds"
	traits = { organisational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Vlastimil Picek"
	picture = "Portrait_Vlastimil_Picek.dds"
	traits = { old_guard inspirational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Jiří Baloun"
	picture = "Portrait_Jiri_Baloun.dds"
	traits = { thorough_planner }
	skill = 3
}

create_field_marshal = {
	name = "Štefan Kaleta"
	picture = "Portrait_Stefan_Kaleta.dds"
	traits = { offensive_doctrine }
	skill = 2
}

create_corps_commander = {
	name = "Milan Virt"
	picture = "Portrait_Milan_Virt.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Gabriel Kovacs"
	picture = "Portrait_Gabriel_Kovacs.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "František Maleninský"
	picture = "Portrait_Frantisek_Maleninsky.dds"
	traits = { bearer_of_artillery }
	skill = 3
}

create_corps_commander = {
	name = "Petr Mikulenka"
	picture = "Portrait_Petr_Mikulenka.dds"
	traits = { trickster }
	skill = 2
}

create_corps_commander = {
	name = "Štefan Muránský"
	picture = "Portrait_Stefan_Muransky.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Jaroslav Kocián"
	picture = "Portrait_Jaroslav_Kocian.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Ján Gurník"
	picture = "Portrait_Jan_Gurnik.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Ivo Střecha"
	picture = "Portrait_Ivo_Strecha.dds"
	traits = { winter_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Jaromír Šebesta"
	picture = "Portrait_Jaromir_Sebesta.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Petr Hromek"
	picture = "Portrait_Petr_Hromek.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Zuzana Špačková"
	picture = "Portrait_Zuzana_Spackova.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Pavel Kříž"
	picture = "Portrait_Pavel_Kriz.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Vratislav Beran"
	picture = "Portrait_Vratislav_Beran.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Karel Řehka"
	picture = "Portrait_Karel_Rehka.dds"
	traits = { commando urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Jaromír Alan"
	picture = "Portrait_Jaromir_Alan.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Jaromír Zůna"
	picture = "Portrait_Jaromir_Zuna.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Jaroslav Trakal"
	picture = "Portrait_Jaroslav_Trakal.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "František Ridzák"
	picture = "Portrait_Frantisek_Ridzak.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Zoltán Bubeník"
	picture = "Portrait_Zoltan_Bubenik.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Josef Kopecký"
	picture = "Portrait_Josef_Kopecky.dds"
	traits = { ranger hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Roman Náhončík"
	picture = "Portrait_Roman_Nahoncik.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Miroslav Hlaváč"
	picture = "Portrait_Miroslav_Hlavac.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Jiří Adamec"
	picture = "Portrait_Jiri_Adamec.dds"
	traits = { commando ranger }
	skill = 1
}

create_corps_commander = {
	name = "Pavel Lipka"
	picture = "Portrait_Pavel_Lipka.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Robert Bielený"
	picture = "Portrait_Robert_Bieleny.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Josef Havlík"
	picture = "Portrait_Josef_Havlik.dds"
	traits = { trickster }
	skill = 1
}

# Land variants
create_equipment_variant = {
	name = "OT-90"
	type = APC_Equipment_1
	upgrades = {
		tank_reliability_upgrade = 0
		tank_engine_upgrade = 0
		tank_armor_upgrade = 1
		tank_gun_upgrade = 0
	}
	obsolete = yes
}
create_equipment_variant = {
	name = "OT-62"
	type = APC_Equipment_1
	upgrades = {
	}
	obsolete = yes
}
	