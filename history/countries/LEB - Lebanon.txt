﻿capital = 202

# Starting tech

oob = "LEB_2017"

diplomatic_relation = {
            country = HEZ
            relation = military_access
            active = yes
       }

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	
	#For templates
	infantry_weapons = 1
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1

	landing_craft = 1
	
}

set_convoys = 25

add_ideas = {
	pop_050
	unrestrained_corruption
	gdp_5
    pluralist
	LoAS_member
	debt_40
	stable_growth
	bud_neg_08
	defence_06
	edu_01
	health_02
	social_01
	bureau_02
	police_02
	al_jazeera_allowed
	volunteer_army
	volunteer_women
	landowners
	small_medium_business_owners
	iranian_quds_force
	hybrid
}
set_country_flag = bud_neg_08
set_country_flag = debt_40
set_country_flag = gdp_5
set_country_flag = positive_landowners
set_country_flag = positive_small_medium_business_owners

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 33
		}

		fascism = {
			popularity = 14
		}
		
		communism = {
			popularity = 20
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 33
		}
	}
	
	ruling_party = neutrality
	last_election = "2013.6.1" #Will give next elections correct
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Mohammad Raad"
	desc = ""
	picture = "Emerging_HEZ_Mohammad_Raad.dds"
	ideology = Vilayat_e_Faqih
	traits = {
		guerrilla_leader
		emerging_Vilayat_e_Faqih
		pro_iranian
		cautious
		zealous
	}
}

create_country_leader = {
	name = "Ahmed al-Assir"
	desc = ""
	picture = "Salafist_Ahmed_al-Assir.dds"
	ideology = Caliphate
	traits = {
		cleric
		salafist_Caliphate
		zealous
		ruthless
	}
}

create_country_leader = {
	name = "Michel Aoun"
	desc = ""
	picture = "Neutral_Michel_Aoun.dds"
	ideology = Neutral_conservatism
	traits = {
		military_career
		neutrality_Neutral_conservatism
		political_dancer
		cautious
	}
}

create_country_leader = {
	name = "Saad Hariri"
	desc = ""
	picture = "Western_Saad_Hariri.dds"
	ideology = Conservative
	traits = {
		businessman
		western_conservatism
		pro_saudi
		rash
		emotional
		greedy
	}
}
create_field_marshal = {
	name = "Joseph Aoun"
	picture = "generals/Portrait_Joseph_Aoun.dds"
	traits = { offensive_doctrine }
	skill = 4
}

create_field_marshal = {
	name = "Jean Kahwagi"
	picture = "generals/Portrait_Jean_Kahwagi.dds"
	traits = { old_guard inspirational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Ghassan Chahine"
	picture = "generals/Portrait_Ghassan_Chahine.dds"
	traits = { logistics_wizard }
	skill = 4
}

create_corps_commander = {
	name = "Walid Salman"
	picture = "generals/Portrait_Walid_Salman.dds"
	traits = { panzer_leader trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Hatem Mallak"
	picture = "generals/Portrait_Hatem_Mallak.dds"
	traits = { bearer_of_artillery }
	skill = 1
}

create_corps_commander = {
	name = "Chamel Roukoz"
	picture = "generals/Portrait_Chamel_Roukoz.dds"
	traits = { commando naval_invader ranger }
	skill = 1
}

create_corps_commander = {
	name = "Imad Othman"
	picture = "generals/Portrait_Imad_Othman.dds"
	traits = { trickster urban_assault_specialist }
	skill = 2
}

create_navy_leader = {
	name = "Majed Alwan"
	picture = "admirals/Portrait_Majed_Alwan.dds"
	traits = { superior_tactician }
	skill = 2
}