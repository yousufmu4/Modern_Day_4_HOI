﻿capital = 114

oob = "POL_2017"



set_technology = { 
	legacy_doctrines = 1 
	modern_blitzkrieg = 1 
	forward_defense = 1 
	encourage_nco_iniative = 1 
	air_land_battle = 1
	
	corvette_1 = 1
	corvette_2 = 1
	missile_corvette_1 = 1 #Kaszub-Class
	
	frigate_1 = 1
	
	submarine_1 = 1
	diesel_attack_submarine_1 = 1
	diesel_attack_submarine_2 = 1
	diesel_attack_submarine_3 = 1 #Sauro class
	
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1 #SPG-9
	AT_upgrade_1 = 1
	AT_upgrade_2 = 1
	Anti_tank_1 = 1 #RPG-76 Komar
	
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1

	
	Anti_Air_0 = 1
	AA_upgrade_1 = 1
	Anti_Air_1 = 1
	AA_upgrade_3 = 1
	Anti_Air_2 = 1 #Grom
	
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	land_Drone_equipment2 = 3
	
	combat_eng_equipment = 1
	
	Early_APC = 1
	MBT_1 = 1
	MBT_2 = 1
	MBT_3 = 1
	MBT_4 = 1 #Leopard 2A5
	
	Rec_tank_0 = 1 #PL-01
	
	APC_1 = 1
	APC_2 = 1
	APC_3 = 1
	APC_4 = 1
	APC_5 = 1 #Anders IFV
	
	IFV_1 = 1
	IFV_2 = 1
	IFV_3 = 1
	IFV_4 = 1
	IFV_5 = 1 #Anders IFV
	
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	util_vehicle_equipment_3 = 1
	util_vehicle_equipment_4 = 1 #AMZ Dzik
	
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	SP_arty_equipment_1 = 1
	SP_arty_equipment_2 = 1 #AHS Krab
	
	SP_R_arty_equipment_0 = 1
	SP_R_arty_equipment_1 = 1
	SP_R_arty_equipment_2 = 1 #WR-40 Langusta
	
	SP_Anti_Air_0 = 1
	
	early_helicopter = 1
	attack_helicopter1 = 1
	
	transport_helicopter1 = 1
	transport_helicopter2 = 1 #PZL SW-4
	
	ENG_MBT_1 = 1
	#ENG_MBT_2 = 1
	
	early_fighter = 1
	MR_Fighter1 = 1
	Strike_fighter1 = 1
	
	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1
	
	landing_craft = 1
	
}

add_ideas = {
	pop_050
	christian
	medium_corruption
	gdp_6
	
	EU_member
	debt_40
	stable_growth
	bud_neg_04
	defence_02
	edu_03
	health_03
	social_04
	bureau_03
	police_02
	volunteer_army
	volunteer_women
	international_bankers
	The_Clergy
	the_military
	intervention_limited_interventionism
	NATO_member
	western_country
	large_far_right_movement
	civil_law
}
set_country_flag = bud_neg_04
set_country_flag = debt_40
set_country_flag = gdp_6
set_country_flag = Major_Importer_GER_Arms

#NATO military access
diplomatic_relation = {
	country = ALB
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BEL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BUL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CAN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CRO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CZH
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = DEN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = EST
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = FRA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GER
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GRE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HUN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ICE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ITA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LAT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LIT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LUX
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HOL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = NOR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ROM
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLV
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SPR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = TUR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ENG
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = USA
	relation = military_access
	active = yes
}

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot
	complete_national_focus = Generic_30_IC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 80
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 5
		}
		
		neutrality = {
			popularity = 10
		}
		
		Nationalist = {
			popularity = 5
		}
	}
	
	ruling_party = democratic
	last_election = "2015.05.24"
	election_frequency = 48
	elections_allowed = yes
}


set_convoys = 150



create_country_leader = {
	name = "Bronisław Komorowski"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "POL_Bronislaw_Komorowski.dds"
	expire = "2055.1.1"
	ideology = liberalism
	traits = {
	
	}
}

create_country_leader = {
	name = "Magdalena Ogórek"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "POL_Magdalena_Ogorek.dds"
	expire = "2060.1.1"
	ideology = socialism
	traits = {
	
	}
}

create_country_leader = {
	name = "Janusz Korwin-Mikke"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "POL_Janusz_Korwin_Mikke.dds"
	expire = "2060.1.1"
	ideology = Conservative
	traits = {
	
	}
}

create_country_leader = {
	name = "Waldemar Pawlak"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "POL_Waldemar_Pawlak.dds"
	expire = "2065.1.1"
	ideology = neutral_social
	traits = {
	
	}
}

create_country_leader = {
	name = "Andrzej Duda"
	desc = "POLITICS_WLADYSLAW_SIKORSKI_DESC"
	picture = "Andrzej_Duda.dds"
	expire = "2050.1.1"
	ideology = conservatism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Krzysztof Szwej"
	desc = "POLITICS_WLADYSLAW_SIKORSKI_DESC"
	picture = "POL_Krzysztof_Szwej.dds"
	expire = "2050.1.1"
	ideology = Communist-State
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Leszek Surawski"
	picture = "Portrait_Leszek_Surawski.dds"
	traits = { old_guard organisational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Zbigniew Głowienka"
	picture = "Portrait_Zbigniew_Glowienka.dds"
	traits = { fast_planner }
	skill = 3
}

create_corps_commander = {
	name = "Michał Sikora"
	picture = "Portrait_Michal_Sikora.dds"
	traits = {  }
	skill = 3
}

create_corps_commander = {
	name = "Jarosław Mika"
	picture = "Portrait_Jaroslaw_Mika.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Stanisław Czosnek"
	picture = "Portrait_Stanislaw_Czosnek.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Rajmund Andrzejczak"
	picture = "Portrait_Rajmund_Andrzejczak.dds"
	traits = { ranger }
	skill = 2
}

create_corps_commander = {
	name = "Andrzej Malinowski"
	picture = "Portrait_Andrzej_Malinowski.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Marek Sokołowski"
	picture = "Portrait_Marek_Sokolowski.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Grzegorz Hałupka"
	picture = "Portrait_Grzegorz_Halupka.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Ryszard Pietras"
	picture = "Portrait_Ryszard_Pietras.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Stanisław Kaczyński"
	picture = "Portrait_Stanislaw_Kaczynski.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Dariusz Żuchowski"
	picture = "Portrait_Dariusz_Zuchowski.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Sławomir Mąkosa"
	picture = "Portrait_Slawomir_Makosa.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Szymon Lepiarz"
	picture = "Portrait_Szymon_Lepiarz.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Marek Golan"
	picture = "Portrait_Marek_Golan.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Marcin Maj"
	picture = "Portrait_Marcin_Maj.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Jacek Kwiatkowski"
	picture = "Portrait_Jacek_Kwiatkowski.dds"
	traits = { trait_engineer }
	skill = 3
}

create_corps_commander = {
	name = "Bogdan Kula"
	picture = "Portrait_Bogdan_Kula.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Lech Majewski"
	picture = "Portrait_Lech_Majewski.dds"
	traits = {  }
	skill = 3
}

create_corps_commander = {
	name = "Sławomir Kałuziński"
	picture = "Portrait_Slawomir_Kaluzinski.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Sławomir Wojciechowski"
	picture = "Portrait_Slawomir_Wojciechowski.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Piotr Patalong"
	picture = "Portrait_Piotr_Patalong.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Jerzy Gut"
	picture = "Portrait_Jerzy_Gut.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Wiesław Kukuła"
	picture = "Portrait_Wieslaw_Kukula.dds"
	traits = { commando naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Michał Strzelecki"
	picture = "Portrait_Michal_Strzelecki.dds"
	traits = { commando trait_mountaineer }
	skill = 1
}


create_navy_leader = {
	name = "Tomasz Mathea"
	picture = "Portrait_Tomasz_Mathea.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 4
}

create_navy_leader = {
	name = "Ryszard Demczuk"
	picture = "Portrait_Ryszard Demczuk.dds"
	traits = { blockade_runner }
	skill = 3
}

create_navy_leader = {
	name = "Krzysztof Teryfter"
	picture = "Portrait_Krzysztof_Teryfter.dds"
	traits = { seawolf }
	skill = 2
}

create_navy_leader = {
	name = "Krzysztof Jaworski"
	picture = "Portrait_Krzysztof_Jaworski.dds"
	traits = { spotter }
	skill = 1
}

create_navy_leader = {
	name = "Stanisław Zarychta"
	picture = "Portrait_Stanislaw_Zarychta.dds"
	traits = { ironside }
	skill = 3
}

create_navy_leader = {
	name = "Krzysztof Zdonek"
	picture = "Portrait_Krzysztof_Zdonek.dds"
	traits = { fly_swatter }
	skill = 1
}

create_navy_leader = {
	name = "Tadeusz Drybczewski"
	picture = "Portrait_Tadeusz_Drybczewski.dds"
	traits = { air_controller }
	skill = 1
}

create_corps_commander = {
	name = "Leszek Surawski"
		picture = "gen_Leszek_Surawski.dds"
	traits = { }
	skill = 1
}
create_corps_commander = {
	name = "Miroslaw Rozanski"
		picture = "gen_Miroslaw_Rozanski.dds"
	traits = { }
	skill = 3
}
create_corps_commander = {
	name = "Wieslaw Kukula"
		picture = "gen_Wieslaw_Kukula.dds"
	traits = { }
	skill = 2
}
create_corps_commander = {
	name = "Zbigniew Glowienka"
		picture = "gen_Zbigniew_Glowienka.dds"
	traits = { }
	skill = 2
}
create_navy_leader = {
	name = "Tomasz Mathea"
		picture = "adm_Tomasz_Mathea.dds"
	traits = {  }
	skill = 2
}
create_equipment_variant = {
	name = "OT-64"
	type = APC_Equipment_1 #OT-64
	upgrades = {
			
	}
	obsolete = yes
}