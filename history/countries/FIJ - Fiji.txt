﻿capital = 535

oob = "FIJ_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1


	#Basic Rifles
	infantry_weapons = 1
    #Old Radios
	command_control_equipment = 1
	
	#Needed for SPAA template
	Anti_Air_0 = 1
	
	#Needed for HAT and HIW
	Anti_tank_0 = 1

}

add_ideas = {
	pop_050
	systematic_corruption
	gdp_4
	christian
	reserves_20
	fast_growth
	bud_neg_04
	defence_01
	edu_03
	health_02
	social_01
	bureau_04
	police_02
	volunteer_army
	volunteer_women
	small_medium_business_owners
	the_military
	landowners
	common_law
}
set_country_flag = bud_neg_04
set_country_flag = reserves_20
set_country_flag = gdp_4
set_country_flag = negative_small_medium_business_owners
set_country_flag = negative_landowners

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot

add_opinion_modifier = { target = SOL modifier = melanesian_spearhead_group }
add_opinion_modifier = { target = PAP modifier = melanesian_spearhead_group }
add_opinion_modifier = { target = AST modifier = melanesian_diplomacy }


set_convoys = 15

set_politics = {

	parties = {
		democratic = { 
			popularity = 0
		}

		fascism = {
			popularity = 20
		}
		
		communism = {
			popularity = 0
			#banned = no #default is no
		}
		
		neutrality = {
			popularity = 80
		}
	}
	
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Frank Bainimarama"
	desc = "POLITICS_Frank_Bainimarama_DESC"
	picture = "Frank_Bainimarama.dds"
	expire = "2050.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Viliame Nauputo"
	picture = "generals/Portrait_Villiame_Naupoto.dds"
	traits = { organisational_leader }
	skill = 2
}

create_field_marshal = {
	name = "Epeli Nailatikau"
	picture = "generals/Portrait_Epeli_Nailatikau.dds"
	traits = { defensive_doctrine }
	skill = 2
}

create_field_marshal = {
	name = "Frank Bainimarama"
	picture = "generals/Portrait_Frank_Bainimarama.dds"
	traits = { inspirational_leader }
	skill = 2
}

create_field_marshal = {
	name = "Mosese Tikoitoga"
	picture = "generals/Portrait_Mosese_Tikoitoga.dds"
	traits = { offensive_doctrine }
	skill = 1
}

create_corps_commander = {
	name = "Ratu Jone Kalouniwai"
	picture = "generals/Portrait_Jone_Kalouniwai.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Sitiveni Qiliho"
	picture = "generals/Portrait_Sitiveni_Qiliho.dds"
	traits = { bearer_of_artillery }
	skill = 1
}

create_corps_commander = {
	name = "Mohammed Aziz"
	picture = "generals/Portrait_Mohammed_Aziz.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Ben Groenewald"
	picture = "generals/Portrait_Ben_Groenewald.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Pita Driti"
	picture = "generals/Portrait_Pita_Driti.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Manoa Gadai"
	picture = "generals/Portrait_Manoa_Gadai.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Petero Talemaivatuiri"
	picture = "generals/Portrait_Petero_Talemaivatuiri.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Ratu Tevita Mara"
	picture = "generals/Portrait_Ratu_Tevita_Mara.dds"
	traits = { swamp_fox }
	skill = 1
}

create_navy_leader = {
	name = "Viliame Nauputo"
	picture = "admirals/Portrait_Villiame_Naupoto.dds"
	traits = { blockade_runner }
	skill = 2
}

create_navy_leader = {
	name = "Epeli Nailatikau"
	picture = "admirals/Portrait_Epeli_Nailatikau.dds"
	traits = {  }
	skill = 2
}

create_navy_leader = {
	name = "Frank Bainimarama"
	picture = "admirals/Portrait_Frank_Bainimarama.dds"
	traits = {  }
	skill = 2
}