﻿division_template = {
	name = "Infantry Regiment"
	
	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
	}
	
	support = {
		L_Engi_Comp = { x = 0 y = 0 }	
		armor_Recce_Comp = { x = 0 y = 1 }
	}
	
}

units = {
	
	division= {	
		name = "1st Battalion, The Jamaica Regiment"
		location = 12304
		division_template = "Infantry Regiment"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division= {	
		name = "2nd Battalion, The Jamaica Regiment"
		location = 12477
		division_template = "Infantry Regiment"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division= {	
		name = "3rd Battalion, The Jamaica Regiment"
		location = 12304
		division_template = "Infantry Regiment"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
}


instant_effect = {

	add_equipment_to_stockpile = {
		type = Rec_tank_Equipment_0		#Cadillac Cage Commando
		#version_name = "Cadillac Cage Commando"
		amount = 7
		producer = USA
    }

	add_equipment_to_stockpile = {
		type = command_control_equipment_2	
		amount = 200
		producer = USA
    }

	add_equipment_to_stockpile = {
		type = Inf_equipment_0			#M16	
		amount = 450
		producer = USA
    }

	add_equipment_to_stockpile = {
		type = Inf_equipment_2			#L85	
		amount = 450
		producer = ENG
    }
 }
