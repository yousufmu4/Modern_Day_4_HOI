﻿division_template = {
	name = "Guarda National"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Engi_Bat = { x = 0 y = 2 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
	}
}

units = {

	division = {			
		name = "Guarda National"
		location = 13014		#
		division_template = "Guarda National"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}

}

instant_effect = {
	
	add_equipment_to_stockpile = {
		type = Inf_equipment_0 		#AKM
		amount = 200
		producer = SOV
    }
	
	add_equipment_to_stockpile = {
		type = Inf_equipment_0 		#G3
		amount = 200
		producer = GER
    }
	
	add_equipment_to_stockpile = {
		type = Inf_equipment_0 		#M16
		amount = 200
		producer = USA
    }
	
	add_equipment_to_stockpile = {
		type = command_control_equipment_0 
		amount = 100
		producer = GER
    }
	
	add_equipment_to_stockpile = {
		type = AA_Equipment_0 
		amount = 50
		producer = SOV
    }
	
	add_equipment_to_stockpile = {
		type = Rec_tank_Equipment_0 		#BRDM-2
		#version_name = "BRDM-2"
		amount = 10
		producer = SOV
    }
	
}
	