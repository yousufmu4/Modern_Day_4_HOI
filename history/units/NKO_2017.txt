﻿division_template = {
	name = "First Elechon Infantry Division" #extra light tanks and engineers

	regiments = {						
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
		L_Inf_Bat = { x = 0 y = 4 }
		L_Inf_Bat = { x = 1 y = 0 }
		L_Inf_Bat = { x = 1 y = 1 }
		L_Inf_Bat = { x = 1 y = 2 }
		L_Inf_Bat = { x = 1 y = 3 }
		L_Inf_Bat = { x = 1 y = 4 }
		L_Inf_Bat = { x = 2 y = 0 } 
		L_Inf_Bat = { x = 2 y = 1 }
		L_Recce_Bat = { x = 2 y = 2 }
		armor_Recce_Bat = { x = 2 y = 3 }
		Arty_Bat = { x = 3 y = 0 }
		Arty_Bat = { x = 3 y = 1 }
		Arty_Bat = { x = 3 y = 2 }
		Arty_Bat = { x = 3 y = 3 }
		SP_AA_Bat = { x = 3 y = 4 }
		armor_Bat = { x = 4 y = 0 }
		L_Engi_Bat = { x = 4 y = 1 }
		L_Engi_Bat = { x = 4 y = 2 }
	}
}

division_template = {
	name = "Infantry Division" #standard

	regiments = {						
		L_Inf_Bat = { x = 0 y = 0 } 
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
		L_Inf_Bat = { x = 0 y = 4 }
		L_Inf_Bat = { x = 1 y = 0 } 
		L_Inf_Bat = { x = 1 y = 1 }
		L_Inf_Bat = { x = 1 y = 2 }
		L_Inf_Bat = { x = 1 y = 3 }
		L_Inf_Bat = { x = 1 y = 4 }
		L_Inf_Bat = { x = 2 y = 0 } 
		L_Inf_Bat = { x = 2 y = 1 }
		L_Recce_Bat = { x = 2 y = 2 }
		L_Engi_Bat = { x = 2 y = 3 }
		Arty_Bat = { x = 3 y = 0 } 
		Arty_Bat = { x = 3 y = 1 }
		Arty_Bat = { x = 3 y = 2 }
		Arty_Bat = { x = 3 y = 3 }
		SP_AA_Bat = { x = 3 y = 4 }
		armor_Bat = { x = 4 y = 0 }
	}
    priority = 0
}

division_template = {
	name = "Motor Infantry Division"

	regiments = {						
		Mot_Inf_Bat = { x = 0 y = 0 } 
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Mot_Inf_Bat = { x = 0 y = 3 }
		Mot_Inf_Bat = { x = 0 y = 4 }
		Mot_Inf_Bat = { x = 1 y = 0 } 
		Mot_Inf_Bat = { x = 1 y = 1 }
		Mot_Inf_Bat = { x = 1 y = 2 }
		Mot_Inf_Bat = { x = 1 y = 3 }
		Mot_Inf_Bat = { x = 1 y = 4 }
		Mot_Inf_Bat = { x = 2 y = 0 } 
		Mot_Inf_Bat = { x = 2 y = 1 }
		Mot_Recce_Bat = { x = 2 y = 2 }
		L_Engi_Bat = { x = 2 y = 3 }
		Arty_Bat = { x = 3 y = 0 } 
		Arty_Bat = { x = 3 y = 1 }
		Arty_Bat = { x = 3 y = 2 }
		Arty_Bat = { x = 3 y = 3 }
		SP_AA_Bat = { x = 3 y = 4 }
		armor_Bat = { x = 4 y = 0 }
	}
}

division_template = {
	name = "Mechanized Brigade" #mostly motorized

	regiments = {						
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Mot_Inf_Bat = { x = 0 y = 3 }
		armor_Bat = { x = 1 y = 0 }
		Arty_Bat = { x = 2 y = 0 }
		Arty_Bat = { x = 2 y = 1 }
		SP_Arty_Bat = { x = 2 y = 2 }
	}
	support = {
	    SP_AA_Battery = { x = 0 y = 0 }
		Mot_Recce_Comp = { x = 0 y = 1 }
		L_Engi_Comp = { x = 0 y = 2 }
		}
}

division_template = {
	name = "Armor Brigade"

	regiments = {						
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		armor_Recce_Bat = { x = 1 y = 1 }
		SP_Arty_Bat = { x = 1 y = 2 }
		
	}
	support = {
	H_Engi_Comp = { x = 0 y = 0 }
	SP_AA_Battery = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Armor Division" #105th Arm Div

	regiments = {						
		armor_Bat = { x = 0 y = 0 } 
		armor_Bat = { x = 0 y = 1 } 
		armor_Bat = { x = 0 y = 2 }
		armor_Recce_Bat = { x = 0 y = 3 }
		armor_Bat = { x = 1 y = 0 } 
		armor_Bat = { x = 1 y = 1 } 
		armor_Bat = { x = 1 y = 2 }
		armor_Recce_Bat = { x = 1 y = 3 }
		Arm_Inf_Bat = { x = 2 y = 0 }
		Arm_Inf_Bat = { x = 2 y = 1 }
		Arm_Inf_Bat = { x = 2 y = 2 }
		Arm_Inf_Bat = { x = 2 y = 3 }
		Arm_Inf_Bat = { x = 2 y = 4 }
		SP_Arty_Bat = { x = 3 y = 0 }
		SP_Arty_Bat = { x = 3 y = 1 }
		SP_Arty_Bat = { x = 3 y = 2 }
		SP_Arty_Bat = { x = 3 y = 3 }
		SP_Arty_Bat = { x = 3 y = 4 }
		SP_AA_Bat = { x = 4 y = 0 }
		H_Engi_Bat = { x = 4 y = 1 }
		Arty_Bat = { x = 4 y = 2 }
		Arty_Bat = { x = 4 y = 3 }
		Arty_Bat = { x = 4 y = 4 }
	}
	priority = 2
}

division_template = {
	name = "Amphibious Sniper Brigade" #Light Marine infantry

	regiments = {						
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
		L_Marine_Bat = { x = 0 y = 3 }
		L_Marine_Bat = { x = 0 y = 4 }
		L_Recce_Bat = { x = 1 y = 0 }
		L_Engi_Bat = { x = 1 y = 1 }
	}
	priority = 2
}

division_template = {
	name = "Airborne Sniper Brigade" #Light Airborne infantry

	regiments = {						
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
		L_Air_Inf_Bat = { x = 0 y = 3 }
		L_Air_Inf_Bat = { x = 0 y = 4 }
		L_Recce_Bat = { x = 1 y = 0 }
		L_Engi_Bat = { x = 1 y = 1 }
	}
	priority = 2
}

division_template = {
	name = "Sniper Brigade" #Spec ops inf

	regiments = {						
		Special_Forces = { x = 0 y = 0 } 
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
		Special_Forces = { x = 1 y = 0 }
		Special_Forces = { x = 1 y = 1 }
		Special_Forces = { x = 1 y = 2 }
		L_Recce_Bat = { x = 2 y = 0 }
		L_Engi_Bat = { x = 2 y = 1 }
	}
	priority = 2
}

division_template = {
	name = "Light Infantry Brigade" #Spec ops inf

	regiments = {						
		Special_Forces = { x = 0 y = 0 } 
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
		L_Inf_Bat = { x = 1 y = 0 }
		L_Inf_Bat = { x = 1 y = 1 }
		L_Inf_Bat = { x = 1 y = 2 }
		L_Recce_Bat = { x = 2 y = 0 }
		L_Engi_Bat = { x = 2 y = 1 }
	}
	priority = 2
}

division_template = {
	name = "Airborne Brigade" #Light Airborne infantry

	regiments = {						
		L_Air_Inf_Bat = { x = 0 y = 0 } 
		L_Air_Inf_Bat = { x = 0 y = 1 }
		L_Air_Inf_Bat = { x = 0 y = 2 }
		L_Air_Inf_Bat = { x = 0 y = 3 }
		L_Air_Inf_Bat = { x = 0 y = 4 }
		L_Recce_Bat = { x = 1 y = 0 }
		L_Engi_Bat = { x = 1 y = 1 }
	}
	priority = 2
}

division_template = {
	name = "Artillery Brigade"

	regiments = {						
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 } 
		L_Recce_Bat = { x = 0 y = 2 }
		Arty_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 1 y = 1 }
		SP_Arty_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 1 y = 3 }
		SP_Arty_Bat = { x = 1 y = 4 }
	}
}

units = {

############## OOB ############
# NOTES:
# About 1/2 of names are made up, but the units themselves are accurate.

# Does not contain PMTU-Infantry Divisions (Active reserves):
# These have 100% strength of small arms, and 80% of heavy equipment
# Manned by college students & ex-active duty soldiers
# Their levels of training make them ok to include later if we want

#v.02 - missing some specialized units, equipment incomplete

###############################
##### First Elechon Units #####
###############################

###5th Army Corps

	division = {			
		name = "2nd Artillery Brigade"
		location = 3187
		division_template = "Artillery Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "75th Light Infantry Brigade"
		location = 3187
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "80th Light Infantry Brigade"
		location = 3187
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "103rd Armor Brigade"
		location = 3187
		division_template = "Armor Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "45th Infantry Division"
		location = 3187
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "5th Infantry Division"
		location = 3187
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "12th Infantry Division"
		location = 3187
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "25th Infantry Division"
		location = 3187
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}

###4th Army Corps

	division = {			
		name = "1st Artillery Brigade"
		location = 9981
		division_template = "Artillery Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "20th Light Infantry Brigade"
		location = 9981
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "19th Light Infantry Brigade"
		location = 9981
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "35th Armor Brigade"
		location = 9981
		division_template = "Armor Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "28th Infantry Division"
		location = 9981
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "33rd Infantry Division"
		location = 9981
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "41st Infantry Division"
		location = 9981
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "26th Infantry Division"
		location = 9981
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
###2nd Army Corps

	division = {			
		name = "7th Artillery Brigade"
		location = 3173
		division_template = "Artillery Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "32nd Light Infantry Brigade"
		location = 3173
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "2nd Light Infantry Brigade"
		location = 3173
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "10th Light Infantry Brigade"
		location = 3173
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "107th Armor Brigade"
		location = 3173
		division_template = "Armor Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "3rd Infantry Division"
		location = 3173
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "6th Infantry Division"
		location = 3173
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "8th Infantry Division"
		location = 3173
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}

	###1st Army Corps

	division = {			
		name = "23rd Artillery Brigade"
		location = 12040
		division_template = "Artillery Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "51st Light Infantry Brigade"
		location = 12040
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "28th Armor Brigade"
		location = 12040
		division_template = "Armor Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "2nd Infantry Division"
		location = 12040
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "13th Infantry Division"
		location = 12040
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "31st Infantry Division"
		location = 12040
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "46th Infantry Division"
		location = 12040
		division_template = "First Elechon Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}

###############################
#### Second Elechon Units #####
###############################

    ###806 Mechanized Corps 
    division = {			
		name = "7th Mechanized Brigade"
		location = 10083
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "4th Mechanized Brigade"
		location = 10083
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "47th Mechanized Brigade"
		location = 10083
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "56th Mechanized Brigade"
		location = 10083
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "29th Mechanized Brigade"
		location = 10083
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}

    ###815 Mechanized Corps
	#One extra mech bde?
	division = {			
		name = "27th Mechanized Brigade"
		location = 1003
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "45th Mechanized Brigade"
		location = 1003
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "41st Mechanized Brigade"
		location = 1003
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "66th Mechanized Brigade"
		location = 1003
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
	###820 Armor Corps
	division = {			
		name = "88th Artillery Brigade"
		location = 3176
		division_template = "Artillery Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "53rd Armor Brigade"
		location = 3176
		division_template = "Armor Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "3rd Armor Brigade"
		location = 3176
		division_template = "Armor Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "32nd Armor Brigade"
		location = 3176
		division_template = "Armor Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "15th Mechanized Brigade"
		location = 3176
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "105th Armor Division"
		location = 3176
		division_template = "Armor Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

###############################
##### Third Elechon Units ##### (need locations)
###############################

    ### 3rd Army Corps
    division = {			
		name = "UI Armor Brigade"
		location = 4052
		division_template = "Armor Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Infantry Division"
		location = 4052
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Infantry Division"
		location = 4052
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Infantry Division"
		location = 4052
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
	### 7th Army Corps
    division = {			
		name = "UI Armor Brigade"
		location = 3981
		division_template = "Armor Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "87th Light Infantry Brigade"
		location = 3981
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "10th Motor Infantry Division"
		location = 3981
		division_template = "Motor Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "20th Motor Infantry Division"
		location = 3981
		division_template = "Motor Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
	### 12th Army Corps
    division = {			
		name = "UI Armor Brigade"
		location = 9790
		division_template = "Armor Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "59th Motor Infantry Division"
		location = 9790
		division_template = "Motor Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "68th Infantry Division"
		location = 9790
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
	### U/I Army Corps, Hamgyong
	division = {			
		name = "16th Infantry Division"
		location = 6928
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
	### Pyongyang Defense Command 4052
	# 1 L inf rgt, 4 mot bde, 1 tank rgt, 2 art bde
	# Represented as 3 inf divs
	division = {			
		name = "103rd Infantry Division"
		location = 4052
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "106th Infantry Division"
		location = 4052
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Infantry Division"
		location = 4052
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
	### Kangdong Artillery Corps (DMZ)
	division = {			
		name = "UI Artillery Brigade"
		location = 3173
		division_template = "Artillery Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Artillery Brigade"
		location = 3173
		division_template = "Artillery Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Artillery Brigade"
		location = 3173
		division_template = "Artillery Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Artillery Brigade"
		location = 1003
		division_template = "Artillery Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Artillery Brigade"
		location = 1003
		division_template = "Artillery Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Artillery Brigade"
		location = 1003
		division_template = "Artillery Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
###############################
##### Fourth Elechon Units ####
###############################

    ### 8th Army Corps
    division = {			
		name = "UI Armor Brigade"
		location = 912
		division_template = "Armor Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Motor Infantry Division"
		location = 912
		division_template = "Motor Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Motor Infantry Division"
		location = 912
		division_template = "Motor Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
	### 9th Army Corps
	division = {			
		name = "24th Infantry Division"
		location = 6822
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "42nd Infantry Division"
		location = 6822
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
	### 10th Army Corps
	division = {			
		name = "UI Infantry Division"
		location = 3191
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
	### 11th Army Corps
	division = {			
		name = "UI Infantry Division"
		location = 6963
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
	### 108th Mechanized Corps, Hamhung
	division = {			
		name = "UI Mechanized Brigade"
		location = 6928
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Mechanized Brigade"
		location = 6928
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Mechanized Brigade"
		location = 6928
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Mechanized Brigade"
		location = 6928
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Mechanized Brigade"
		location = 6928
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
	### 425th Mechanized Corps, Chongju & Yongbyon
	division = {			
		name = "UI Mechanized Brigade"
		location = 9795
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Mechanized Brigade"
		location = 9795
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Mechanized Brigade"
		location = 9795
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Mechanized Brigade"
		location = 9795
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {			
		name = "UI Mechanized Brigade"
		location = 9795
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
######################################
### Special Purpose Forces Command ###
######################################
#Missing:
# 8 spec ops bn

    ### Naval Infantry
	division = {			
		name = "1st Amph. Sniper Brigade"
		location = 3189
		division_template = "Amphibious Sniper Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "12th Amph. Sniper Brigade"
		location = 3189
		division_template = "Amphibious Sniper Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	
	### Airborne units  
	division = {			
		name = "13th Airborne Brigade"
		location = 3189
		division_template = "Airborne Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {			
		name = "17th Airborne Brigade"
		location = 3189
		division_template = "Airborne Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {			
		name = "22nd Airborne Brigade"
		location = 3189
		division_template = "Airborne Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {			
		name = "19th Airborne Sniper Brigade"
		location = 3189
		division_template = "Airborne Sniper Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {			
		name = "6th Airborne Sniper Brigade"
		location = 3189
		division_template = "Airborne Sniper Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	
	### Special Recon Force
	division = {			
		name = "4th Sniper Brigade"
		location = 848
		division_template = "Sniper Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "11th Sniper Brigade"
		location = 848
		division_template = "Sniper Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "2nd Sniper Brigade"
		location = 848
		division_template = "Sniper Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "7th Sniper Brigade"
		location = 848
		division_template = "Sniper Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "3rd Sniper Brigade"
		location = 848
		division_template = "Sniper Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {			
		name = "18th Sniper Brigade"
		location = 848
		division_template = "Sniper Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	
	navy = {
        name = "KPN Surface Fleet"
        base = 3189
		location = 3189
		ship = { name = "Najin" definition = frigate equipment = { frigate_1 = { amount = 1 owner = NKO } } }
		ship = { name = "Soho" definition = frigate equipment = { frigate_1 = { amount = 1 owner = NKO } } }
		ship = { name = "Pyongyang" definition = frigate equipment = { frigate_1 = { amount = 1 owner = NKO } } }	
	}
	
	navy = {
        name = "1st East Sea Flotilla"
        base = 848
		location = 848
	    ship = { name = "SS-01" definition = diesel_attack_submarine experience = 20 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-03" definition = diesel_attack_submarine experience = 20 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-05" definition = diesel_attack_submarine experience = 20 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-07" definition = diesel_attack_submarine experience = 20 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-09" definition = diesel_attack_submarine experience = 20 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-11" definition = diesel_attack_submarine experience = 20 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-13" definition = diesel_attack_submarine experience = 20 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-15" definition = diesel_attack_submarine experience = 20 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-17" definition = diesel_attack_submarine experience = 20 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-19" definition = diesel_attack_submarine experience = 20 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
        }
		
		navy = {
        name = "2nd East Sea Flotilla"
        base = 6944
		location = 6944
	    ship = { name = "SS-02" definition = diesel_attack_submarine equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-04" definition = diesel_attack_submarine equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-06" definition = diesel_attack_submarine equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-08" definition = diesel_attack_submarine equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-10" definition = diesel_attack_submarine equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-12" definition = diesel_attack_submarine equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-14" definition = diesel_attack_submarine equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-16" definition = diesel_attack_submarine equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-18" definition = diesel_attack_submarine equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
		ship = { name = "SS-20" definition = diesel_attack_submarine equipment = { diesel_attack_submarine_1 = { amount = 1 owner = NKO } } }
        }


}


instant_effect = {

   ### Aircraft
	add_equipment_to_stockpile = {
    type = MR_Fighter_equipment_1 #MiG-21
    amount = 78
	producer = SOV
    }
	
	add_equipment_to_stockpile = {
    type = MR_Fighter_equipment_1 #J-7
    amount = 72
	producer = CHI
    }
	
	add_equipment_to_stockpile = {
    type = MR_Fighter_equipment_2 #MiG-29
    amount = 18
	producer = SOV
    }
	
	add_equipment_to_stockpile = {
    type = AS_Fighter_equipment_1 #MiG-23
    amount = 56
	producer = SOV
    }
	
	add_equipment_to_stockpile = {
    type = Strike_fighter_equipment_1 #Su-7
    amount = 18
	producer = SOV
    }
	add_equipment_to_stockpile = {
		type = CAS_equipment_1 #Su-25 Frogfoot-A
		amount = 34
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = attack_helicopter_equipment_2 #Hughes 500D
		amount = 80
		producer = USA
		#version_name = "Hughes 500D"
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_2 #Mil Mi-26 Halo
		amount = 4
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_1 #Mil Mi-8
		amount = 15
		producer = SOV
    }
	
	### Tanks
	
	add_equipment_to_stockpile = {
    type = MBT_Equipment_1 #T-55
    amount = 1600
	producer = SOV
    }
	
	add_equipment_to_stockpile = {
    type = MBT_Equipment_1 #Type-59
    amount = 175
	producer = CHI
    }
	
	add_equipment_to_stockpile = {
    type = MBT_Equipment_2 #Ch'onma-ho
    amount = 1600
    }
	
	add_equipment_to_stockpile = {
    type = MBT_Equipment_3 #P'okpoong-ho
    amount = 500
    }
	
	### IFV
	
	add_equipment_to_stockpile = {
    type = IFV_Equipment_1 #BMP-1
    amount = 200
	producer = SOV
    }
	
	### Light Tanks
	
	add_equipment_to_stockpile = {
    type = Rec_tank_Equipment_0 #Type 62
    amount = 100
	producer = CHI
    }
	
	add_equipment_to_stockpile = {
    type = Rec_tank_Equipment_0 #PT-76
    amount = 400
	producer = SOV
    }
	
	add_equipment_to_stockpile = {
    type = Rec_tank_Equipment_1 #PT-85
    amount = 360
    }
	
	### APC's
	
	add_equipment_to_stockpile = {
    type = APC_Equipment_1 #VTT-323
    amount = 2800
    }
	
	add_equipment_to_stockpile = {
    type = APC_Equipment_1 #Type-63
    amount = 400
	producer = CHI
    }
	
	add_equipment_to_stockpile = {
    type = APC_Equipment_1 #BTR-60
    amount = 1000
	producer = SOV
    }
	
	add_equipment_to_stockpile = {
    type = APC_Equipment_2 #M-1992
    amount = 1300
    }
	
	add_equipment_to_stockpile = {
    type = APC_Equipment_4 #BTR-80A
    amount = 32
	producer = SOV
    }
	
	add_equipment_to_stockpile = {
    type = APC_Equipment_4 #Chunma-D
    amount = 100
    }
	
	### Trucks
	
	add_equipment_to_stockpile = {
    type = util_vehicle_equipment_1 #ZIL-131
    amount = 6000
	producer = SOV
    }
	
	add_equipment_to_stockpile = {
    type = util_vehicle_equipment_2 #Ural-4320
    amount = 1500
    }
	
	add_equipment_to_stockpile = {
    type = util_vehicle_equipment_2 #UAZ-3151
    amount = 400
	producer = SOV
    }
	
	### AA, ART, AGTM ###
	#SP-AA
	
	add_equipment_to_stockpile = {
    type = SP_AA_Equipment_0 #not exact 
    amount = 450
	producer = SOV
    }
	
	#MANPADS
	
	add_equipment_to_stockpile = {
    type = AA_Equipment_0 #SA-7 Strela-2
    amount = 3000
	producer = SOV
    }
	
	add_equipment_to_stockpile = {
    type = AA_Equipment_1 #SA-16 Igla
    amount = 300
	producer = SOV
    }
	
	#ATGM
	
	add_equipment_to_stockpile = {
    type = L_AT_Equipment_0 #Susong-po
    amount = 2750
    }
	
	add_equipment_to_stockpile = {
    type = L_AT_Equipment_1 #AT-4 Fagot
    amount = 400
	producer = SOV
    }
	
	#mounted
	
	add_equipment_to_stockpile = {
    type = H_AT_Equipment_0 #AT-3 Malyutka
    amount = 1800
	producer = SOV
    }
	
	add_equipment_to_stockpile = {
    type = H_AT_Equipment_1 #AT-5 Konkurs
    amount = 300
	producer = SOV
    }
	
	### MLRS ###
	
	add_equipment_to_stockpile = {
    type = SP_R_arty_equipment_0 #M1977
    amount = 1850
    }
	
	add_equipment_to_stockpile = {
    type = SP_R_arty_equipment_1 #M1985
    amount = 875
    }

	#Artillery
	
	add_equipment_to_stockpile = {
		type = artillery_equipment_0
		amount = 4000 #as many as needed
    }
	
	add_equipment_to_stockpile = {
    type = SP_arty_equipment_0 #M-1978
    amount = 2500
    }
	
	add_equipment_to_stockpile = {
    type = SP_arty_equipment_1 #M-1989
    amount = 1000
    }
	
	
	#Small Arms
	add_equipment_to_stockpile = {
    type = Inf_equipment_0 #1965
    amount = 122850
    }
	
	#Heavy
	
		
	#C&C
	
	add_equipment_to_stockpile = {
    type = command_control_equipment_0 #C2
    amount = 10000 #a lot, but not enough for all units. 
    }
	
	#Engineer
	
		
	add_equipment_to_stockpile = {
    type = ENGI_MBT_Equipment_1 #Type-653... No idea what they actually use.
    amount = 400
	producer = CHI
    }
	
	### Drones
	
    #just joking. 
	
}
