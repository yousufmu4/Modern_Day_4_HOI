﻿#
division_template = {
	name = "Tuareg Tribesmen"
	
	regiments = {
		Militia_Bat = { x = 0 y = 0 }
		Militia_Bat = { x = 0 y = 1 }
	}
	
	support = {
	}
	
}

units = {
	
	#No real numbers exist, estimated as "thousands"
	division= {	
		name = "Tuareg Tribesmen"
		location = 9134 		#
		division_template = "Tuareg Tribesmen"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division= {	
		name = "Tuareg Tribesmen"
		location = 13420 		#
		division_template = "Tuareg Tribesmen"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division= {	
		name = "Tuareg Tribesmen"
		location = 13418 		#
		division_template = "Tuareg Tribesmen"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division= {	
		name = "Tuareg Tribesmen"
		location = 9110 		#
		division_template = "Tuareg Tribesmen"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division= {	
		name = "Tuareg Tribesmen"
		location = 5004 		#
		division_template = "Tuareg Tribesmen"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

}

instant_effect = {
	
	add_equipment_to_stockpile = {
		type = Inf_equipment_0	
		amount = 2500
		producer = SOV 
	}
	
}