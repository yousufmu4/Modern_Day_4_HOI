﻿division_template = {
	name = "Armoured Cavalry Brigade"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		SP_AA_Bat = { x = 0 y = 4 }
		Mech_Recce_Bat = { x = 1 y = 0 }
		H_Engi_Bat = { x = 1 y = 1 }
		
		
	}
	support = {
	}
}
division_template = {
	name = "Mechanized Brigade"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		SP_AA_Bat = { x = 0 y = 4 }
		Mech_Recce_Bat = { x = 1 y = 0 }
		L_Engi_Bat = { x = 2 y = 0 }
		
	}
	support = {
	}
}
division_template = {
	name = "Legion Brigade"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		SP_AA_Bat = { x = 0 y = 4 }
		Mech_Recce_Bat = { x = 1 y = 0 }
		H_Engi_Bat = { x = 1 y = 1 }
		
	}
	support = {
	}
}
division_template = {
	name = "Coastal Brigade"

	regiments = {
		Arm_Marine_Bat = { x = 0 y = 0 }
		Arm_Marine_Bat = { x = 0 y = 1 }
		Arm_Marine_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		SP_AA_Bat = { x = 0 y = 4 }
		Mech_Recce_Bat = { x = 1 y = 0 }
		H_Engi_Bat = { x = 1 y = 1 }
		
	}
	support = {
	}
}
division_template = {
	name = "Airborne Brigade"

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
		L_Air_Inf_Bat = { x = 0 y = 2 }
		
	}
	support = {
	}
}
division_template = {
	name = "Riffle Brigade"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 3 }
		SP_Arty_Bat = { x = 0 y = 4 }
		SP_AA_Bat = { x = 1 y = 0 }
		L_Engi_Bat = { x = 2 y = 0 }
		
	}
	support = {
	}
}

units = {
	
	#frigate_2 = Oliver Hazard Perry-Class
	#missile_corvette_1 = Kaszub-Class
	#diesel_attack_submarine_1 = Kobben class
	#diesel_attack_submarine_3 = Kilo class
	
	navy = {
		name = "3rd Ship Flotilla"				
		base = 362
		location = 362
		ship = { name = "ORP Generał Kazimierz Pułaski" definition = frigate experience = 75 equipment = { frigate_2 = { amount = 1 owner = POL creator = USA } } }
		ship = { name = "ORP Generał Tadeusz Kościuszko" definition = frigate experience = 75 equipment = { frigate_2 = { amount = 1 owner = POL creator = USA  } } }
		ship = { name = "ORP Kaszub" definition = corvette experience = 75 equipment = { missile_corvette_1 = { amount = 1 owner = POL } } }
		ship = { name = "ORP Orzeł" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = POL creator = NOR } } }
		ship = { name = "ORP Sokół" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = POL creator = SOV } } }
		ship = { name = "ORP Sęp" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = POL creator = SOV } } }
		ship = { name = "ORP Bielik" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = POL creator = SOV } } }
		ship = { name = "ORP Kondor" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = POL creator = SOV } } }
	}
	
	
	
	#11th Armoured Cavalry Division
	division = {			
		name = "10th Armoured Cavalry Brigade"
		location = 3438
		division_template = "Armoured Cavalry Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {			
		name = "17th Mechanised Brigade"
		location = 9387
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {			
		name = "34th Armoured Cavalry Brigade"
		location = 9470
		division_template = "Armoured Cavalry Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	#12th Mechanised Division
	division = {			
		name = "2nd Legion Mechanised Brigade"
		location = 9252
		division_template = "Legion Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {			
		name = "7th Coastal Defense Brigade"
		location = 11372
		division_template = "Coastal Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	#16th Mechanised Division
	division = {			
		name = "12th Mechanised Brigade"
		location = 6282
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {			
		name = "1st Armoured Brigade"
		location = 3544
		division_template = "Legion Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {			
		name = "9th Armoured Cavalry Brigade"
		location = 6402
		division_template = "Armoured Cavalry Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "15th Mechanised Brigade"
		location = 266
		division_template = "Legion Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {			
		name = "20th Mechanised Brigade"
		location = 11245
		division_template = "Legion Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	#6th Airborne Brigade
	division = {			
		name = "6th Airborne Brigade"
		location = 9427
		division_template = "Airborne Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	#21st Podhale Rifles Brigade
	division = {			
		name = "21st Podhale Rifles Brigade"
		location = 9494
		division_template = "Riffle Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	#25th Air Cavalry Brigade
	division = {			
		name = "25th Air Cavalry Brigade"
		location = 467
		division_template = "Airborne Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	
	add_equipment_to_stockpile = {
		type = Inf_equipment_3 #Kbz Wz
		amount = 7200
    }
		add_equipment_to_stockpile = {
		type = H_AT_Equipment_0 #SPG-9
		amount = 65
    }
	add_equipment_to_stockpile = {
		type = L_AT_Equipment_1 #RPG-76 Komar
		amount = 315
    }
	add_equipment_to_stockpile = {
		type = command_control_equipment_2 #C4ISTAR
		amount = 1030
    }
	add_equipment_to_stockpile = {
		type = AA_Equipment_2 #Grom
		amount = 330
    }
			
	add_equipment_to_stockpile = {
		type = MBT_Equipment_4 #Leopard 2A4
		amount = 142
		producer = GER
    }
	add_equipment_to_stockpile = {
		type = MBT_Equipment_4 #Leopard 2A5
		amount = 91
		producer = GER
		#version_name = "Leopard 2A5"
    }
	add_equipment_to_stockpile = {
		type = MBT_Equipment_4 #PT-91
		amount = 233
    }
	add_equipment_to_stockpile = {
		type = MBT_Equipment_2 #T-72
		amount = 505
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = APC_Equipment_1 #BDRM-2
		amount = 396 #Added 159 for balance
    }
	add_equipment_to_stockpile = {
		type = APC_Equipment_1 #BWR (BMP-1)
		amount = 37
    }
	add_equipment_to_stockpile = {
		type = APC_Equipment_1 #WD R-5 (BDRM-2)
		amount = 92
    }
	add_equipment_to_stockpile = {
		type = IFV_Equipment_1 #BMP1
		amount = 1118
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = IFV_Equipment_1 #BMP1
		amount = 150 #Removed 110 from ifv_equipment for balance
    }
	add_equipment_to_stockpile = {
		type = IFV_Equipment_4 #KTO Rosomak
		amount = 570
    }
	add_equipment_to_stockpile = {
		type = util_vehicle_equipment_4 #M-ATV
		amount = 45
		producer = USA
    }
	add_equipment_to_stockpile = {
		type = util_vehicle_equipment_5 #MaxxPro MRAP
		amount = 30
		producer = USA
    }
	add_equipment_to_stockpile = {
		type = SP_arty_equipment_0 #2S1 Gvozdika
		amount = 292
    }
	add_equipment_to_stockpile = {
		type = SP_arty_equipment_0 #M-77 Dana
		amount = 111
		producer = CZH
    }
	add_equipment_to_stockpile = {
		type = SP_R_arty_equipment_0 #BM-21 Grad
		amount = 75
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = SP_R_arty_equipment_1 #RM-70
		amount = 30
    }
	add_equipment_to_stockpile = {
		type = SP_R_arty_equipment_2 #WR-40 Langusta
		amount = 75
    }
	add_equipment_to_stockpile = {
		type = SP_AA_Equipment_0 #ZSU-23
		amount = 208 #added +180 for balance
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = ENGI_MBT_Equipment_2 #Leopard 1 variants
		amount = 90 #added 80 more for balance
		producer = GER
    }
	add_equipment_to_stockpile = {
		type = ENGI_MBT_Equipment_1 #MT-LB
		amount = 15
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = ENGI_MBT_Equipment_1 #WZT-3
		amount = 40
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = MR_Fighter_equipment_2 #Mig 29
		amount = 32
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = MR_Fighter_equipment_3 #F-16C
		amount = 36
		producer = USA
    }
	add_equipment_to_stockpile = {
		type = MR_Fighter_equipment_3 #F-16D
		amount = 12
		producer = USA
    }
	add_equipment_to_stockpile = { #Su-22
		type = Strike_fighter_equipment_1
		amount = 18
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = transport_plane_equipment_1 #C-130e
		amount = 5
		producer = USA
    }
	add_equipment_to_stockpile = {
		type = attack_helicopter_equipment_1 #MI-24
		amount = 28
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_1 #Mil Mi-8
		amount = 7
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_1 #PZL W-3 Sokół
		amount = 22
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_2 #PZL SW-4
		amount = 24
    }
	
	###DOMESTIC PRODUCTION###
	
	add_equipment_production = { #Rosomak
		equipment = {
			type = IFV_Equipment_4
			creator = "POL"
		}
		requested_factories = 1
		progress = 0.2
		efficiency = 50
		amount = 2
	}
	add_equipment_production = { #AHS Krab
		equipment = {
			type = SP_arty_equipment_2
			creator = "POL"
		}
		requested_factories = 1
		progress = 0.2
		efficiency = 50
		amount = 2
	}
	
	if = {
		limit = { has_dlc = "Death or Dishonor" }
		GER = {
			create_production_license = {
				target = POL 
				equipment = {
					type = MBT_Equipment_4 #Leopard 2A4/5
					creator = "GER"
				}
				cost_factor = 0
			}
		}
		else = {
			add_equipment_production = { #Leopard 2A4/5
				equipment = {
					type = MBT_Equipment_4
					creator = "GER"
				}
				requested_factories = 1
				progress = 0.2
				efficiency = 50
				amount = 2
			}
		}
	}
	
}
	