﻿division_template = {
	name = "Infantry Brigade"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		
	}
	support = {

	}
}
division_template = {
	name = "Mehcanized Brigade"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		
	}
	support = {
		armor_Comp = { x = 0 y = 0 }
	}
}


units = {
	division = {	
		name = "Infantry Brigade"
		location = 10777
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 10833
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 3199
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 8094
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 10921
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 10921
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 8094
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 3035
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 12814
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 10921
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 10921
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 12814
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 3035
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Infantry Brigade"
		location = 8094
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Mehcanized Brigade"
		location = 10833
		division_template = "Mehcanized Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Mehcanized Brigade"
		location = 8094
		division_template = "Mehcanized Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	
	
	
}
instant_effect = {

	add_equipment_to_stockpile = {
		type = Inf_equipment_0
		amount = 3000
		producer = SOV
    }
		add_equipment_to_stockpile = {
		type = command_control_equipment_0
		amount = 310
		producer = SOV
    }
		add_equipment_to_stockpile = {
		type = L_AT_Equipment_1
		amount = 170
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = H_AT_Equipment_1
		amount = 15
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = AA_Equipment_1
		amount = 140
		producer = SOV
    }
		
	add_equipment_to_stockpile = {
		type = MBT_Equipment_1 #T-55
		amount = 30
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = APC_Equipment_2 #Type 6614
		amount = 120
		producer = ITA
    }

}