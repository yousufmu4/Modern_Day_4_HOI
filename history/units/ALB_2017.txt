﻿#
division_template = {
	name = "Brigada e Reagimit të Shpejtë"
	
	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		L_Engi_Bat = { x = 0 y = 3 }
	}
	
	support = {
		Mot_Recce_Comp = { x = 0 y = 0 }
	}
	
}

division_template = {
	name = "Batalioni i Komando"
	
	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}
	
	support = {
	}
	
	priority = 2
}

units = {
	
	#Main three infantry battalions
	division= {	
		name = "Brigada e Reagimit të Shpejtë"
		location = 9914		#Tirana
		division_template = "Brigada e Reagimit të Shpejtë"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	
	#Commandos
	division= {	
		name = "Batalioni i Komando"
		location = 9914		#Tirana
		division_template = "Batalioni i Komando"
		start_experience_factor = 0.7
		start_equipment_factor = 0.01
	}
	
	#Special Operations Battalion
	division= {	
		name = "Batalioni i Operacioneve Speciale"
		location = 9914		#Tirana
		division_template = "Batalioni i Komando"
		start_experience_factor = 0.7
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	#Eurocopter AS532 Cougar x 6
	
	add_equipment_to_stockpile = {
		type = Inf_equipment_0			#AK-47s
		amount = 2000
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = Inf_equipment_3			#M4s	
		amount = 400					#at least for 1 battalion
		producer = USA
	}
	
	add_equipment_to_stockpile = {
		type = Inf_equipment_4			#Beretta ARX160
		amount = 400					#For special forces
		producer = ITA
	}
	
	add_equipment_to_stockpile = {
		type = AA_Equipment_0			#Strela-2
		amount = 50
		producer = SOV
	}
	
	add_equipment_to_stockpile = {
		type = L_AT_Equipment_1			#HJ-8
		amount = 50						
		producer = CHI
	}
	
	add_equipment_to_stockpile = {
		type = util_vehicle_equipment_2	#Land-Rover Defender
		amount = 127
		producer = ENG
	}
	
	add_equipment_to_stockpile = {
		type = util_vehicle_equipment_1	#Wolf SSA
		amount = 150
		producer = GER
	}
	
	add_equipment_to_stockpile = {
		type = util_vehicle_equipment_2	#Iveco VM 90
		amount = 250
		producer = ITA
	}
	
	add_equipment_to_stockpile = {
		type = util_vehicle_equipment_4	#Iveco LMV
		amount = 23
		producer = ITA
	}
	
	add_equipment_to_stockpile = {
		type = util_vehicle_equipment_2	#Humwee
		amount = 30
		producer = USA
	}
	
	add_equipment_to_stockpile = {
		type = util_vehicle_equipment_5	#MRAP
		amount = 150
		producer = USA
	}
	
	add_equipment_to_stockpile = {
		type = APC_Equipment_2			#M113
		amount = 130
		producer = USA
	}
	
	add_equipment_to_stockpile = {
		type = APC_Equipment_1			#Type 63
		amount = 86
		producer = CHI
	}
	
	add_equipment_to_stockpile = {
		type = MBT_Equipment_1			#Type 59
		amount = 721
		producer = CHI
	}
	
	#Fill up what's missing
	add_equipment_to_stockpile = {
		type = command_control_equipment_1		
		amount = 150
		producer = USA
	}
	
		
	#Fixed wing aircraft that have been retired since 2005
	add_equipment_to_stockpile = {
		type = MR_Fighter_equipment_1	#Chengdu F-7	
		amount = 11
		producer = CHI
	}
	
}