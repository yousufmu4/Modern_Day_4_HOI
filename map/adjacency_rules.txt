#Adjacency rules are special rules for adjacencies
#If smbd who is at war fights over canal/strait it is considered closed
#Enemy status is considered if any of sides controlling the chanel is at war with country
#Friend status is considered non of sides controlling the chanel is at war with country and one of controllers is ally of giving military access
#Every one else is considered as neutral according to canal/strait
#Military access considered as being friend
#Rules needs linking from the straits that are affected by the rule ( adjacency_rule_id )

#Name is how you refer to the rule in adjecencies.csv

#Suez Canal
adjacency_rule
{
	contested =
	{
		army = no
		navy = no
		submarine = no
		trade = no
	}
	enemy =
	{
		army = no
		navy = no
		submarine = no
		trade = no
	}
	friend =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	
	required_provinces = { 12049 1155 4073 9947 }
		
	icon = 12049
	offset = { 1 0 -6 }

	name = "SUEZ_CANAL"
}

#Gibraltar Strait
adjacency_rule
{
	contested =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	enemy =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	friend =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	
	required_provinces = { 4135 9945 }
	
	icon = 5407	
	offset = { -31.0 0.0 -2.0 }
	

	name = "GIBRALTAR_STRAIT"
}

#Øresund - (Østersøen <-> Kattegat/Skagerak/Nordsøen)
adjacency_rule
{
	contested =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	enemy =
	{
		army = no
		navy = no
		submarine = no
		trade = no
	}
	friend =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	
	required_provinces = { 6287 3277 3325 3260 }
	
	icon = 6287
	offset = { -14 0 9 }

	name = "DANISH_BELTS_STRAIT"
}

#Panama Canal
adjacency_rule
{
	contested =
	{
		army = no
		navy = no
		submarine = no
		trade = no
	}
	enemy =
	{
		army = no
		navy = no
		submarine = no
		trade = no
	}
	friend =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	
	required_provinces = { 4624 7617 }
		
	icon = 7617
	offset = { -3 0 -2 }

	name = "PANAMA_CANAL"
}

#Turkish Straits
adjacency_rule
{
	contested =
	{
		army = no
		navy = no
		submarine = no
		trade = no
	}
	enemy =
	{
		army = no
		navy = no
		submarine = no
		trade = no
	}
	friend =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	
	required_provinces = { 9833 11829 }
	
	icon = 9833
	offset = { -2 0 -9 }

	name = "BOSPHORUS_STRAIT"
}

#Kiel Canal
adjacency_rule
{
	contested =
	{
		army = no
		navy = no
		submarine = no
		trade = no
	}
	enemy =
	{
		army = no
		navy = no
		submarine = no
		trade = no
	}
	friend =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	
	required_provinces = { 11366 317 3231 }
		
	icon = 11366
	offset = { 5 0 -6 }

	name = "KIEL_CANAL"
}
#Mandeb Strait
adjacency_rule
{
	contested =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	enemy =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	friend =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	
	required_provinces = { 4985 5074 }
		
	icon = 2776
	offset = { -17 0 16 }

	name = "Mandeb_Strait"
}
#Strait of Hormuz
adjacency_rule
{
	contested =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	enemy =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	friend =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	
	required_provinces = { 13381 4959 }
		
	icon = 2919
	offset = { -2 0 8 }

	name = "Strait_of_Hormuz"
}
#Strait of Malacca
adjacency_rule
{
	contested =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	enemy =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	friend =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	
	required_provinces = { 12299 12113 }
		
	icon = 5960
	offset = { 40 0 -31 }

	name = "Strait_of_Malacca"
}
#Palk Strait
adjacency_rule
{
	contested =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	enemy =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	friend =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	
	required_provinces = { 10201 4416 1381 }
		
	icon = 7479
	offset = { -10 0 7 }

	name = "Palk_Strait"
}
#Sunda Strait
adjacency_rule
{
	contested =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	enemy =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	friend =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	
	required_provinces = { 10279 1342 }
		
	icon = 2714
	offset = { 20 0 25 }

	name = "Sunda_Strait"
}
#Strait of Dover
adjacency_rule
{
	contested =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	enemy =
	{
		army = no
		navy = no
		submarine = yes
		trade = no
	}
	friend =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral =
	{
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	
	required_provinces = { 3501 11548 }
		
	icon = 13004
	offset = { 10 0 0 }

	name = "Strait_of_Dover"
}
