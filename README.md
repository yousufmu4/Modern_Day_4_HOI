Welcome to Modern Day 4 HOI.

If your new visit the [WIKi] (https://gitlab.com/truemikesmith/Modern_Day_4_HOI/wikis/home) for tutorials & resources.

---

**How we make decisions on the mod (rules/guiding document)**

I'm putting this out here to make things more clear, mainly for the new members. It's not meant to change the way we do things, (I think) I'm simply putting the general established practice into writing.  

This is my suggestion for a "guiding document" on how we make decisions and a few other things surrounding the mod. It may be edited, replaced or removed according to discussion.  

**The aim of the project is to create an modern day mod, where the emphasis is more on realism than grand conquest.** 

Within that framework, there will always be disagreements with regards to what one would like the to see implemented, or how to handle certain things. So how do we make decisions?  

Well, we do decisions by trying to reach a consensus. Anyone can propose an idea, and anyone can participate in the discussion surrounding it. If one thinks a proposed idea will not work, then state why. Often a proposed idea will have some shortcomings or be very labor intensive, but by proposing adjustments, it can be made into an workable idea, and an consensus will be reached around the modified idea. It's the job of the proponents of an idea to convince the skeptics, and the job of the skeptics to convince the proponent. Arguing until everyone agrees will naturally bring forth technical issues or alternative ideas that may be better.  

If there, following an extensive discussion, is an general consensus, except for a single person who don't like it, it may still be approved. An exception to this, is if the person objecting is regarded as the person who have put the most work into that area of the mod, or is the one with the most technical expertise within that area. Also, if only 2 ppl in total are discussing an issue and these two don't agree, there can be no consensus.  

If no consensus can be reached, the idea is 'on hold', but open to future discussion. Maybe it needs more suggestions, or someone needs more time to think about it.  

Even if a consensus is reached, that does not mean that it will be implemented right away. We don't have enough manpower to do all the things we want right away, and then the idea becomes an 'planned' one. Whenever it starts being implemented, things may be further discussed or modified, as maybe we have gotten new tools to do it more efficient or new ideas that can do roughly the same thing.  

Keep in mind that someone working on a area should always have a certain degree of freedom to do things how they think is best. If someone objects to how things are made, this is best handled by them discussing it over.  

Sometimes my (@Killerrabbit ) suggestions may sound very authoritative in tone, like a to-do list... this is because as the founder of the project, I did draw up ideas for nearly every area of the mod even before making the mod thread on the Paradox Forums. Regardless, they should be counted as suggestions like all others.  

To prevent more time being spent discussing than actually doing work, any member can just do the work and put things they want into the mod without approval, but it may be modified or removed. Thankfully gitlabs keeps tracks of changes so if there should be any disputes things can easily be restored.  

**Some other things:**  

People who join the project, but who are totally inactive will be removed.  

Inactive profiles of people who have made contributions before will be kept for a long time, because they are always welcome to return. If the person has joined another modern day project the profile may be removed regardless.

This is not a full 'rules' for the mod project. Hopefully we never have to actually spell them out. Just use common sense and decency and all will be fine. 