﻿add_namespace = KUW_SAU
#This makes kuwait/saudi no longer want to ally each other if kuwait leaves gcc
############THE KUWATI-SAUDI AI STRATEGY IS ADDED IN history/countries.SAUDI ARABIA.txt and removed by
#events/kuwait.txt  - KUW_SAU.1
country_event = {
    id = KUW_SAU.1
    

   hidden = yes
	
	trigger = {
		original_tag = KUW
		NOT = { has_opinion_modifier = Gulf_Cooperation_Council_Relations }
	}
	mean_time_to_happen = {
		days = 10
	}
	
	immediate  = {
		SAU = {
				add_ai_strategy = {
					type = alliance
					id = "KUW"
					value = -200
				}
			}
			KUW = {
				add_ai_strategy = {
					type = alliance
					id = "SAU"
					value = -200
				}
			}
	}
	
    option = {
        name = KUW_SAU.1.o1
        
    }
}