﻿# Internal Faction Events
add_namespace = int_fac_deposed

#Faction removed!
#one event for every faction so it can be individualized

#small_medium_business_owners the_bazaar the_donju
country_event = {
	id = int_fac_deposed.1
	title = int_fac_deposed.1.t
	desc = int_fac_deposed.1.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#international_bankers wall_street
country_event = {
	id = int_fac_deposed.2
	title = int_fac_deposed.2.t
	desc = int_fac_deposed.2.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#Fossil_fuel_industry
country_event = {
	id = int_fac_deposed.3
	title = int_fac_deposed.3.t
	desc = int_fac_deposed.3.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#industrial_conglomerates chaebols
country_event = {
	id = int_fac_deposed.4
	title = int_fac_deposed.4.t
	desc = int_fac_deposed.4.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#oligarchs
country_event = {
	id = int_fac_deposed.5
	title = int_fac_deposed.5.t
	desc = int_fac_deposed.5.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#maritime_industry
country_event = {
	id = int_fac_deposed.6
	title = int_fac_deposed.6.t
	desc = int_fac_deposed.6.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#Defense_Industry
country_event = {
	id = int_fac_deposed.7
	title = int_fac_deposed.7.t
	desc = int_fac_deposed.7.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#the_military
country_event = {
	id = int_fac_deposed.8
	title = int_fac_deposed.8.t
	desc = int_fac_deposed.8.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#intelligence_community isi_pakistan VEVAK
country_event = {
	id = int_fac_deposed.9
	title = int_fac_deposed.9.t
	desc = int_fac_deposed.9.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#labour_unions
country_event = {
	id = int_fac_deposed.10
	title = int_fac_deposed.10.t
	desc = int_fac_deposed.10.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#landowners
country_event = {
	id = int_fac_deposed.11
	title = int_fac_deposed.11.t
	desc = int_fac_deposed.11.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}
#farmers
add_namespace = farmers_events
add_namespace = farmers_events.0.t
add_namespace = farmers_events.0.d
add_namespace = farmers_events.0.a
add_namespace = farmers_events.0.b
add_namespace = farmers_events.0.c
add_namespace = farmers_events.0.dd
country_event = {
	id = int_fac_deposed.12
	title = int_fac_deposed.12.t
	desc = int_fac_deposed.12.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#communist_cadres
country_event = {
	id = int_fac_deposed.13
	title = int_fac_deposed.13.t
	desc = int_fac_deposed.13.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#The_Clergy The_Ulema the_priesthood wahabi_ulema
country_event = {
	id = int_fac_deposed.14
	title = int_fac_deposed.14.t
	desc = int_fac_deposed.14.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#saudi_royal_family
country_event = {
	id = int_fac_deposed.15
	title = int_fac_deposed.15.t
	desc = int_fac_deposed.15.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#iranian_quds_force
country_event = {
	id = int_fac_deposed.16
	title = int_fac_deposed.16.t
	desc = int_fac_deposed.16.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#foreign_jihadis
country_event = {
	id = int_fac_deposed.17
	title = int_fac_deposed.17.t
	desc = int_fac_deposed.17.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

#IRGC
country_event = {
	id = int_fac_deposed.18
	title = int_fac_deposed.18.t
	desc = int_fac_deposed.18.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	option = {
		name = int_fac_deposed.1.a
		add_timed_idea = { idea = internal_faction_removed days = 550 }
	}
}

############ WIP ##############
## Faction opinion of leader ##
###############################

#Decrease in faction opinion
country_event = {
	id = int_fac_deposed.19
	title = int_fac_deposed.19.t
	desc = int_fac_deposed.19.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	immediate = { 
		decrease_internal_faction_opinion = yes
	}
	
	option = {
		name = int_fac_deposed.19.a
	}
}

#Increase in faction opinion
country_event = {
	id = int_fac_deposed.20
	title = int_fac_deposed.20.t
	desc = int_fac_deposed.20.d
	#picture = SOMEPIC

	is_triggered_only = yes
	
	immediate = { 
		increase_internal_faction_opinion = yes
	}
	
	option = {
		name = int_fac_deposed.19.a
	}
}

