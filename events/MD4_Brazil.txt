﻿add_namespace = brazil
add_namespace = brafocus

# Election of 2018: Lula vs Bolsonaro
country_event = {
	id = brazil.1
	title = brazil.1.t
	desc = brazil.1.d
	picture = GFX_report_event_usa_election_generic
	
	is_triggered_only = yes

	trigger = {
		tag = BRA
		#democratic > 0.5
		date > 2018.1.1
		date < 2019.1.1
	}
	
	option = { 	
		name = brazil.1.a	# Elect Lula
		trigger = {
			communism > 0.2
		}
		ai_chance = {
			base = 60
			# modifier = {
				# factor = 0
				# is_historical_focus_on = yes
			# }
		}		
		set_politics = {
			ruling_party = communism
			elections_allowed = yes
		}
	}

	option = { 
		name = brazil.1.b	# Elect Bolsonaro
		trigger = {
			Nationalist > 0.2
		}
		ai_chance = {
			base = 38
			# modifier = {
				# factor = 0
				# is_historical_focus_on = yes
			# }
		}		
		set_politics = {
			ruling_party = Nationalist
			elections_allowed = yes
		}
	}
	
	option = { 
		name = brazil.1.c	# Elect Aécio
		trigger = {
			democratic > 0.2
		}
		ai_chance = {
			base = 1
			# modifier = {
				# factor = 100
				# is_historical_focus_on = yes
			# }
		}
		set_politics = {
			ruling_party = democratic
			elections_allowed = yes
		}
	}
	
	option = { 
		name = brazil.1.e	# Elect Michel Temer
		trigger = {
			neutrality > 0.2
		}
		ai_chance = {
			base = 1
			# modifier = {
				# factor = 100
				# is_historical_focus_on = yes
			# }
		}
		set_politics = {
			ruling_party = neutrality
			elections_allowed = yes
		}
	}
}

# Brazil Is Influencing Our Country
country_event = {
	id = brafocus.1
	title = brafocus.1.t
	desc = brafocus.1.d
	picture = GFX_report_event_usa_election_generic
	
	is_triggered_only = yes
	
	option = { 	
		name = brafocus.1.a
		trigger = { has_government = communism }
		add_ideas = brazilian_emerging_influence
		# reverse_add_opinion_modifier = { target = BRA modifier = integrated_into_unasur }
		# ROOT = { add_opinion_modifier = { target = BRA modifier = integrated_into_unasur } }
	}

	option = { 
		name = brafocus.1.a
		trigger = { 
			OR = {
				has_government = democratic
				has_government = neutrality
			}
		}
		add_ideas = brazilian_democratic_influence
		# reverse_add_opinion_modifier = { target = BRA modifier = integrated_into_unasur }
		# ROOT = { add_opinion_modifier = { target = BRA modifier = integrated_into_unasur } }
	}
}

# UNASUR Continental Customs Tariff
country_event = {
	id = brafocus.2
	title = brafocus.2.t
	desc = brafocus.2.d
	picture = GFX_trade_agreement
	
	is_triggered_only = yes
	
	option = { 	
		name = brafocus.2.a
		# custom_effect_tooltip = decrease_economic_growth_tt
		# hidden_effect = {
			# decrease_economic_growth = yes
		# }
		# set_growth_flag = yes
	}

}