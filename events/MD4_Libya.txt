﻿add_namespace = Libya

### Libyan civil war cleanup events ###

#Give the winner LBA cosmetic tag
country_event = {
	id = Libya.0
	
	hidden = yes
	
	trigger = {
		OR = {
			AND = {
				tag = HOR
				NOT = {
					country_exists = GNA 
					country_exists = GNC
				}
			}
			AND = {
				tag = GNA
				NOT = {
					country_exists = HOR 
					country_exists = GNC
				}
			}
			AND = {
				tag = GNC
				NOT = {
					country_exists = GNA 
					country_exists = HOR
				}
			}
		}
		NOT = { has_global_flag = Libya_Civil_War_Over }
	}
	
	mean_time_to_happen = {
		days = 2
	}
	
	immediate = {
		#Civil war is over
		set_global_flag = Libya_Civil_War_Over
		#Clear core status
		391 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		392 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		393 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		394 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		395 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		396 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		397 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		918 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		919 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		920 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		#Make the winner Libya
		random_country = {
			limit = {
				OR = {
					tag = HOR
					tag = GNA
					tag = GNC
				}
			}
			set_cosmetic_tag = LBA
			add_state_core = 391
			add_state_core = 392
			add_state_core = 393
			add_state_core = 394
			add_state_core = 395
			add_state_core = 396
			add_state_core = 397
			add_state_core = 918
			add_state_core = 919
			add_state_core = 920
			set_capital = 391
		}
	}
	
	option = {
		name = Libya.0.a
	}
}

#Recreate Libya in case Libya is annexed
country_event = {
	id = Libya.1
	
	hidden = yes
	
	trigger = {
		NOT = {
			country_exists = LBA
			country_exists = HOR
			country_exists = GNC
			country_exists = GNA
		}
		NOT = { 391 = { is_core_of = LBA } }
	}
	
	mean_time_to_happen = {
		days = 2
	}
	
	immediate = {
		#Clear core status
		391 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		392 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		393 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		394 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		395 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		396 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		397 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		918 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		919 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		920 = {
			remove_core_of = LBA
			remove_core_of = HOR
			remove_core_of = GNA
			remove_core_of = GNC
		}
		LBA = {
			add_state_core = 391
			add_state_core = 392
			add_state_core = 393
			add_state_core = 394
			add_state_core = 395
			add_state_core = 396
			add_state_core = 397
			add_state_core = 918
			add_state_core = 919
			add_state_core = 920
			set_capital = 391
		}
	}
	
	option = {
		name = Libya.1.a
	}
}
			