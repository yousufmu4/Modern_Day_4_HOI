# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Algeria supplies weapons to HOR in Libya in the fight against ISIS
Algeria_Support_HOR = {
	
	enable = {
		original_tag = ALG
	}
	
	abort = {
		HOR = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
				in_different_faction = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "HOR"
		value = 25
	}
	
	ai_strategy = {
		type = protect 
		id = "HOR"
		value = 25
	}
	
	ai_strategy = {
		type = influence
		id = "HOR"
		value = 25
	}
	
	ai_strategy = {
		type = support
		id = "HOR"
		value = 25
	}
	
}

#Algerian-Moroccan tension
Algeria_Morocco_BorderClosed ={
	enable = {
		original_tag = ALG
		country_exists = MOR
	}
	abort = {
		OR = {
			NOT = { country_exists = SHA }
			NOT = { country_exists = MOR }
			SHA = { NOT = { has_opinion_modifier = SHA_MOR_Occupation } }
		}

	}
	
	ai_strategy = {
		type = antagonize
		id = "MOR"
		value = 25
	}
	ai_strategy = {
		type = contain
		id = "MOR"
		value = 50
	}
}