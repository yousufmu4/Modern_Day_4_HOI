# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Saudi Arabia supports Haftar of Libya
Saudi_Support_HOR = {
	
	enable = {
		original_tag = SAU
	}
	
	abort = {
		HOR = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
				in_different_faction = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "HOR"
		value = 50
	}
	
	ai_strategy = {
		type = protect 
		id = "HOR"
		value = 50
	}
	
	ai_strategy = {
		type = influence
		id = "HOR"
		value = 50
	}
	
	ai_strategy = {
		type = support
		id = "HOR"
		value = 50
	}
	
}

#Saudi support for Syrian islamists
SAU_Intervene_NUS = {

	enable = {
		original_tag = SAU
	}
	
	abort = {
		NUS = { 
			OR = { 
				war_with_us = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "NUS"
		value = 25
	}
	
	ai_strategy = {
		type = support
		id = "NUS"
		value = 150
	}
}

#Saudis support Yemen
SAU_YEM = {

	enable = {
		original_tag = SAU
	}
	
	abort = {
		YEM = { 
			war_with_us = yes
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "YEM"
		value = 100
	}
	
	ai_strategy = {
		type = protect 
		id = "YEM"
		value = 100
	}
	
	ai_strategy = {
		type = influence
		id = "YEM"
		value = 100
	} 
	
	ai_strategy = {
		type = support
		id = "YEM"
		value = 200
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "YEM"
		value = 200
	}
}

SaudiArabia_InfluencesInTheWorld = { # NOTE Values are high to remove the define value of -100 (this should be changed IF define is change)
	enable = {
		has_government = fascism
		OR = {
			original_tag = SAU
			original_tag = UAE
			original_tag = QAT
		}
	}
	abort = {
		OR = {
			NOT = { has_government = fascism }
			NOT = { has_opinion_modifier = Arabian_Peninsula_West_Economical_Relations }
		}
	}

	# North America
	ai_strategy = {
		type = befriend
		id = "USA"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "CAN"
		value = 100
	}

	# EU
	ai_strategy = {
		type = befriend
		id = "ENG"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "IRE"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "IRE"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "FRA"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "SPR"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "POR"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "AUS"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "ITA"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "BEL"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "HOL"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "LUX"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "GER"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "DEN"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "NOR"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "SWE"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "FIN"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "POL"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "EST"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "LAT"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "LIT"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "CZH"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "SLO"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "HUN"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "ROM"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "BUL"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "CRO"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "GRE"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "TUR"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "FIN"
		value = 100
	}
	ai_strategy = {
		type = befriend	 # NOT EU MEMBER BUT THEY LOVE THEM ANYWAYS
		id = "SWI"
		value = 100
	}
	ai_strategy = {
		type = befriend	 # NOT EU MEMBER BUT THEY LOVE THEM ANYWAYS
		id = "ICE"
		value = 100
	}

	# Russia
	ai_strategy = {
		type = befriend
		id = "SOV"
		value = 100
	}
	
	# Far East
	ai_strategy = {
		type = befriend
		id = "CHI"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "JAP"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "KOR"
		value = 100
	}
}

Western_SaudiArabia_Relations {
	enable = {
		is_western_nation = yes
		has_opinion_modifier = Arabian_Peninsula_West_Economical_Relations
	}
	abort = {
		OR = {
			NOT = { has_opinion_modifier = Arabian_Peninsula_West_Economical_Relations }
		}
	}

	ai_strategy = {
		type = befriend
		id = "SAU"
		value = 25
	}
	ai_strategy = {
		type = befriend
		id = "UAE"
		value = 25
	}
	ai_strategy = {
		type = befriend
		id = "QAT"
		value = 25
	}
}

Asia_SaudiArabia_Relations {
	enable = {
		OR = {
			original_tag = CHI
			original_tag = KOR
			original_tag = RAJ
			original_tag = JAP
		}
		has_opinion_modifier = Arabian_Peninsula_Asia_Economical_Relations
	}
	abort = {
		OR = {
			NOT = { has_opinion_modifier = Arabian_Peninsula_West_Economical_Relations }
		}
	}
	
	ai_strategy = {
		type = befriend
		id = "SAU"
		value = 25
	}
	ai_strategy = {
		type = befriend
		id = "UAE"
		value = 25
	}
	ai_strategy = {
		type = befriend
		id = "QAT"
		value = 25
	}
}

SaudiArabia_Oman_Relations {
	enable = {
		original_tag = SAU
		has_opinion_modifier = Gulf_Cooperation_Council_Relations
	}
	abort = {
		NOT = { has_opinion_modifier = Gulf_Cooperation_Council_Relations }
	}
	
	ai_strategy = {
		type = befriend
		id = "OMA"
		value = 250
	}
	ai_strategy = {
		type = alliance
		id = "OMA"
		value = 250
	}
	
}

Saudi_Iran_Relations = {
	enable = {
		original_tag = SAU
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = befriend 
		id = "PER"
		value = -100
	}
	ai_strategy = {
		type = conquer 
		id = "PER"
		value = 50
	}
	ai_strategy = {
		type = invade 
		id = "PER"
		value = 50
	}
	ai_strategy = {
		type = protect 
		id = "PER"
		value = -100
	}
	ai_strategy = {
		type = antagonize
		id = "PER"
		value = 200
	}
	ai_strategy = {
		type = alliance
		id = "PER"
		value = -200
	}
	ai_strategy = {
		type = support
		id = "PER"
		value = -100
	}
	ai_strategy = {
		type = contain
		id = "PER"
		value = 200
	}
}

Saudi_Houti_Relations = {
	enable = {
		original_tag = SAU
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = befriend 
		id = "HOU"
		value = -100
	}
	ai_strategy = {
		type = conquer 
		id = "HOU"
		value = 50
	}
	ai_strategy = {
		type = invade 
		id = "HOU"
		value = 50
	}
	ai_strategy = {
		type = protect 
		id = "HOU"
		value = -100
	}
	ai_strategy = {
		type = antagonize
		id = "HOU"
		value = 100
	}
	ai_strategy = {
		type = alliance
		id = "HOU"
		value = -200
	}
	ai_strategy = {
		type = support
		id = "HOU"
		value = -100
	}
	ai_strategy = {
		type = contain
		id = "HOU"
		value = 100
	}
}

Saudi_Hezbollah_Relations = {
	enable = {
		original_tag = SAU
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = befriend 
		id = "HEZ"
		value = -100
	}
	ai_strategy = {
		type = conquer 
		id = "HEZ"
		value = 25
	}
	ai_strategy = {
		type = invade 
		id = "HEZ"
		value = 25
	}
	ai_strategy = {
		type = protect 
		id = "HEZ"
		value = -100
	}
	ai_strategy = {
		type = antagonize
		id = "HEZ"
		value = 100
	}
	ai_strategy = {
		type = alliance
		id = "HEZ"
		value = -200
	}
	ai_strategy = {
		type = support
		id = "HEZ"
		value = -100
	}
	ai_strategy = {
		type = contain
		id = "HEZ"
		value = 100
	}
}

Saudi_SYR_Relations = {
	enable = {
		original_tag = SAU
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = befriend 
		id = "SYR"
		value = -100
	}
	ai_strategy = {
		type = conquer 
		id = "SYR"
		value = 25
	}
	ai_strategy = {
		type = invade 
		id = "SYR"
		value = 25
	}
	ai_strategy = {
		type = protect 
		id = "SYR"
		value = -100
	}
	ai_strategy = {
		type = antagonize
		id = "SYR"
		value = 100
	}
	ai_strategy = {
		type = alliance
		id = "SYR"
		value = -200
	}
	ai_strategy = {
		type = ignore
		id = "SYR"
		value = -100
	}
	ai_strategy = {
		type = support
		id = "SYR"
		value = -100
	}
	ai_strategy = {
		type = contain
		id = "SYR"
		value = 100
	}
}

Saudi_SYR_Relations = {
	enable = {
		original_tag = SAU
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = send_volunteers_desire 
		id = "ROJ"
		value = -200
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "TUA"
		value = -100
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "PAK"
		value = 100
	}
}

Saudi_area_priority = {
	enable = {
		original_tag = SAU
	}
	ai_strategy = {
		type = area_priority
		id = europe
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = north_america
		value = -25
	}
	ai_strategy = {
		type = area_priority
		id = caribbean
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = south_america
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = asia
		value = -25
	}
	ai_strategy = {
		type = area_priority
		id = pacific
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = oceania
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = middle_east
		value = 200
	}
	ai_strategy = {
		type = area_priority
		id = africa
		value = -25
	}
}