# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Russia on very friendly terms with commander Haftar of Libya
Russia_Support_HOR = {
	
	enable = {
		original_tag = SOV
	}
	
	abort = {
		HOR = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
				in_different_faction = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "HOR"
		value = 50
	}
	
	ai_strategy = {
		type = protect 
		id = "HOR"
		value = 50
	}
	
	ai_strategy = {
		type = influence
		id = "HOR"
		value = 50
	}
	
	ai_strategy = {
		type = support
		id = "HOR"
		value = 50
	}
	
}

#Russo-Georgian relations
Russia_Georgia_Area_Politics ={
	enable = {
		original_tag = SOV
		country_exists = GEO
	}
	abort = {
		OR = {
			GEO = { has_government = communism }
			is_in_faction_with = GEO
		}
	}

	ai_strategy = {
		type = contain
		id = "GEO"
		value = 100
	}
	ai_strategy = {
		type = protect
		id = "ABK"
		value = 50
	}
	ai_strategy = {
		type = protect
		id = "SOO"
		value = 50
	}
}

#Russo-Ukrainian relations
Russia_Ukraine_Area_Politics ={
	enable = {
		original_tag = SOV
	}
	abort = {
		UKR = { has_government = communism }
	}
	
	ai_strategy = {
		type = contain
		id = "UKR"
		value = 50
	}
	ai_strategy = {
		type = antagonize
		id = "UKR"
		value = 25
	}
	ai_strategy = {
		type = befriend
		id = "NOV"
		value = 50
	}
	ai_strategy = {
		type = support
		id = "NOV"
		value = 50
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "NOV"
		value = 100
	}
}

#To avoid volunteer forces from defeating ISIS alone
Russia_IRQ = {

	enable = {
		original_tag = SOV
	}
	
	abort = {
		IRQ = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "IRQ"
		value = 50
	}
	
	ai_strategy = {
		type = protect 
		id = "IRQ"
		value = 50
	}
	
	ai_strategy = {
		type = influence
		id = "IRQ"
		value = 50
	} 
	
	ai_strategy = {
		type = support
		id = "IRQ"
		value = 50
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "IRQ"
		value = -200
	}
}

#Russia supports Assad
RUS_Assad_SYR = {

	enable = {
		original_tag = SOV
	}
	
	abort = {
		SYR = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
				SYR = {	has_war_with = ROJ }
				NOT = { has_government = communism }
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "SYR"
		value = 225
	}
	
	ai_strategy = {
		type = protect 
		id = "SYR"
		value = 200
	}
	
	ai_strategy = {
		type = support
		id = "SYR"
		value = 200
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "SYR"
		value = 200
	}
}

Russia_USA_Relations = {
	enable = {
		original_tag = SOV
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = antagonize
		id = "USA"
		value = 50
	}
	ai_strategy = {
		type = alliance
		id = "USA"
		value = -200
	}
	ai_strategy = {
		type = support
		id = "USA"
		value = -200
	}
}

Russia_SaudiArabia_Relations = { # NOTE Values are high to remove the define value of -100 (this should be changed IF define is change)
	enable = {
		original_tag = SOV
		has_opinion_modifier = Arabian_Peninsula_Russia_Economical_Relations
	}
	abort = {
		NOT = { has_opinion_modifier = Arabian_Peninsula_Russia_Economical_Relations }
	}

	ai_strategy = {
		type = befriend
		id = "SAU"
		value = 100
	}
	ai_strategy = {
		type = alliance
		id = "SAU"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "UAE"
		value = 100
	}
	ai_strategy = {
		type = alliance
		id = "UAE"
		value = 100
	}
	ai_strategy = {
		type = befriend
		id = "QAT"
		value = 100
	}
	ai_strategy = {
		type = alliance
		id = "QAT"
		value = 100
	}
}

Russia_area_priority = {
	enable = {
		original_tag = SOV
	}
	ai_strategy = {
		type = area_priority
		id = europe
		value = 200
	}
	ai_strategy = {
		type = area_priority
		id = north_america
		value = 50
	}
	ai_strategy = {
		type = area_priority
		id = caribbean
		value = 50
	}
	ai_strategy = {
		type = area_priority
		id = south_america
		value = 50
	}
	ai_strategy = {
		type = area_priority
		id = asia
		value = 150
	}
	ai_strategy = {
		type = area_priority
		id = pacific
		value = 50
	}
	ai_strategy = {
		type = area_priority
		id = oceania
		value = 50
	}
	ai_strategy = {
		type = area_priority
		id = middle_east
		value = 150
	}
	ai_strategy = {
		type = area_priority
		id = africa
		value = 50
	}
}