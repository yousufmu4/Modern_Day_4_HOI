# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

Ukraine_Novorossiya_Relations ={
	enable = {
		original_tag = UKR
		country_exists = NOV
	}
	abort = {
		NOT = { country_exists = NOV }
	}
	
	ai_strategy = {
		type = antagonize
		id = "NOV"
		value = 200
	}
	ai_strategy = {
		type = conquer
		id = "NOV"
		value = 100
	}
	ai_strategy = {
		type = contain
		id = "NOV"
		value = 100
	}
	ai_strategy = {
		type = antagonize
		id = "SOV"
		value = 50
	}
}