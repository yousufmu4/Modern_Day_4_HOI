# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Conquer Somaliland
SOM_conquer_SML = {
	
	enable = {
		OR = {
			original_tag = SOM
			original_tag = SHB
		}
		OR = {
			has_government = Nationalist
			has_government = fascism
		}
		SML = { exists = yes }
		has_war = no
	}
	
	abort = {
		OR = {
			NOT = {
				has_government = Nationalist
				has_government = fascism
			}
			SML = { is_subject_of = ROOT }
			AND = {
				owns_state = 237
				owns_state = 238
			}
		}
	}
	
	ai_strategy = {
		type = antagonize
		id = "SML"
		value = 100
	}
	
	ai_strategy = {
		type = conquer
		id = "SML"
		value = 100
	}	
}

#Conquer Ogaden
SOM_conquer_Ogaden = {
	
	enable = {
		OR = {
			original_tag = SOM
			original_tag = SHB
		}
		OR = {
			has_government = Nationalist
			has_government = fascism
		}
		NOT = { owns_state = 236 }
		has_war = no
	}
	
	abort = {
		OR = {
			NOT = {
				has_government = Nationalist
				has_government = fascism
			}
			any_country = {
				OR = {
					is_subject_of = ROOT
					is_in_faction_with = ROOT
				}
				owns_state = 237
			}
			owns_state = 237
		}
	}
	
	ai_strategy = {
		type = antagonize
		id = "ETH"
		value = 100
	}
	
	ai_strategy = {
		type = conquer
		id = "ETH"
		value = 100
	}	
}