ideas = {

	Vehicle_Company = {
	
		designer = yes
		
		TUR_FNSS_Vehicle_Company = {
		
			picture = FNSS_TUR
			
			allowed = {
				tag = TUR
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.124
			}
			
			traits = {
				Cat_AFV_4
			
			}
			ai_will_do = {
				factor = 0.4 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		TUR_Otokar_Vehicle_Company = {
		
			picture = Otokar_TUR
			
			allowed = {
				tag = TUR
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARMOR = 0.155
			}
			
			traits = {
				Cat_ARMOR_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		TUR_Roketsan_Infantry_Weapon_Company = {
		
			picture = Roketsan_TUR
			
			allowed = {
				tag = TUR
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AT = 0.186
			}
			
			traits = {
				Cat_AT_6
			
			}
			ai_will_do = {
				factor = 0.6 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		TUR_MKEK_Infantry_Weapon_Company = {
		
			picture = MKEK_TUR
			
			allowed = {
				tag = TUR
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.124
			}
			
			traits = {
				Cat_INF_WEP_4
			
			}
			ai_will_do = {
				factor = 0.4 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		TUR_MKEK_Vehicle_Company = {
		
			picture = MKEK_TUR
			
			allowed = {
				tag = TUR
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARTILLERY = 0.124
			}
			
			traits = {
				Cat_ARTILLERY_4
			
			}
			ai_will_do = {
				factor = 0.4 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
	Aircraft_Company = {
	
		designer = yes
		
		TUR_Turkish_Aerospace_Industries_Aircraft_Company = {
		
			picture = Turkish_Aerospace_Industries_TUR
			
			allowed = {
				tag = TUR
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_FIGHTER = 0.124
			}
			
			traits = {
				Cat_FIGHTER_4
			
			}
			ai_will_do = {
				factor = 0.4 #Most countries don't have decent airforces
				
				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
	Helicopter_Company = {
	
		designer = yes
		
		TUR_Turkish_Aerospace_Industries_Helicopter_Company = {
		
			picture = Turkish_Aerospace_Industries_TUR
			
			allowed = {
				tag = TUR
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_HELI = 0.124
			}
			
			traits = {
				Cat_HELI_4
			
			}
			ai_will_do = {
				factor = 0.4 #Most countries don't have decent airforces
				
				modifier = {
					has_tech = attack_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					has_tech = transport_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
	Ship_Company = {
	
		designer = yes
		
		TUR_Golcuk_Naval_Shipyard_Ship_Company = {
		
			picture = Golcuk_Naval_Shipyard_TUR
			
			allowed = {
				tag = TUR
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_SURFACE_SHIP = 0.093
			}
			
			traits = {
				Cat_NAVAL_EQP_3
			
			}
			ai_will_do = {
				factor = 0.3
				
				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
							is_coastal = yes
						}
					}
					factor = -10
				}
			}
			
		}
	}
	
	Submarine_Company = {
	
		designer = yes
		
		TUR_Golcuk_Naval_Shipyard_Submarine_Company = {
		
			picture = Golcuk_Naval_Shipyard_TUR
			
			allowed = {
				tag = TUR
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_D_SUB = 0.093
			}
			
			traits = {
				Cat_D_SUB_3
			
			}
			ai_will_do = {
				factor = 0.3
				
				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
						factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
						is_coastal = yes
						}
					}
					factor = -10
				}
			}
			
		}
	}
	
}
