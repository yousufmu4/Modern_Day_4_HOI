ideas = {

	Infantry_Weapon_Company = {
	
		designer = yes
		
		AUS_Steyr_Mannlicher_Infantry_Weapon_Company = {
		
			picture = Steyr_Mannlicher_AUS
			
			allowed = {
				tag = AUS
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.217
			}
			
			traits = {
				Cat_INF_WEP_7
			
			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		AUS_General_Dynamics_Steyr_Vehicle_Company = {
		
			picture = General_Dynamics_Steyr_AUS
			
			allowed = {
				tag = AUS
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.217
			}
			
			traits = {
				Cat_AFV_7
			
			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
}
