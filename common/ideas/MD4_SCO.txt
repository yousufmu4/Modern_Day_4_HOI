ideas = {

	country = {
		
		sco_member = {
			
			allowed = {
				OR = {
					original_tag = CHI
					original_tag = SOV
					original_tag = RAJ
					original_tag = PAK
					original_tag = KAZ
					original_tag = TAJ
					original_tag = KYR
					original_tag = UZB
					
					

				}
			}
			
			default = yes
	
			modifier = {
				political_power_gain = 0.2
				trade_opinion_factor = 0.2
			}
		}
}
}
