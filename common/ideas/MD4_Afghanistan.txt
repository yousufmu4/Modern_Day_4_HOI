ideas = {
	country = {

		Resolute_Support_Mission = {
			
			allowed = {
				original_tag = USA
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				experience_gain_army = 0.01
			}
		}
	}
}