#al_jazeera

ideas = {

	country = {
		
		al_jazeera = {
			
			allowed = {
					original_tag = QAT
			}
			
			default = yes
	
			modifier = {
				political_power_gain = 0.5
				neutrality_drift = 0.03
				consumer_goods_factor = 0.02
				send_volunteer_size = 1
				send_volunteer_divisions_required = -0.6
				subversive_activites_upkeep = -0.5
				ai_join_ally_desire_factor = -100
			}
		}

		al_jazeera_banned = {
			
			allowed = {
					is_arabic_nation = yes
			}
			
			default = yes
	
			modifier = {
				neutrality_drift = 0.02
				stability_factor = -0.02
			}
		}
		
		al_jazeera_allowed = {
			
			allowed = {
					is_arabic_nation = yes
			}
			
			default = yes
	
			modifier = {
				drift_defence_factor = -0.2
				neutrality_drift = 0.03
			}
		}
		
		muslim_brotherhood_crackdown = {
			
			allowed = {
					is_arabic_nation = yes
			}
			
			default = yes
	
			modifier = {
				neutrality_drift = -0.05
				fascism_acceptance = 5
				stability_factor = -0.03
			}
		}
		
		muslim_brotherhood_home = {
			
			allowed = {
					original_tag = EGY
			}
			
			default = yes
	
			modifier = {
				neutrality_drift = 0.05
			}
		}
		
		rentier_state = {
			
			allowed = {
				always = no
			}
	
			modifier = {
				production_speed_buildings_factor = -0.1
				corruption_cost_factor = 0.5
				economic_cycles_cost_factor = 0.25
				political_power_gain = 0.15
				drift_defence_factor = 0.25
			}
		}
		
		youth_radicalization = {
			
			allowed = {
				always = no
			}
	
			modifier = {
				fascism_drift = 0.04
				drift_defence_factor = -0.1
				foreign_subversive_activites = 0.1
			}
		}
		
		saudi_aid = {
			
			allowed = {
				always = no
			}
	
			modifier = {
				consumer_goods_factor = -0.05
				fascism_drift = 0.03
				drift_defence_factor = -0.1
			}
		}
		
		cybercaliphate = {
			
			allowed = {
				always = no
			}
	
			modifier = {
				fascism_drift = 0.02
				foreign_subversive_activites = 0.2
			}
		}
		
		katib_hezbollah = {
			
			allowed = {
				original_tag = HEZ
			}
	
			modifier = {
				send_volunteer_size = 4
				send_volunteer_divisions_required = -0.3
				subversive_activites_upkeep = -0.2
				ai_focus_aggressive_factor = 0.5
				ai_join_ally_desire_factor = -100
				conscription_factor = 0.1
			}
		}
		
		syrian_hezbollah = {
			
			allowed = {
				original_tag = HEZ
			}
	
			modifier = {
				send_volunteer_size = 5
				send_volunteer_divisions_required = -0.3
				subversive_activites_upkeep = -0.2
				ai_focus_aggressive_factor = 0.5
				ai_join_ally_desire_factor = -100
				conscription_factor = 0.1
			}
		}
		
		qatar_expat = {
			
			allowed = {
				original_tag = QAT
			}
	
			modifier = {
				conscription = -0.022
				MONTHLY_POPULATION = 1.7
			}
		}
		saudi_expat = {
			
			allowed = {
				original_tag = SAU
			}
	
			modifier = {
				conscription = -0.0075
				MONTHLY_POPULATION = 0.4
				send_volunteer_size = 2
			}
		}
		uae_expat = {
			
			allowed = {
				original_tag = UAE
			}
	
			modifier = {
				conscription = -0.022
				MONTHLY_POPULATION = 1.1
				send_volunteer_size = 1
				send_volunteer_divisions_required = -0.7
			}
		}
		kuwait_expat = {
			
			allowed = {
				original_tag = KUW
			}
	
			modifier = {
				conscription = -0.0175
				MONTHLY_POPULATION = 0.6
			}
		}
		
		iranian_aid = {
			
			allowed = {
				NOT = { original_tag = PER }
			}
	
			modifier = {
				consumer_goods_factor = -0.05
				experience_gain_army = 0.05
				industrial_capacity_factory = 0.1
				communism_drift = 0.02
				political_power_gain = -0.25
				
			}
		}
		sadrist_militias = {
			
			allowed = { original_tag = IRQ }
	
			modifier = {
				Nationalist_drift = 0.05
			}
		}
		
		GCC_member = {
			
			allowed = {
				always = no
			}
	
			modifier = {
			}
		}
		
		LoAS_member = {
			
			allowed = {
				always = no
			}
	
			modifier = {
			}
		}
		
		restoring_hope = {
			
			allowed = {
				original_tag = SAU
			}
	
			modifier = {
			air_close_air_support_defence_factor = 0.02
			experience_gain_air = 0.02
			}
		}

		
	}



}