ideas = {

	Infantry_Weapon_Company = {
	
		designer = yes
		
		SAF_Denel_Land_Systems_Infantry_Weapon_Company = {
		
			picture = Denel_Land_Systems_SAF
			
			allowed = {
				tag = SAF
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.155
			}
			
			traits = {
				Cat_INF_WEP_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		SAF_Denel_Land_Systems_Vehicle_Company = {
		
			picture = Denel_Land_Systems_SAF
			
			allowed = {
				tag = SAF
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARTILLERY = 0.155
			}
			
			traits = {
				Cat_ARTILLERY_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		SAF_Land_Systems_OMC_Vehicle_Company = {
		
			picture = Land_Systems_OMC_SAF
			
			allowed = {
				tag = SAF
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.248
			}
			
			traits = {
				Cat_AFV_8
			
			}
			ai_will_do = {
				factor = 0.8 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		SAF_Denel_Dynamics_Infantry_Weapon_Company = {
		
			picture = Denel_Dynamics_SAF
			
			allowed = {
				tag = SAF
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF = 0.155
			}
			
			traits = {
				Cat_INF_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
			}
			
		}
	}
	
}
