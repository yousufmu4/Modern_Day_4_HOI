Anti_Bully = {
	AB_offer_stalemate_Atk = {

		icon = jap_conquer_china
		
		target_trigger = {
			from = { has_defensive_war_with = ROOT } #deffender
			has_offensive_war_with = FROM #attacker
		}
		
		available = {
			has_offensive_war_with = FROM #attacker
			or = {
				has_war_support < 0.51
				from = { surrender_progress > 0.49 }
			}
			not = { has_idea = three_months_of_war }
		}

		visible = {
			has_offensive_war_with = FROM #attacker
			# if = {
				# limit = {
					# or = {
						# tag = SYR
					# }
					# has_global_flag = Syria_Civil_War_Over
				# }
			# }
			# has_global_flag = Yemen_Civil_War_Over
		}
		
		complete_effect = {
			hidden_effect = { save_event_target_as = AB_event_target }
			from = {
				hidden_effect = { set_country_flag = stalemate@ROOT }
				country_event = { id = anti_bully.5 days = 1 }  
			}
			effect_tooltip = {
				if = {
					limit = { #If you're in a faction, not a major and not the leader
						is_in_faction = yes
						is_faction_leader = no
						any_country = { #Faction leader must have capitulated for me to see this decision
							is_in_faction_with = ROOT
							has_war_together_with = ROOT
							is_faction_leader = yes
							has_capitulated = no
						}
					}
					custom_effect_tooltip = anti_bully.tt
					if = {
						limit = { has_idea = NATO_member }
						remove_ideas = NATO_member
					}
				}
			}
		}

	}
	
	AB_offer_stalemate_Def = {

		icon = jap_conquer_china
		
		target_trigger  = {
			from = { has_offensive_war_with = ROOT } #attacker
			has_defensive_war_with = FROM #deffender
		}
		
		available = {
			has_defensive_war_with = FROM #attacker
			or = {
				has_war_support < 0.51
				from = { surrender_progress > 0.49 }
			}
			not = { has_idea = three_months_of_war }
			custom_trigger_tooltip = { tooltip = anti_bully2.tt has_country_flag = stalemate@FROM }
			
		}

		visible = {
			has_defensive_war_with = FROM #attacker
			#not = { tag = from }
			# if = {
				#If you're in a faction, not a major and not the leader
				# limit = {
					# is_in_faction = yes
					# is_faction_leader = no
					# is_major = no
				# }
				# any_country = { #Faction leader must have capitulated for me to see this decision
					# is_in_faction_with = ROOT
					# has_war_together_with = ROOT
					# is_faction_leader = yes
					# has_capitulated = yes
				# }
			# }
			# if = {
				#If you're in NATO
				# limit = {
					# has_idea = NATO_member
				# }
				# has_country_flag = Leader_of_NATO #You must be the leader to offer a stalemate
			# }

		}
		complete_effect = {
			hidden_effect = { save_event_target_as = AB_event_target }
			from = {
				country_event = { id = anti_bully.5 days = 1 }  
			}
			effect_tooltip = {
				if = {
					limit = { #If you're in a faction, not a major and not the leader
						is_in_faction = yes
						is_faction_leader = no
						any_country = { #Faction leader must have capitulated for me to see this decision
							is_in_faction_with = ROOT
							has_war_together_with = ROOT
							is_faction_leader = yes
							has_capitulated = no
						}
					}
					custom_effect_tooltip = anti_bully.tt
					if = {
						limit = { has_idea = NATO_member }
						remove_ideas = NATO_member
					}
					
				}
			}
		}
		
		#Ask for territory, puppet, change goverment, gain influence

	}
	
}